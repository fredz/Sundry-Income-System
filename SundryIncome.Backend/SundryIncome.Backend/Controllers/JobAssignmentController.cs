﻿using AutoMapper;
using SundryIncome.Common.DTOs;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Domain.Entities;
using SundryIncome.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using SundryIncome.Backend.Models;
using SundryIncome.Repositories;

namespace SundryIncome.Backend.Controllers
{
    [RoutePrefix("Amc")]
    [EnableCors("*", "*", "*")]
    public class JobAssignmentController : BaseApiController
    {
        private readonly IJobAssignmentManager _jManager;

        public JobAssignmentController()
        {
            _jManager = new JobAssignmentManager(new JobAssignmentRepository());
        }

        [Route("GetJobAssignmentByJobId")]
        [HttpPost]
        public IHttpActionResult GetJobAssignmentByJobId(JobAssignmentModel jobAssignment)
        {
            try
            {
                if (jobAssignment != null && IsAuthorize(jobAssignment.Validator))
                {
                    _jManager.Configure(Logger);
                    var jb = _jManager.GetJobAssignmentByJobId(jobAssignment.JobId);

                    return Ok(jb); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened getting job assignment by JobId {jobAssignment.JobId}", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateJobAssignment")]
        public IHttpActionResult UpdateJobAssignment(JobAssignmentModel jobAssignment)
        {
            try
            {
                if (jobAssignment != null && IsAuthorize(jobAssignment.Validator))
                {
                    JobAssignment nJobAssignment = new JobAssignment
                    {
                        JobAssignId = jobAssignment.JobAssignId,
                        JobId = jobAssignment.JobId,
                        FranchiseeId = jobAssignment.FranchiseeId,
                        SubcontractorId = jobAssignment.SubcontractorId,
                        Amount = jobAssignment.Amount,
                        PurchaseInvoiceNo = jobAssignment.PurchaseInvoiceNo,
                        Primary = jobAssignment.Primary,
                    };
                    Logger.InfoToPortal("Updating job assignment");
                    _jManager.Configure(Logger);
                    _jManager.UpdateJobAssignment(nJobAssignment);
                    return Ok("UpdateJobAssignment Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating job assignment", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateJobAssignmentAmount")]
        public IHttpActionResult UpdateJobAssignmentAmount(JobAssignmentModel jobAssignment)
        {
            try
            {
                if (jobAssignment != null && IsAuthorize(jobAssignment.Validator))
                {
                    Logger.InfoToPortal("Updating amount job assignment");
                    _jManager.Configure(Logger);
                    _jManager.UpdateJobAssignmentAmount(jobAssignment.JobAssignId, jobAssignment.Amount);
                    return Ok("UpdateJobAssignmentAmount Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating job assignment", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateJobAssignmentPrimary")]
        public IHttpActionResult UpdateJobAssignmentPrimary(JobAssignmentModel jobAssignment)
        {
            try
            {
                if (jobAssignment != null && IsAuthorize(jobAssignment.Validator))
                {
                    Logger.InfoToPortal("Updating primary  job assignment ");
                    _jManager.Configure(Logger);
                    _jManager.UpdateJobAssignmentPrimary(jobAssignment.JobAssignId, jobAssignment.Primary);
                    return Ok("UpdateJobAssignmentPrimary Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating job assignment", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("DeleteJobAssignment")]
        public IHttpActionResult DeleteJobAssignment(JobAssignmentModel jobAssignment)
        {
            try
            {
                if (jobAssignment != null && IsAuthorize(jobAssignment.Validator))
                {
                    Logger.InfoToPortal($"Deleting job assignment {jobAssignment.JobAssignId}");
                    _jManager.Configure(Logger);
                    _jManager.DeleteJobAssignment(jobAssignment.JobAssignId);
                    return Ok("Delete Successfully"); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened deleting job {jobAssignment.JobAssignId}", e);
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetJobAssignmentByJobId")]
        public IHttpActionResult GetJobAssignmentByJobId(int jobId)
        {
            try
            {
                Logger.InfoToPortal("Getting all jobs assignment by jobId");
                _jManager.Configure(Logger);
                var allJobs= _jManager.GetJobAssignmentByJobId(jobId);
                return Ok(allJobs);
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened getting all jobs assignment by jobId", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("AddJobAssignment")]
        public IHttpActionResult AddJobAssignment(JobAssignmentModel jobAssignment)
        {
            try
            {
                if (jobAssignment != null && IsAuthorize(jobAssignment.Validator) )
                {
                    JobAssignment nJobAssignment = new JobAssignment
                    {   
                        JobId = jobAssignment.JobId,
                        FranchiseeId = jobAssignment.FranchiseeId,
                        SubcontractorId = jobAssignment.SubcontractorId,
                        Amount = jobAssignment.Amount,
                        PurchaseInvoiceNo = jobAssignment.PurchaseInvoiceNo,
                        Primary = jobAssignment.Primary,
                    };
                    Logger.InfoToPortal("Adding new job Assignment");
                    _jManager.Configure(Logger);
                    var jobAdded = _jManager.AddJobAssignment(nJobAssignment);

                    return Ok(jobAdded); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened adding new job Assignment", e);
                return InternalServerError(e);
            }
        }
    }
}
