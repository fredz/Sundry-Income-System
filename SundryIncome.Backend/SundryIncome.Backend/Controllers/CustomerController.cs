﻿using AutoMapper;
using SundryIncome.Common.DTOs;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Domain.Entities;
using SundryIncome.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using SundryIncome.Backend.Models;
using SundryIncome.Repositories;

namespace SundryIncome.Backend.Controllers
{
    [RoutePrefix("Amc")]
    [EnableCors("*", "*", "*")]
    public class CustomerController : BaseApiController
    {
        private readonly ICustomerManager _cManager;
      
        public CustomerController()
        {
            _cManager = new CustomerManager(new CustomerRepository());
        }

        [Route("GetCustomerById")]
        [HttpPost]
        public IHttpActionResult GetCustomerById(CustomerModel customer)
        {
            try
            {
                if (customer != null && IsAuthorize(customer.Validator))
                {
                    _cManager.Configure(Logger);
                    var cst = _cManager.GetCustomerById(customer.CustomerId);

                    return Ok(cst); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened getting customer by id {customer.CustomerId}", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("DeleteCustomer")]
        public IHttpActionResult DeleteCustomer(CustomerModel customer)
        {
            try
            {
                if (customer != null && IsAuthorize(customer.Validator))
                {
                    Logger.InfoToPortal($"Deleting customer {customer.CustomerId}");
                    _cManager.Configure(Logger);
                    _cManager.DeleteCustomer(customer.CustomerId);
                    return Ok("Delete Successfully"); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened deleting customer {customer.CustomerId}", e);
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetAllCustomers")]
        public IHttpActionResult GetAllCustomers(CustomerModel customer)
        {
            try
            {
                Logger.InfoToPortal("Getting all customers");
                _cManager.Configure(Logger);
                var allCustomers = _cManager.GetAllCustomers();
                return Ok(allCustomers);
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened getting all customers", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("AddCustomer")]
        public IHttpActionResult AddCustomer(CustomerModel customer)
        {
            try
            {
                if (customer != null && IsAuthorize(customer.Validator) )
                {
                    Customer cst = new Customer
                    {
                        FirstName = customer.FirstName,
                        LastName = customer.LastName,
                        BillingName = customer.BillingName,
                        Email = customer.Email,
                        Phone = customer.Phone,
                        MobileNo = customer.MobileNo,
                        PurchaseOrderRequired = customer.PurchaseOrderRequired,
                        AMCInvoiceOnly = customer.AMCInvoiceOnly,
                        DefaultInvoiceDescription = customer.DefaultInvoiceDescription
                    };
                    Logger.InfoToPortal("Adding new customer");
                    _cManager.Configure(Logger);
                    var nCustomer = _cManager.AddCustomer(cst);

                    return Ok(nCustomer); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened adding new customer", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateCustomer")]
        public IHttpActionResult UpdateCustomer(CustomerModel customer)
        {
            try
            {
                if (customer != null && IsAuthorize(customer.Validator))
                {
                    Customer cst = new Customer
                    {
                        CustomerId = customer.CustomerId,
                        FirstName = customer.FirstName,
                        LastName = customer.LastName,
                        BillingName = customer.BillingName,
                        Email = customer.Email,
                        Phone = customer.Phone,
                        MobileNo = customer.MobileNo,
                        PurchaseOrderRequired = customer.PurchaseOrderRequired,
                        AMCInvoiceOnly = customer.AMCInvoiceOnly,
                        DefaultInvoiceDescription = customer.DefaultInvoiceDescription
                    };
                    Logger.InfoToPortal("Updating customer");
                    _cManager.Configure(Logger);
                    _cManager.UpdateCustomer(cst);
                    return Ok("Update Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating customer", e);
                return InternalServerError(e);
            }
        }
    }
}
