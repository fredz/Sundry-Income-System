﻿using AutoMapper;
using SundryIncome.Common.DTOs;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Domain.Entities;
using SundryIncome.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using SundryIncome.Backend.Models;
using SundryIncome.Repositories;

namespace SundryIncome.Backend.Controllers
{
    [RoutePrefix("Amc")]
    [EnableCors("*", "*", "*")]
    public class JobController : BaseApiController
    {
        private readonly IJobManager _jManager;

        public JobController()
        {
            _jManager = new JobManager(new JobRepository());
        }

        [Route("GetCreateJobById")]
        [HttpPost]
        public IHttpActionResult GetCreateJobById(JobModel job)
        {
            try
            {
                if (job != null && IsAuthorize(job.Validator))
                {
                    _jManager.Configure(Logger);
                    var jb = _jManager.GetJobById(job.JobId);

                    return Ok(jb); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened getting job by id {job.JobId}", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("DeleteCreateJob")]
        public IHttpActionResult DeleteCreatedJob(JobModel job)
        {
            try
            {
                if (job != null && IsAuthorize(job.Validator))
                {
                    Logger.InfoToPortal($"Deleting job {job.CustomerId}");
                    _jManager.Configure(Logger);
                    _jManager.DeleteCreatedJob(job.JobId);
                    return Ok("Delete Successfully"); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened deleting job {job.JobId}", e);
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetAllCreateJobs")]
        public IHttpActionResult GetAllCreateJobs(JobModel job)
        {
            try
            {
                Logger.InfoToPortal("Getting all jobs");
                _jManager.Configure(Logger);
                var allJobs= _jManager.GetAllJobs();
                return Ok(allJobs);
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened getting all jobs", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("CreateJob")]
        public IHttpActionResult CreateJob(JobModel job)
        {
            try
            {
                if (job != null && IsAuthorize(job.Validator) )
                {
                    Job nJob = new Job
                    {
                        CustomerId = job.CustomerId,
                        AreaManagerId = job.AreaManagerId,
                        OrderNo = job.OrderNo,
                        Description = job.Description,
                        RequestedDate = job.RequestedDate,
                        DateFrom = job.DateFrom,
                        DateTo = job.DateTo,
                        //DateCompleted = job.DateCompleted,
                        AMCAmount = job.AMCAmount,
                       // CreatedOn = job.CreatedOn,
                        CreatedBy = job.CreatedBy,
                       // ModifiedOn = job.ModifiedOn,
                       // ModifiedBy = job.ModifiedBy,
                       // SalesInvoiceNo = job.SalesInvoiceNo,
                       // SalesInvoiceDate = job.SalesInvoiceDate
                    };
                    Logger.InfoToPortal("Adding new job");
                    _jManager.Configure(Logger);
                    var jobAdded = _jManager.AddJob(nJob);

                    return Ok(jobAdded); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened adding new customer", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateCreateJob")]
        public IHttpActionResult UpdateCreateJob(JobModel job)
        {
            try
            {
                if (job != null && IsAuthorize(job.Validator))
                {
                    Job nJob = new Job
                    {
                        JobId = job.JobId,
                        CustomerId = job.CustomerId,
                        AreaManagerId = job.AreaManagerId,
                        OrderNo = job.OrderNo,
                        Description = job.Description,
                        RequestedDate = job.RequestedDate,
                        DateFrom = job.DateFrom,
                        DateTo = job.DateTo,
                        DateCompleted = job.DateCompleted,
                        AMCAmount = job.AMCAmount,
                       // CreatedOn = job.CreatedOn,
                        CreatedBy = job.CreatedBy,
                       // ModifiedOn = job.ModifiedOn,
                        ModifiedBy = job.ModifiedBy,
                        SalesInvoiceNo = job.SalesInvoiceNo,
                        SalesInvoiceDate = job.SalesInvoiceDate
                    };
                    Logger.InfoToPortal("Updating job");
                    _jManager.Configure(Logger);
                    _jManager.UpdateJob(nJob);
                    return Ok("UpdateCreateJob Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating job", e);
                return InternalServerError(e);
            }
        }
    }
}
