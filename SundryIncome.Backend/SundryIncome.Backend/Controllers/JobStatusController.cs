﻿using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Domain.Entities;
using SundryIncome.Managers;
using System;
using System.Web.Http;
using System.Web.Http.Cors;
using SundryIncome.Backend.Models;
using SundryIncome.Repositories;

namespace SundryIncome.Backend.Controllers
{
    [RoutePrefix("Amc")]
	[EnableCors("*", "*", "*")]
	public class JobStatusController : BaseApiController
    {
        private readonly IJobStatusManager _jManager;

        public JobStatusController()
        {
            _jManager = new JobStatusManager(new JobStatusRepository());
        }

        [Route("GetJobStatusById")]
        [HttpPost]
        public IHttpActionResult GetJobStatusById(JobStatusModel jobStatus)
        {
            try
            {
                if (jobStatus != null && IsAuthorize(jobStatus.Validator))
                {
                    _jManager.Configure(Logger);
                    var rol = _jManager.GetJobStatusById(jobStatus.JobStatusId);

                    return Ok(rol); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened getting jobStatus by id {jobStatus.JobStatusId}", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("DeleteStatusJob")]
        public IHttpActionResult DeleteJobStatus(JobStatusModel jobStatus)
        {
            try
            {
                if (jobStatus != null && IsAuthorize(jobStatus.Validator))
                {
                    Logger.InfoToPortal($"Deleting jobStatus {jobStatus.JobStatusId}");
                    _jManager.Configure(Logger);
                    _jManager.DeleteJobStatus(jobStatus.JobStatusId);
                    return Ok("Delete Successfully"); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened deleting jobstatus {jobStatus.JobStatusId}", e);
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetAllJobStatuses")]
        public IHttpActionResult GetallJobStatuses(JobStatusModel jobStatus)
        {
            try
            {
                Logger.InfoToPortal("Getting all Statuses");
                _jManager.Configure(Logger);
                var allJobStatuses = _jManager.GetallJobStatuses();
                return Ok(allJobStatuses);
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened getting all JobStatus", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("AddJobStatus")]
        public IHttpActionResult AddJobStatus(JobStatusModel jobStatus)
        {
            try
            {
                if (jobStatus != null && IsAuthorize(jobStatus.Validator))
                {
                    JobStatus js = new JobStatus()
                    {
                        JobStatusId = jobStatus.JobStatusId,
                        Name = jobStatus.Name,
                        ColorCode = jobStatus.ColorCode,
                        Rank = jobStatus.Rank
                    };
                    Logger.InfoToPortal("Adding new jobStatus");
                    _jManager.Configure(Logger);
                    var nJobStatus = _jManager.AddJobStatus(js);

                    return Ok(nJobStatus); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened adding new jobstatus", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateJobStatus")]
        public IHttpActionResult UpdateJobStatus(JobStatusModel jobStatus)
        {
            try
            {
                if (jobStatus != null && IsAuthorize(jobStatus.Validator))
                {
                    JobStatus js = new JobStatus()
                    {
                        JobStatusId = jobStatus.JobStatusId,
                        Name = jobStatus.Name,
                        ColorCode = jobStatus.ColorCode,
                        Rank = jobStatus.Rank
                    };
                    Logger.InfoToPortal("Updating jobStatus");
                    _jManager.Configure(Logger);
                    _jManager.UpdateJobStatus(js);
                    return Ok("UpdateCreateJob Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating jobStatus", e);
                return InternalServerError(e);
            }
        }
    }
}
