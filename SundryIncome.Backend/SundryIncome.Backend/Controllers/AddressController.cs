﻿using AutoMapper;
using SundryIncome.Common.DTOs;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Domain.Entities;
using SundryIncome.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using SundryIncome.Backend.Models;
using SundryIncome.Repositories;
using System.Web.Http.Cors;

namespace SundryIncome.Backend.Controllers
{
    [RoutePrefix("Amc")]
	[EnableCors("*", "*", "*")]
	public class AddressController : BaseApiController
    {
        private readonly IAddressManager _aManager;
        //private const string authCode = "F335C7EE410E437CAE0EE49CC93A84F5";

        public AddressController()
        {
            _aManager = new AddressManager(new AddressRepository());
        }

        [Route("GetAddressById")]
        [HttpPost]
        public IHttpActionResult GetAddressById(AddressModel address)
        {
            try
            {
                if (address != null && IsAuthorize(address.Validator))
                {
                    _aManager.Configure(Logger);
                    var ad = _aManager.GetAddressById(address.AddressId);

                    return Ok(ad); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened getting address by id {address.AddressId}", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("DeleteAddress")]
        public IHttpActionResult DeleteAddress(AddressModel address)
        {
            try
            {
                if (address != null && IsAuthorize(address.Validator))
                {
                    Logger.InfoToPortal($"Deleting address {address.AddressId}");
                    _aManager.Configure(Logger);
                    _aManager.DeleteAddress(address.AddressId);
                    return Ok("Delete Successfully"); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened deleting address {address.AddressId}", e);
                return InternalServerError(e);
            }
        }

        [Route("GetAddressByCustomerId")]
        [HttpPost]
        public IHttpActionResult GetAddressCustomerByCustomerId(AddressModel address)
        {
            try
            {
                if (IsAuthorize(address.Validator))
                {
                    _aManager.Configure(Logger);
                    var ad = _aManager.GetAddressByCustomerId(address.CustomerId);

                    return Ok(ad);
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened getting address customer by id {address.AddressId}", e);
                return InternalServerError(e);
            }
        }
        [HttpGet]
        [Route("GetAllAddresses")]
        public IHttpActionResult GetAllAddresses(AddressModel address)
        {
            try
            {
                Logger.InfoToPortal("Getting all addresses");
                _aManager.Configure(Logger);
                var allAddresses = _aManager.GetAllAddresses();
                return Ok(allAddresses);
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened getting all addresses", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("AddAddress")]
        public IHttpActionResult AddAddress(AddressModel address)
        {
            try
            {
                if (address != null && IsAuthorize(address.Validator) )
                {
                    Address ad = new Address
                    {
                        Addresss = address.Addresss, 
                        Locality = address.Locality,
                        State = address.State,
                        PostalCode = address.PostalCode,
                        CustomerId =  address.CustomerId,
                        Type = address.Type
                    };
                    Logger.InfoToPortal("Adding new Address");
                    _aManager.Configure(Logger);
                    var nAddress = _aManager.AddAddress(ad);

                    return Ok(nAddress); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened adding new Address", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateAddress")]
        public IHttpActionResult UpdateAddress(AddressModel address)
        {
            try
            {
                if (address != null && IsAuthorize(address.Validator))
                {
                    Address ad = new Address
                    {
                        AddressId = address.AddressId,
                        Addresss = address.Addresss,
                        Locality = address.Locality,
                        State = address.State,
                        PostalCode = address.PostalCode,
                        CustomerId = address.CustomerId,
                        Type =  address.Type
                    };
                    Logger.InfoToPortal("Updating address");
                    _aManager.Configure(Logger);
                    _aManager.UpdateAddress(ad);
                    return Ok("Update Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating address", e);
                return InternalServerError(e);
            }
        }
    }
}
