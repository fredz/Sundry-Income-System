﻿using AutoMapper;
using SundryIncome.Common.DTOs;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Domain.Entities;
using SundryIncome.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using SundryIncome.Backend.Models;
using SundryIncome.Repositories;

namespace SundryIncome.Backend.Controllers
{
    [RoutePrefix("Amc")]
	[EnableCors("*", "*", "*")]
	public class RoleController : BaseApiController
    {
        private readonly IRoleManager _roleManager;
        private const string authCode = "F335C7EE410E437CAE0EE49CC93A84F5";

        public RoleController()
        {
            _roleManager = new RoleManager(new RoleRepository());
        }

        [Route("GetRoleById")]
        [HttpPost]
        public IHttpActionResult GetRoleById(RoleModel role)
        {
            try
            {
                if (ModelState.IsValid && role.Validator.Equals(authCode))
                {
                    _roleManager.Configure(Logger);
                    var rol = _roleManager.GetRoleById(role.RoleId);

                    return Ok(rol); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened getting role by id {role.RoleId}", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("DeleteRole")]
        public IHttpActionResult DeleteRole(RoleModel role)
        {
            try
            {
                if (ModelState.IsValid && role.Validator.Equals(authCode))
                {
                    Logger.InfoToPortal($"Deleting role {role.RoleId}");
                    _roleManager.Configure(Logger);
                    _roleManager.DeleteRole(role.RoleId);
                    return Ok("Delete Successfully"); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened deleting role {role.RoleId}", e);
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetAllRoles")]
        public IHttpActionResult GetAllRoles(RoleModel role)
        {
            try
            {
                Logger.InfoToPortal("Getting all roles");
                _roleManager.Configure(Logger);
                var allRoles = _roleManager.GetAllRoles();
                return Ok(allRoles);
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened getting all roles", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("AddRole")]
        public IHttpActionResult AddRole(RoleModel role)
        {
            try
            {
                if (ModelState.IsValid && role.Validator.Equals(authCode))
                {
                    Role rol = new Role()
                    {
                        RoleId = role.RoleId,
                        Name = role.Name,
                        Rank = role.Rank
                    };
                    Logger.InfoToPortal("Adding new role");
                    _roleManager.Configure(Logger);
                    var nRole = _roleManager.AddRole(rol);

                    return Ok(nRole); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened adding new role", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateRole")]
        public IHttpActionResult UpdateRole(RoleModel role)
        {
            try
            {
                if (ModelState.IsValid && role.Validator.Equals(authCode))
                {
                    Role rol = new Role()
                    {
                        RoleId = role.RoleId,
                        Name = role.Name,
                        Rank = role.Rank
                    };
                    Logger.InfoToPortal("Updating role");
                    _roleManager.Configure(Logger);
                    _roleManager.UpdateRole(rol);
                    return Ok("Update Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating role", e);
                return InternalServerError(e);
            }
        }
    }
}
