﻿using AutoMapper;
using SundryIncome.Common.DTOs;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Domain.Entities;
using SundryIncome.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using SundryIncome.Backend.Models;
using SundryIncome.Repositories;

namespace SundryIncome.Backend.Controllers
{
    [RoutePrefix("Amc")]
	[EnableCors("*", "*", "*")]
	public class UserRoleController : BaseApiController
    {
        private readonly IUserRoleManager _userRoleManager;
        private const string authCode = "F335C7EE410E437CAE0EE49CC93A84F5";

        public UserRoleController()
        {
            _userRoleManager = new UserRoleManager(new UserRoleRepository());
        }

        [Route("GetUserRolesByUserId")]
        [HttpPost]
        public IHttpActionResult GetUserRolesByUserId(UserRoleModel userRole)
        {
            try
            {
                if (IsAuthorize(userRole.Validator))
                {
                    _userRoleManager.Configure(Logger);
                    var rol = _userRoleManager.GetUserRolesByUserId(userRole.UserId);

                    return Ok(rol); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened getting UserRole by id {userRole.RoleId}", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("DeleteUserRole")]
        public IHttpActionResult DeleteUserRole(UserRoleModel userRole)
        {
            try
            {
                if (IsAuthorize(userRole.Validator))
                {
                    Logger.InfoToPortal($"Deleting role {userRole.UserId}");
                    _userRoleManager.Configure(Logger);
                    _userRoleManager.DeleteUserRole(userRole.UserId, userRole.RoleId);
                    return Ok("Delete Successfully"); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened deleting UserRole {userRole.UserId}", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("AddUserRole")]
        public IHttpActionResult AddUserRole(UserRoleModel userRole)
        {
            try
            {
                if (IsAuthorize(userRole.Validator))
                {
                    UserRole rol = new UserRole()
                    {
                        UserId = userRole.UserId,
                        RoleId = userRole.RoleId
                    };
                    Logger.InfoToPortal("Adding new UseRrole");
                    _userRoleManager.Configure(Logger);
                    var nRole = _userRoleManager.AddUserRole(rol);

                    return Ok(nRole); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened adding new UserRole", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateUserRole")]
        public IHttpActionResult UpdateUserRole(UserRoleModel userRole)
        {
            try
            {
                if (IsAuthorize(userRole.Validator))
                {
                    UserRole uRol = new UserRole()
                    {
                        UserId = userRole.UserId,
                        RoleId = userRole.RoleId,
                        Disabled = userRole.Disabled
                    };
                    Logger.InfoToPortal("Updating userRole");
                    _userRoleManager.Configure(Logger);
                    int rows = _userRoleManager.UpdateUserRole(uRol);
                    return Ok(rows);
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating UserRole", e);
                return InternalServerError(e);
            }
        }
    }
}
