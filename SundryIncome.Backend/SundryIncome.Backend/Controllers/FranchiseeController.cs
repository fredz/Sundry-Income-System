﻿using AutoMapper;
using SundryIncome.Common.DTOs;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Domain.Entities;
using SundryIncome.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using SundryIncome.Backend.Models;
using SundryIncome.Repositories;

namespace SundryIncome.Backend.Controllers
{
    [RoutePrefix("Amc")]
	[EnableCors("*", "*", "*")]
	public class FranchiseeController : BaseApiController
    {
        private readonly IFranchiseeManager _fManager;
        private const string authCode = "F335C7EE410E437CAE0EE49CC93A84F5";

        public FranchiseeController()
        {
            _fManager = new FranchiseeManager(new FranchiseeRepository());
        }

        [Route("GetFranchiseeById")]
        [HttpPost]
        public IHttpActionResult GetFranchiseeById(FranchiseeModel franchisee)
        {
            try
            {
                if (franchisee != null && IsAuthorize(franchisee.Validator))
                {
                    _fManager.Configure(Logger);
                    var frFrachisee = _fManager.GetFranchiseeById(franchisee.FranchiseeId);

                    return Ok(frFrachisee); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened getting franchisee by id {franchisee.FranchiseeId}", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("DeleteFranchisee")]
        public IHttpActionResult DeleteFranchisee(FranchiseeModel franchisee)
        {
            try
            {
                if (franchisee != null && IsAuthorize(franchisee.Validator))
                {
                    Logger.InfoToPortal($"Deleting franchisee {franchisee.FranchiseeId}");
                    _fManager.Configure(Logger);
                    _fManager.DeleteFranchisee(franchisee.FranchiseeId);
                    return Ok("Delete Successfully"); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened deleting franchisee {franchisee.FranchiseeId}", e);
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetAllFranchisees")]
        public IHttpActionResult GetAllFranchisees(FranchiseeModel franchisee)
        {
            try
            {
                Logger.InfoToPortal("Getting all franchisees");
                _fManager.Configure(Logger);
                var allFranchisees = _fManager.GetAllFranchisees();
                return Ok(allFranchisees);
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened getting all Franchisees", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("AddFranchisee")]
        public IHttpActionResult AddFranchisee(FranchiseeModel franchisee)
        {
            try
            {
                if (franchisee != null && IsAuthorize(franchisee.Validator) )
                {
                    Franchisee fc = new Franchisee
                    {
                        Code = franchisee.Code, 
                        FirstName = franchisee.FirstName,
                        LastName = franchisee.LastName,
                        ContactName = franchisee.ContactName,
                        CompanyName = franchisee.CompanyName
                    };
                    Logger.InfoToPortal("Adding new Franchisee");
                    _fManager.Configure(Logger);
                    var nFc = _fManager.AddFranchisee(fc);

                    return Ok(nFc); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened adding new Franchisee", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateFranchisee")]
        public IHttpActionResult UpdateFranchisee(FranchiseeModel franchisee)
        {
            try
            {
                if (franchisee != null && IsAuthorize(franchisee.Validator))
                {
                    Franchisee fc = new Franchisee
                    {
                        FranchiseeId = franchisee.FranchiseeId,
                        Code = franchisee.Code,
                        FirstName = franchisee.FirstName,
                        LastName = franchisee.LastName,
                        ContactName = franchisee.ContactName,
                        CompanyName = franchisee.CompanyName
                    };
                    Logger.InfoToPortal("Updating Franchisee");
                    _fManager.Configure(Logger);
                    _fManager.UpdateFranchisee(fc);
                    return Ok("Update Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating franchisee", e);
                return InternalServerError(e);
            }
        }
    }
}
