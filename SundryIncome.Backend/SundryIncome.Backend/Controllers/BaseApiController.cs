﻿using System.Web.Http;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Backend.Controllers
{
    public class BaseApiController : ApiController
    {
        private ISundryIncomeLogger _logger;
        private const string authCode = "F335C7EE410E437CAE0EE49CC93A84F5";

        public ISundryIncomeLogger Logger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = new Common.Helpers.Log4NetSundryIncomeLogger();
                }

                return _logger;
            }
        }

        public bool IsAuthorize(string code)
        {
            return (authCode.Equals(code));
        }
    }
}
