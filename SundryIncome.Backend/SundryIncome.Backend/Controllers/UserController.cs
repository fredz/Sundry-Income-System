﻿using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using SundryIncome.Backend.Models;
using SundryIncome.Domain.Entities;
using SundryIncome.Repositories;

namespace SundryIncome.Backend.Controllers
{
    [RoutePrefix("Amc")]
    [EnableCors("*", "*", "*")]
    public class UserController : BaseApiController
    {
        private readonly IUserManager _userManager;
        public UserController()
        {
            _userManager = new UserManager(new UserRepository());
        }

        [Route("GetUserById")]
        [HttpPost]
        public IHttpActionResult GetUserById(UserModel user)
        {
            try
            {
                if (IsAuthorize(user.Validator))
                {
                    _userManager.Configure(Logger);
                    var us = _userManager.GetUserById(user.UserId);

                    return Ok(us);
                }
                else
                {
                    Logger.ErrorToLog($"Not Authorized. {user.Validator}");
                    return BadRequest("Missing or invalid Authorization");
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened getting user by id {user.UserId}", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("DeleteUser")]
        public IHttpActionResult DeleteUser(UserModel user)
        {
            try
            {
                if (IsAuthorize(user.Validator))
                {
                    Logger.InfoToPortal($"Deleting user {user.UserId}");
                    _userManager.Configure(Logger);
                    _userManager.DeleteUser(user.UserId);
                    return Ok("Delete Successfully");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened deleting user {user.UserId}", e);
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetAllUsers")]
        public IHttpActionResult GetAllUsers(UserModel user)
        {
            try
            {
                Logger.InfoToPortal("Getting all users");
                _userManager.Configure(Logger);
                var allUsers = _userManager.GetAllUsers();
                return Ok(allUsers);
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened getting all users", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("AddUser")]
        public IHttpActionResult AddUser(UserModel user)
        {
            try
            {
                if (IsAuthorize(user.Validator))
                {
                    User us = new User()
                    {
                        UserId = user.UserId,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email,
                        MobileNo = user.MobileNo,
                        Phone = user.Phone,
                        Username = user.UserName,
                        Password = user.Password,
                        Uid = null
                    };
                    Logger.InfoToPortal("Adding new user");
                    _userManager.Configure(Logger);
                    var nUser = _userManager.AddUser(us);

                    return Ok(nUser);
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened adding new User", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateUser")]
        public IHttpActionResult UpdateUser(UserModel user)
        {
            try
            {
                if (IsAuthorize(user.Validator))
                {
                    User us = new User()
                    {
                        UserId = user.UserId,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email,
                        MobileNo = user.MobileNo,
                        Phone = user.Phone,
                        Username = user.UserName,
                        Password = user.Password,
                        Uid = user.Uid
                    };
                    Logger.InfoToPortal("Updating User");
                    _userManager.Configure(Logger);
                    _userManager.UpdateUser(us);
                    return Ok("Update Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating user", e);
                return InternalServerError(e);
            }
        }

        [Route("VerifyIfUsernameExists")]
        [HttpPost]
        public IHttpActionResult VerifyIfUsernameExists(UserModel user)
        {
            try
            {
                if (IsAuthorize(user.Validator))
                {
                    _userManager.Configure(Logger);
                    var us = _userManager.VerifyIfUserNameExists(user.UserName);

                    return Ok(us);
                }
                else
                {
                    Logger.ErrorToLog($"Not Authorized. {user.Validator}");
                    return BadRequest("Missing or invalid Authorization");
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened verifying if username exists {user.UserName}", e);
                return InternalServerError(e);
            }
        }


        [Route("LogoutUser")]
        [HttpPost]
        public IHttpActionResult LogoutUser(UserModel user)
        {
            try
            {
                if (IsAuthorize(user.Validator))
                {
                    _userManager.Configure(Logger);
                    _userManager.LogoutUser(user.UserName, user.UserId);

                    return Ok("Logout Succesfully");
                }
                else
                {
                    Logger.ErrorToLog($"Not Authorized. {user.Validator}");
                    return BadRequest("Missing or invalid Authorization");
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened verifying if username exists {user.UserName}", e);
                return InternalServerError(e);
            }
        }

        [Route("ValidateUser")]
        [HttpPost]
        public IHttpActionResult ValidateUser(UserModel user)
        {
            try
            {
                if (IsAuthorize(user.Validator))
                {
                    _userManager.Configure(Logger);
                    var res = _userManager.ValidateUser(user.UserName, user.Password);

                    return Ok(res);
                }
                else
                {
                    Logger.ErrorToLog($"Not Authorized. {user.Validator}");
                    return BadRequest("Missing or invalid Authorization");
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened validating user {user.UserName}", e);
                return InternalServerError(e);
            }
        }

    }
}