﻿using AutoMapper;
using SundryIncome.Common.DTOs;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Domain.Entities;
using SundryIncome.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using SundryIncome.Backend.Models;
using SundryIncome.Repositories;

namespace SundryIncome.Backend.Controllers
{
    [RoutePrefix("Amc")]
    [EnableCors("*", "*", "*")]
    public class JobProgressStatusController : BaseApiController
    {
        private readonly IJobProgressStatusManager _jManager;

        public JobProgressStatusController()
        {
            _jManager = new JobProgressStatusManager(new JobProgressStatusRepository());
        }

        [HttpGet]
        [Route("GetAllJobProgressStatus")]
        public IHttpActionResult GetAllJobProgressStatus(JobProgressStatusModel jobProgressStatus)
        {
            try
            {
                Logger.InfoToPortal("Getting all jobProgressStatus");
                _jManager.Configure(Logger);
                var allJobs = _jManager.GetAllJobProgressStatus();
                return Ok(allJobs);
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened getting all jobProgressStatus", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("AddJobProgressStatus")]
        public IHttpActionResult CreateJobProgressStatus(JobProgressStatusModel jobProgressStatus)
        {
            try
            {
                if (jobProgressStatus != null && IsAuthorize(jobProgressStatus.Validator))
                {
                    JobProgressStatus nJobProgressStatus = new JobProgressStatus
                    {
                        CustomerId = jobProgressStatus.CustomerId,
                        AreaManagerId = jobProgressStatus.AreaManagerId,
                        OrderNo = jobProgressStatus.OrderNo,
                        Description = jobProgressStatus.Description,
                        RequestedDate = jobProgressStatus.RequestedDate,
                        DateFrom = jobProgressStatus.DateFrom,
                        DateTo = jobProgressStatus.DateTo,
                        //DateCompleted = job.DateCompleted,
                        AMCAmount = jobProgressStatus.AMCAmount,
                        // CreatedOn = job.CreatedOn,
                        CreatedBy = jobProgressStatus.CreatedBy,
                        // ModifiedOn = job.ModifiedOn,
                        // ModifiedBy = job.ModifiedBy,
                        // SalesInvoiceNo = job.SalesInvoiceNo,
                        // SalesInvoiceDate = job.SalesInvoiceDate
                        StepNo = jobProgressStatus.StepNo,
                        JobStatusId = jobProgressStatus.JobStatusId,                        
                        //Name = jobProgressStatus.Name,
                        //ColorDate = jobProgressStatus.ColorDate,
                        //Rank = jobProgressStatus.Rank
                    };
                    Logger.InfoToPortal("Adding new jobProgressStatus");
                    _jManager.Configure(Logger);
                    var jobProgressStatusAdded = _jManager.AddJobProgressStatus(nJobProgressStatus);

                    return Ok(jobProgressStatusAdded);
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened adding new jobProgressStatus", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateJobProgressStatus")]
        public IHttpActionResult UpdateJobProgressStatus(JobProgressStatusModel jobProgressStatusModel)
        {
            try
            {
                if (jobProgressStatusModel != null && IsAuthorize(jobProgressStatusModel.Validator))
                {
                    JobProgressStatus js = new JobProgressStatus()
                    {
                        JobProgressId = jobProgressStatusModel.JobProgressId,
                        JobId = jobProgressStatusModel.JobId,
                        JobStatusId = jobProgressStatusModel.JobStatusId                        
                    };
                    Logger.InfoToPortal("Updating JobProgressStatus");
                    _jManager.Configure(Logger);
                    _jManager.UpdateJobProgressStatus(js);
                    return Ok("UpdateJobProgressStatus Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating jobStatus", e);
                return InternalServerError(e);
            }
        }

    }
}
