﻿using AutoMapper;
using SundryIncome.Common.DTOs;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Domain.Entities;
using SundryIncome.Managers;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using SundryIncome.Backend.Models;
using SundryIncome.Repositories;

namespace SundryIncome.Backend.Controllers
{
    [RoutePrefix("Amc")]
	[EnableCors("*", "*", "*")]
	public class SubContractorController : BaseApiController
    {
        private readonly ISubContractorManager _sManager;
        private const string authCode = "F335C7EE410E437CAE0EE49CC93A84F5";

        public SubContractorController()
        {
            _sManager = new SubContractorManager(new SubContractorRepository());
        }

        [Route("GetSubContractorById")]
        [HttpPost]
        public IHttpActionResult GetSubContractorById(SubContractorModel sContractor)
        {
            try
            {
                if (IsAuthorize(sContractor.Validator))
                {
                    _sManager.Configure(Logger);
                    var subContractor = _sManager.GetSubContractorById(sContractor.SubContractorId);

                    return Ok(subContractor); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened getting subcontractor by id {sContractor.SubContractorId}", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("DeleteSubContractor")]
        public IHttpActionResult DeleteSubContractor(SubContractorModel sContractor)
        {
            try
            {
                if (IsAuthorize(sContractor.Validator))
                {
                    Logger.InfoToPortal($"Deleting subcontractor {sContractor.SubContractorId}");
                    _sManager.Configure(Logger);
                    _sManager.DeleteSubContractor(sContractor.SubContractorId);
                    return Ok("Delete Successfully"); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog($"Unexpected error happened deleting subcontractor {sContractor.SubContractorId}", e);
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("GetAllSubContractors")]
        public IHttpActionResult GetAllSubContractors(SubContractorModel sContractor)
        {
            try
            {
                Logger.InfoToPortal("Getting all subcontractors");
                _sManager.Configure(Logger);
                var allSubContractors = _sManager.GetAllSubContractors();
                return Ok(allSubContractors);
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened getting all subcontractors", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("AddSubContractor")]
        public IHttpActionResult AddSubContractor(SubContractorModel sContractor)
        {
            try
            {
                if (IsAuthorize(sContractor.Validator))
                {
                    SubContractor sc = new SubContractor()
                    {
                        SubContractorId = sContractor.SubContractorId,
                        FirstName = sContractor.FirstName,
                        LastName = sContractor.LastName,
                        CompanyName = sContractor.CompanyName
                    };
                    Logger.InfoToPortal("Adding new Subcontractor");
                    _sManager.Configure(Logger);
                    var nSubContractor = _sManager.AddSubContractor(sc);

                    return Ok(nSubContractor); 
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened adding new SubContractor", e);
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("UpdateSubContractor")]
        public IHttpActionResult UpdateSubContractor(SubContractorModel sContractor)
        {
            try
            {
                if (IsAuthorize(sContractor.Validator))
                {
                    SubContractor sc = new SubContractor()
                    {
                        SubContractorId = sContractor.SubContractorId,
                        FirstName = sContractor.FirstName,
                        LastName = sContractor.LastName,
                        CompanyName = sContractor.CompanyName
                    };
                    Logger.InfoToPortal("Updating SubContractor");
                    _sManager.Configure(Logger);
                    _sManager.UpdateSubContractor(sc);
                    return Ok("Update Succesfull");
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception e)
            {
                Logger.ErrorToLog("Unexpected error happened updating subcontractor", e);
                return InternalServerError(e);
            }
        }
    }
}
