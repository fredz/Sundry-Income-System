﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SundryIncome.Backend.Models
{
    public class CustomerModel
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BillingName { get; set; }
        public string Email { get; set; }
        public int Phone { get; set; }
        public int MobileNo { get; set; }
        public bool PurchaseOrderRequired { get; set; }
        public bool AMCInvoiceOnly { get; set; }
        public string DefaultInvoiceDescription { get; set; }

        [Required]
        public string Validator { get; set; }
    }
}