﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SundryIncome.Backend.Models
{
    public class JobProgressStatusModel
    {
        public int JobId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerFullName { get; set; }
        public int AreaManagerId { get; set; }
        public string AManagerFullName { get; set; }
        public int OrderNo { get; set; }
        public string Description { get; set; }
        public DateTime? RequestedDate { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public DateTime? DateCompleted { get; set; }
        public float? AMCAmount { get; set; }
        public int? SalesInvoiceNo { get; set; }
        public DateTime? SalesInvoiceDate { get; set; }
        public int JobProgressId { get; set; }
        public int StepNo { get; set; }
        public int JobStatusId { get; set; }
        public string Name { get; set; }
        public string ColorCode { get; set; }
        public int Rank { get; set; }
                
        public DateTime? CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int ModifiedBy { get; set; }

        [Required]
        public string Validator { get; set; }
    }
}