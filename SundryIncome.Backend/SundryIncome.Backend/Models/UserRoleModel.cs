﻿using System.ComponentModel.DataAnnotations;

namespace SundryIncome.Backend.Models
{
    public class UserRoleModel
    {
        public int UserRoleId { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public int Disabled { get; set; }

        [Required]
        public string Validator { get; set; }
    }
}