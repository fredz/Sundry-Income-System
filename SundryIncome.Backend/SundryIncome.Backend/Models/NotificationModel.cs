﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SundryIncome.Backend.Models
{
    public class NotificationModel
    {
        public int NotificationId { get; set; }
        public int JobId { get; set; }
        public DateTime Date { get; set; }
        public string Content { get; set; }
        public string SendTo { get; set; }
        public string TypeNotification { get; set; }

        [Required]
        public string Validator { get; set; }
    }
}