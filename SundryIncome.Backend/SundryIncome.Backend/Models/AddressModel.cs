﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SundryIncome.Backend.Models
{
    public class AddressModel
    {
        public int AddressId { get; set; }
        public string Addresss { get; set; }
        public string Locality { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        public int CustomerId { get; set; }
        public string Type { get; set; }
        public string BillingName { get; set; }

        [Required]
        public string Validator { get; set; }
    }
}