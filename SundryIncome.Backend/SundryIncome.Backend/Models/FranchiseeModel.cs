﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SundryIncome.Backend.Models
{
    public class FranchiseeModel
    {
        public int FranchiseeId { get; set; }
        public int Code { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactName { get; set; }

        public string CompanyName { get; set; }

        [Required]
        public string Validator { get; set; }
    }
}