﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SundryIncome.Backend.Models
{
    public class JobAssignmentModel
    {
        public int JobAssignId { get; set; }
        public int JobId { get; set; }
        public int FranchiseeId { get; set; }
        public int SubcontractorId { get; set; }
        public float Amount { get; set; }
        public int PurchaseInvoiceNo { get; set; }
        public bool Primary { get; set; }

        /* parameter for franchisee */
        public string FranchiseeFullName { get; set; }
        public string CompanyFranchisee { get; set; }

        /* parameter for subcontractor */
        public string SubcontractorFullName { get; set; }
        public string CompanySubcontractor { get; set; }

        [Required]
        public string Validator { get; set; }
    }
}