﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SundryIncome.Backend.Models
{
    public class JobProgressModel
    {
        public int JobProgressId { get; set; }
        public int JobId { get; set; }
        public int JobStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public int StepNo { get; set; }

        [Required]
        public string Validator { get; set; }
    }
}