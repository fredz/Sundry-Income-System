﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SundryIncome.Backend.Models
{
    public class SubContractorModel
    {
        public int SubContractorId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }

        [Required]
        public string Validator { get; set; }
    }
}