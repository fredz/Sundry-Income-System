﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SundryIncome.Backend.Models
{
    public class JobStatusModel
    {
        public int JobStatusId { get; set; }
        public string Name { get; set; }
        public string ColorCode { get; set; }
        public int Rank { get; set; }

        [Required]
        public string Validator { get; set; }
    }
}