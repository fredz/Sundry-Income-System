﻿using SundryIncome.Domain.Entities;
using SundryIncome.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Common.Interfaces.Repositories;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Managers
{
    public class JobStatusManager: BaseManager, IJobStatusManager
    {
        private readonly IJobStatusRepository _jRepository;

        public JobStatusManager(IJobStatusRepository jRepository)
        {
            _jRepository = jRepository;
        }

        public override void Configure(ISundryIncomeLogger logger)
        {
            base.Configure(logger);
            _jRepository.Configure(logger);
        }

        public IList<JobStatus> GetallJobStatuses()
        {
            try
            {
                _logger.InfoToPortal("Getting all jobstatuses - Services layer");
                return _jRepository.GetAllJobStatuses();
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog("Error getting all jobstatuses from database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog("Error getting all jobstatuses from database", e);
                throw new SundryIncomeException("Error getting all jobstatuses from database", e);
            }
        }
        public JobStatus GetJobStatusById(int jStatusId)
        {
            try
            {
                return _jRepository.GetById(jStatusId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToPortal($"Error getting jobstatus by id {jStatusId}", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal($"Error getting jobstatus by id {jStatusId}", e);
                throw new SundryIncomeException($"Error getting jobstatus by id {jStatusId}", e);
            }
        }

        public void UpdateJobStatus(JobStatus jStatus)
        {
            try
            {
                _jRepository.Update(jStatus);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating jobstatus {jStatus.JobStatusId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating jobstatus {jStatus.JobStatusId} in business layer", e);
                throw new SundryIncomeException($"Error updating jobstatus {jStatus.JobStatusId} in business layer", e);
            }
        }

        public void DeleteJobStatus(int id)
        {
            try
            {
                _jRepository.Delete(id);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error deleting jobstus {id} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error deleting jobstatus {id} in business layer", e);
                throw new SundryIncomeException($"Error deleting jobstatus {id} in business layer", e);
            }
        }

        public JobStatus AddJobStatus(JobStatus jStatus)
        {
            JobStatus resJobStatus;

            try
            {
                _logger.InfoToPortal(
                    $"Adding jobstatus  {jStatus.Name + " " + jStatus.ColorCode} - Services layer");
                resJobStatus = _jRepository.AddJobStatus(jStatus);
            }
            catch (Exception exception)
            {
                _logger.ErrorToPortal(
                    $"Error adding jobstatus {jStatus.Name + " " + jStatus.ColorCode} on database ",
                    exception);
                throw;
            }

            return resJobStatus;
        }
    }
}
