﻿using SundryIncome.Domain.Entities;
using SundryIncome.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Common.Interfaces.Repositories;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Managers
{
    public class UserRoleManager: BaseManager, IUserRoleManager
    {
        private readonly IUserRoleRepository _userRoleRepository;

        public UserRoleManager(IUserRoleRepository userRoleRepository)
        {
            _userRoleRepository = userRoleRepository;
        }

        public override void Configure(ISundryIncomeLogger logger)
        {
            base.Configure(logger);
            _userRoleRepository.Configure(logger);
        }

        public IList<UserRole> GetUserRolesByUserId(int userRoleId)
        {
            try
            {
                return _userRoleRepository.GetByUserId(userRoleId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToPortal($"Error getting UserRole by id {userRoleId}", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal($"Error getting UserRole by id {userRoleId}", e);
                throw new SundryIncomeException($"Error getting UserRole by id {userRoleId}", e);
            }
        }

        public int UpdateUserRole(UserRole userRole)
        {
            try
            {
                return _userRoleRepository.Update(userRole);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating UserRole {userRole.UserId} - {userRole.RoleId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating UserRole {userRole.UserRoleId} in business layer", e);
                throw new SundryIncomeException($"Error updating UserRole {userRole.UserRoleId} in business layer", e);
            }
        }

        public void DeleteUserRole(int userId, int roleId)
        {
            try
            {
                _userRoleRepository.Delete(userId, roleId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error deleting UserRole {userId + " " + roleId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error deleting UserRole {userId + " " + roleId} in business layer", e);
                throw new SundryIncomeException($"Error deleting UserRole {userId + " " + roleId} in business layer", e);
            }
        }

        public UserRole AddUserRole(UserRole userRole)
        {
            UserRole resultRole;

            try
            {
                _logger.InfoToPortal(
                    $"Adding UserRole  {userRole.UserId + " " + userRole.RoleId} - Services layer");
                resultRole = _userRoleRepository.AddUserRole(userRole);
            }
            catch (Exception exception)
            {
                _logger.ErrorToPortal(
                    $"Error adding UserRole {userRole.UserId + " " + userRole.RoleId} on database ",
                    exception);
                throw;
            }

            return resultRole;
        }
    }
}
