﻿using SundryIncome.Domain.Entities;
using SundryIncome.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Common.Interfaces.Repositories;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Managers
{
    public class AddressManager: BaseManager, IAddressManager
    {
        private readonly IAddressRepository _aRepository;

        public AddressManager(IAddressRepository aRepository)
        {
            _aRepository = aRepository;
        }

        public override void Configure(ISundryIncomeLogger logger)
        {
            base.Configure(logger);
            _aRepository.Configure(logger);
        }

        public IList<Address> GetAllAddresses()
        {
            try
            {
                _logger.InfoToPortal("Getting all address - Services layer");
                return _aRepository.GetAllAddresses();
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog("Error getting all address from database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog("Error getting all address from database", e);
                throw new SundryIncomeException("Error getting all address from database", e);
            }
        }
        public Address GetAddressById(int aId)
        {
            try
            {
                return _aRepository.GetById(aId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToPortal($"Error getting address by id {aId}", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal($"Error getting address by id {aId}", e);
                throw new SundryIncomeException($"Error getting address by id {aId}", e);
            }
        }

        public void UpdateAddress(Address address)
        {
            try
            {
                _aRepository.Update(address);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating address {address.AddressId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating address {address.AddressId} in business layer", e);
                throw new SundryIncomeException($"Error updating subcontractor {address.AddressId} in business layer", e);
            }
        }

        public void DeleteAddress(int id)
        {
            try
            {
                _aRepository.Delete(id);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error deleting address {id} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error deleting address {id} in business layer", e);
                throw new SundryIncomeException($"Error deleting address {id} in business layer", e);
            }
        }

        public Address AddAddress(Address address)
        {
            Address ad;

            try
            {
                _logger.InfoToPortal(
                    $"Adding address  {address.Addresss + " " + address.PostalCode} - Services layer");
                ad = _aRepository.AddAddress(address);
            }
            catch (Exception exception)
            {
                _logger.ErrorToPortal(
                    $"Error adding address {address.Addresss + " " + address.PostalCode} on database ",
                    exception);
                throw;
            }

            return ad;
        }

        public IList<Address> GetAddressByCustomerId(int customerId)
        {
            try
            {
                return _aRepository.GetAddressByCustomerId(customerId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToPortal($"Error getting Address by customer id {customerId}", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal($"Error getting Address by id {customerId}", e);
                throw new SundryIncomeException($"Error getting Address by customer id {customerId}", e);
            }
        }
    }
}
