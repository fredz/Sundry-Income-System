﻿using SundryIncome.Common.Interfaces.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Managers
{
   public class BaseManager : IBaseManager
    {
        protected ISundryIncomeLogger _logger;
        
        public ISundryIncomeLogger Logger
        {
            get
            {
                return _logger;
            }
        }
        public virtual void Configure(ISundryIncomeLogger logger)
        {
            _logger = logger;
        }
    }
}
