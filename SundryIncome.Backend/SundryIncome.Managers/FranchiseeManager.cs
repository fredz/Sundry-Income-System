﻿using SundryIncome.Domain.Entities;
using SundryIncome.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Common.Interfaces.Repositories;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Managers
{
    public class FranchiseeManager: BaseManager, IFranchiseeManager
    {
        private readonly IFranchiseeRepository _fRepository;

        public FranchiseeManager(IFranchiseeRepository fRepository)
        {
            _fRepository = fRepository;
        }

        public override void Configure(ISundryIncomeLogger logger)
        {
            base.Configure(logger);
            _fRepository.Configure(logger);
        }

        public IList<Franchisee> GetAllFranchisees()
        {
            try
            {
                _logger.InfoToPortal("Getting all franchisee - Services layer");
                return _fRepository.GetAllFranchisees();
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog("Error getting all franchisees from database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog("Error getting all franchisees from database", e);
                throw new SundryIncomeException("Error getting all franchisees from database", e);
            }
        }
        public Franchisee GetFranchiseeById(int sId)
        {
            try
            {
                return _fRepository.GetById(sId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToPortal($"Error getting franchisee by id {sId}", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal($"Error getting franchisee by id {sId}", e);
                throw new SundryIncomeException($"Error getting franchisee by id {sId}", e);
            }
        }

        public void UpdateFranchisee(Franchisee franchisee)
        {
            try
            {
                _fRepository.Update(franchisee);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating franchisee {franchisee.FranchiseeId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating franchisee {franchisee.FranchiseeId} in business layer", e);
                throw new SundryIncomeException($"Error updating subcontractor {franchisee.FranchiseeId} in business layer", e);
            }
        }

        public void DeleteFranchisee(int id)
        {
            try
            {
                _fRepository.Delete(id);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error deleting franchisee {id} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error deleting franchisee {id} in business layer", e);
                throw new SundryIncomeException($"Error deleting franchisee {id} in business layer", e);
            }
        }

        public Franchisee AddFranchisee(Franchisee franchisee)
        {
            Franchisee fr;

            try
            {
                _logger.InfoToPortal(
                    $"Adding franchisee  {franchisee.FirstName + " " + franchisee.LastName} - Services layer");
                fr = _fRepository.AddFranchisee(franchisee);
            }
            catch (Exception exception)
            {
                _logger.ErrorToPortal(
                    $"Error adding franchisee {franchisee.FirstName + " " + franchisee.LastName} on database ",
                    exception);
                throw;
            }

            return fr;
        }
    }
}
