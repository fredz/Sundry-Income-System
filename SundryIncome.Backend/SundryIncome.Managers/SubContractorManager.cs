﻿using SundryIncome.Domain.Entities;
using SundryIncome.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Common.Interfaces.Repositories;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Managers
{
    public class SubContractorManager: BaseManager, ISubContractorManager
    {
        private readonly ISubContractorRepository _sRepository;

        public SubContractorManager(ISubContractorRepository sRepository)
        {
            _sRepository = sRepository;
        }

        public override void Configure(ISundryIncomeLogger logger)
        {
            base.Configure(logger);
            _sRepository.Configure(logger);
        }

        public IList<SubContractor> GetAllSubContractors()
        {
            try
            {
                _logger.InfoToPortal("Getting all subcontractors - Services layer");
                return _sRepository.GetAllSubContractors();
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog("Error getting all subcontractors from database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog("Error getting all subcontractors from database", e);
                throw new SundryIncomeException("Error getting all subcontractors from database", e);
            }
        }
        public SubContractor GetSubContractorById(int sId)
        {
            try
            {
                return _sRepository.GetById(sId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToPortal($"Error getting subcontractor by id {sId}", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal($"Error getting subcontractor by id {sId}", e);
                throw new SundryIncomeException($"Error getting subcontractor by id {sId}", e);
            }
        }

        public void UpdateSubContractor(SubContractor sContractor)
        {
            try
            {
                _sRepository.Update(sContractor);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating subcontractor {sContractor.SubContractorId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating role {sContractor.SubContractorId} in business layer", e);
                throw new SundryIncomeException($"Error updating subcontractor {sContractor.SubContractorId} in business layer", e);
            }
        }

        public void DeleteSubContractor(int id)
        {
            try
            {
                _sRepository.Delete(id);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error deleting subcontractor {id} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error deleting subcontractor {id} in business layer", e);
                throw new SundryIncomeException($"Error deleting subcontractor {id} in business layer", e);
            }
        }

        public SubContractor AddSubContractor(SubContractor sContractor)
        {
            SubContractor rSubContractor;

            try
            {
                _logger.InfoToPortal(
                    $"Adding Subcontractor  {sContractor.FirstName + " " + sContractor.LastName} - Services layer");
                rSubContractor = _sRepository.AddSubContractor(sContractor);
            }
            catch (Exception exception)
            {
                _logger.ErrorToPortal(
                    $"Error adding subcontractor {sContractor.FirstName + " " + sContractor.LastName} on database ",
                    exception);
                throw;
            }

            return rSubContractor;
        }
    }
}
