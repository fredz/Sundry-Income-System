﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Helpers;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Common.Interfaces.Repositories;
using SundryIncome.Domain.Entities;

namespace SundryIncome.Managers
{
    public class UserManager : BaseManager, IUserManager
    {
        private readonly IUserRepository _userRepository;

        public UserManager(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public override void Configure(ISundryIncomeLogger logger)
        {
            base.Configure(logger);
            _userRepository.Configure(logger);
        }

        public User GetUserById(int userId)
        {
            try
            {
                return _userRepository.GetUserById(userId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToPortal($"Error getting user by id {userId}", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal($"Error getting user by id {userId}", e);
                throw new SundryIncomeException($"Error getting user by id {userId}", e);
            }
        }

        public void UpdateUser(User user)
        {
            try
            {
                _userRepository.UpdateUser(user);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating user {user.UserId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating user {user.UserId} in business layer", e);
                throw new SundryIncomeException($"Error updating role {user.UserId} in business layer", e);
            }
        }

        public void DeleteUser(int id)
        {
            try
            {
                _userRepository.DeleteUser(id);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error deleting user {id} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error deleting user {id} in business layer", e);
                throw new SundryIncomeException($"Error deleting user {id} in business layer", e);
            }
        }

        public User AddUser(User user)
        {
            User resultUser;

            try
            {
                _logger.InfoToPortal(
                    $"Adding user  {user.FirstName + " " + user.LastName} - Services layer");
                resultUser = _userRepository.AddUser(user);
            }
            catch (Exception exception)
            {
                _logger.ErrorToPortal(
                    $"Error adding user {user.FirstName + " " + user.LastName} on database ",
                    exception);
                throw;
            }

            return resultUser;
        }

        public IList<User> GetAllUsers()
        {
            try
            {
                _logger.InfoToPortal("Getting all users - Services layer");
                return _userRepository.GetAllUsers();
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog("Error getting all users from database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog("Error getting all users from database", e);
                throw new SundryIncomeException("Error getting all users from database", e);
            }
        }
        public bool VerifyIfUserNameExists(string userName)
        {
            try
            {
                return _userRepository.VerifyIfUserNameExists(userName);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error verifying user {userName} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error verifying user {userName} in business layer", e);
                throw new SundryIncomeException($"Error verifying user {userName} in business layer", e);
            }
        }

        public void LogoutUser(string userName, int userId)
        {
            try
            {
                _userRepository.Logout(userName, userId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error trying to logout user {userName} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error trying to logout user {userName} in business layer", e);
                throw new SundryIncomeException($"Error trying to logout {userName} in business layer", e);
            }
        }

        public User ValidateUser(string userName, string password)
        {
            try
            {
               return _userRepository.Validate(userName, password);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error trying to validte user {userName} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error trying to validate user {userName} in business layer", e);
                throw new SundryIncomeException($"Error trying to validate {userName} in business layer", e);
            }
        }
    }
}
