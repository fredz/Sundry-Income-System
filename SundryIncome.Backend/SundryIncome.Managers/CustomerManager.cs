﻿using SundryIncome.Domain.Entities;
using SundryIncome.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Common.Interfaces.Repositories;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Managers
{
    public class CustomerManager: BaseManager, ICustomerManager
    {
        private readonly ICustomerRepository _cRepository;

        public CustomerManager(ICustomerRepository cRepository)
        {
            _cRepository = cRepository;
        }

        public override void Configure(ISundryIncomeLogger logger)
        {
            base.Configure(logger);
            _cRepository.Configure(logger);
        }

        public IList<Customer> GetAllCustomers()
        {
            try
            {
                _logger.InfoToPortal("Getting all customers - Services layer");
                return _cRepository.GetAllCustomers();
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog("Error getting all customers from database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog("Error getting all customers from database", e);
                throw new SundryIncomeException("Error getting all customers from database", e);
            }
        }
        public Customer GetCustomerById(int cId)
        {
            try
            {
                return _cRepository.GetById(cId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToPortal($"Error getting customer by id {cId}", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal($"Error getting customer by id {cId}", e);
                throw new SundryIncomeException($"Error getting franchisee by id {cId}", e);
            }
        }

        public void UpdateCustomer(Customer customer)
        {
            try
            {
                _cRepository.Update(customer);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating customer {customer.CustomerId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating customer {customer.CustomerId} in business layer", e);
                throw new SundryIncomeException($"Error updating customer {customer.CustomerId} in business layer", e);
            }
        }

        public void DeleteCustomer(int id)
        {
            try
            {
                _cRepository.Delete(id);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error deleting customer {id} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error deleting customer {id} in business layer", e);
                throw new SundryIncomeException($"Error deleting costumer {id} in business layer", e);
            }
        }

        public Customer AddCustomer(Customer customer)
        {
            Customer cst;

            try
            {
                _logger.InfoToPortal(
                    $"Adding customer  {customer.FirstName + " " + customer.LastName} - Services layer");
                cst = _cRepository.AddCustomer(customer);
            }
            catch (Exception exception)
            {
                _logger.ErrorToPortal(
                    $"Error adding customer {customer.FirstName + " " + customer.LastName} on database ",
                    exception);
                throw;
            }

            return cst;
        }
    }
}
