﻿using SundryIncome.Domain.Entities;
using SundryIncome.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Common.Interfaces.Repositories;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Managers
{
    public class RoleManager: BaseManager, IRoleManager
    {
        private readonly IRoleRepository _roleRepository;

        public RoleManager(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public override void Configure(ISundryIncomeLogger logger)
        {
            base.Configure(logger);
            _roleRepository.Configure(logger);
        }

        public IList<Role> GetAllRoles()
        {
            try
            {
                _logger.InfoToPortal("Getting all roles - Services layer");
                return _roleRepository.GetAllRoles();
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog("Error getting all roles from database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog("Error getting all roles from database", e);
                throw new SundryIncomeException("Error getting all roles from database", e);
            }
        }
        public Role GetRoleById(int roleId)
        {
            try
            {
                return _roleRepository.GetById(roleId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToPortal($"Error getting role by id {roleId}", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal($"Error getting role by id {roleId}", e);
                throw new SundryIncomeException($"Error getting role by id {roleId}", e);
            }
        }

        public void UpdateRole(Role role)
        {
            try
            {
                _roleRepository.Update(role);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating role {role.RoleId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating role {role.RoleId} in business layer", e);
                throw new SundryIncomeException($"Error updating role {role.RoleId} in business layer", e);
            }
        }

        public void DeleteRole(int id)
        {
            try
            {
                _roleRepository.Delete(id);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error deleting client {id} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error deleting role {id} in business layer", e);
                throw new SundryIncomeException($"Error deleting role {id} in business layer", e);
            }
        }

        public Role AddRole(Role role)
        {
            Role resultRole;

            try
            {
                _logger.InfoToPortal(
                    $"Adding role  {role.Name + " " + role.RoleId} - Services layer");
                resultRole = _roleRepository.AddRole(role);
            }
            catch (Exception exception)
            {
                _logger.ErrorToPortal(
                    $"Error adding role {role.RoleId + " " + role.Name} on database ",
                    exception);
                throw;
            }

            return resultRole;
        }
    }
}
