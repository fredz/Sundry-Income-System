﻿using SundryIncome.Domain.Entities;
using SundryIncome.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Common.Interfaces.Repositories;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Managers
{
    public class JobManager: BaseManager, IJobManager
    {
        private readonly IJobRepository _jRepository;

        public JobManager(IJobRepository jRepository)
        {
            _jRepository = jRepository;
        }

        public override void Configure(ISundryIncomeLogger logger)
        {
            base.Configure(logger);
            _jRepository.Configure(logger);
        }

        public IList<Job> GetAllJobs()
        {
            try
            {
                _logger.InfoToPortal("Getting all jobs - Services layer");
                return _jRepository.GetAllJobs();
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog("Error getting all jobs from database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog("Error getting all jobs from database", e);
                throw new SundryIncomeException("Error getting all jobs from database", e);
            }
        }
        public Job GetJobById(int jId)
        {
            try
            {
                return _jRepository.GetById(jId);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToPortal($"Error getting jobs by id {jId}", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal($"Error getting jobs by id {jId}", e);
                throw new SundryIncomeException($"Error getting jobs by id {jId}", e);
            }
        }

        public void UpdateJob(Job job)
        {
            try
            {
                _jRepository.UpdateCreateJob(job);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating job {job.JobId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating job {job.JobId} in business layer", e);
                throw new SundryIncomeException($"Error updating job {job.JobId} in business layer", e);
            }
        }

        public void DeleteCreatedJob(int id)
        {
            try
            {
                _jRepository.Delete(id);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error deleting job {id} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error deleting job {id} in business layer", e);
                throw new SundryIncomeException($"Error deleting job {id} in business layer", e);
            }
        }

        public Job AddJob(Job job)
        {
            Job nJob;

            try
            {
                _logger.InfoToPortal(
                    $"Adding job  {job.CustomerId + " " + job.AreaManagerId} - Services layer");
                nJob = _jRepository.CreateJob(job);
            }
            catch (Exception exception)
            {
                _logger.ErrorToPortal(
                    $"Error adding job {job.CustomerId + " " + job.AreaManagerId} on database ",
                    exception);
                throw;
            }

            return nJob;
        }
    }
}
