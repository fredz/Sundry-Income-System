﻿using SundryIncome.Domain.Entities;
using SundryIncome.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Common.Interfaces.Repositories;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Managers
{
    public class JobAssignmentManager: BaseManager, IJobAssignmentManager
    {
        private readonly IJobAssignmentRepository _jRepository;

        public JobAssignmentManager(IJobAssignmentRepository jRepository)
        {
            _jRepository = jRepository;
        }

        public override void Configure(ISundryIncomeLogger logger)
        {
            base.Configure(logger);
            _jRepository.Configure(logger);
        }

        public IList<JobAssignment> GetJobAssignmentByJobId(int id)
        {
            try
            {
                _logger.InfoToPortal("Getting all jobs assignment - Services layer");
                return _jRepository.GetJobAssignmentByJobId(id);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog("Error getting all jobs assignment from database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog("Error getting all jobs assignment from database", e);
                throw new SundryIncomeException("Error getting all jobs assignment from database", e);
            }
        }
        
        public void UpdateJobAssignment(JobAssignment jobAssignment)
        {
            try
            {
                _jRepository.UpdateJobAssignment(jobAssignment);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating job assignment {jobAssignment.JobAssignId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating job assignment {jobAssignment.JobAssignId} in business layer", e);
                throw new SundryIncomeException($"Error updating job assignment {jobAssignment.JobAssignId} in business layer", e);
            }
        }

        public void UpdateJobAssignmentAmount(int jobAssignmentId, float amount)
        {
            try
            {
                _jRepository.UpdateJobAssignmentAmount(jobAssignmentId, amount);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating job assignment amount {jobAssignmentId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating job assignment amount {jobAssignmentId} in business layer", e);
                throw new SundryIncomeException($"Error updating job assignment amount {jobAssignmentId} in business layer", e);
            }
        }

        public void UpdateJobAssignmentPrimary(int jobAssignmentId, bool primary)
        {
            try
            {
                _jRepository.UpdateJobAssignmentPrimary(jobAssignmentId, primary);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating job assignment primary {jobAssignmentId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating job assignment primary {jobAssignmentId} in business layer", e);
                throw new SundryIncomeException($"Error updating job assignment primary {jobAssignmentId} in business layer", e);
            }
        }

        public void DeleteJobAssignment(int id)
        {
            try
            {
                _jRepository.DeleteJobAssignment(id);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error deleting job assignment {id} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error deleting job assignment {id} in business layer", e);
                throw new SundryIncomeException($"Error deleting job assignment {id} in business layer", e);
            }
        }

        public JobAssignment AddJobAssignment(JobAssignment jobAssignment)
        {
            JobAssignment nJobAssignment;

            try
            {
                _logger.InfoToPortal(
                    $"Adding job assigment  {jobAssignment.FranchiseeId + " " + jobAssignment.SubcontractorId} - Services layer");
                nJobAssignment = _jRepository.AddJobAssignment(jobAssignment);
            }
            catch (Exception exception)
            {
                _logger.ErrorToPortal(
                    $"Error adding job assignment {jobAssignment.FranchiseeId + " " + jobAssignment.SubcontractorId} on database ",
                    exception);
                throw;
            }

            return nJobAssignment;
        }
    }
}
