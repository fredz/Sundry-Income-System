﻿using SundryIncome.Domain.Entities;
using SundryIncome.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Managers;
using SundryIncome.Common.Interfaces.Repositories;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Managers
{
    public class JobProgressStatusManager: BaseManager, IJobProgressStatusManager
    {
        private readonly IJobProgressStatusRepository _jRepository;

        public JobProgressStatusManager(IJobProgressStatusRepository jRepository)
        {
            _jRepository = jRepository;
        }

        public override void Configure(ISundryIncomeLogger logger)
        {
            base.Configure(logger);
            _jRepository.Configure(logger);
        }

        public IList<JobProgressStatus> GetAllJobProgressStatus()
        {
            try
            {
                _logger.InfoToPortal("Getting all JobProgressStatus - Services layer");
                return _jRepository.GetAllJobProgressStatus();
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog("Error getting all JobProgressStatus from database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog("Error getting all JobProgressStatus from database", e);
                throw new SundryIncomeException("Error getting all JobProgressStatus from database", e);
            }
        }

        public JobProgressStatus AddJobProgressStatus(JobProgressStatus jobProgressStatus)
        {
            JobProgressStatus nJobProgressStatus;

            try
            {
                _logger.InfoToPortal(
                    $"Adding job  {jobProgressStatus.CustomerId + " " + jobProgressStatus.AreaManagerId} - Services layer");
                nJobProgressStatus = _jRepository.AddJobProgressStatus(jobProgressStatus);
            }
            catch (Exception exception)
            {
                _logger.ErrorToPortal(
                    $"Error adding job {jobProgressStatus.CustomerId + " " + jobProgressStatus.AreaManagerId} on database ",
                    exception);
                throw;
            }

            return nJobProgressStatus;
        }

        public void UpdateJobProgressStatus(JobProgressStatus jobProgressStatus)
        {
            try
            {
                _jRepository.UpdateJobProgressStatus(jobProgressStatus);
            }
            catch (SundryIncomeException sie)
            {
                _logger.ErrorToLog($"Error updating jobProgress {jobProgressStatus.JobId} in database", sie);
                throw;
            }
            catch (Exception e)
            {
                _logger.ErrorToLog($"Error updating jobProgress {jobProgressStatus.JobId} in business layer", e);
                throw new SundryIncomeException($"Error updating jobProgress {jobProgressStatus.JobId} in business layer", e);
            }
        }

    }
}
