﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.DTOs
{
   public class RoleResponseDto
    {
        public int resultCode { get; set; }

        public string message { get; set; }

        public RoleDto roleCreated { get; set; }
    }
}
