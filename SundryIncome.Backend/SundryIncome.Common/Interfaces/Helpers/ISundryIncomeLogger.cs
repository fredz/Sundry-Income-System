﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Helpers
{
    public interface ISundryIncomeLogger
    {
        void DebugToPortal(string message);
        void InfoToPortal(string message);
        void WarnToPortal(string message);
        void ErrorToPortal(string message);
        void DebugToPortal(string message, Exception e);
        void InfoToPortal(string message, Exception e);
        void WarnToPortal(string message, Exception e);
        void ErrorToPortal(string message, Exception e);

        void DebugToLog(string message);
        void InfoToLog(string message);
        void WarnToLog(string message);
        void ErrorToLog(string message);

        void DebugToLog(string message, Exception e);
        void InfoToLog(string message, Exception e);
        void WarnToLog(string message, Exception e);
        void ErrorToLog(string message, Exception e);
    }
}
