﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface ICustomerManager : IBaseManager
    {
        Customer GetCustomerById(int frId);
        Customer AddCustomer(Customer customer);

        void UpdateCustomer(Customer customer);
        void DeleteCustomer(int id);
        IList<Customer> GetAllCustomers();
    }
}
