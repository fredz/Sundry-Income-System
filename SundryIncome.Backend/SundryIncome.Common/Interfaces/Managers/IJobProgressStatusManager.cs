﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface IJobProgressStatusManager : IBaseManager
    {
        IList<JobProgressStatus> GetAllJobProgressStatus();
        JobProgressStatus AddJobProgressStatus(JobProgressStatus jobProgressStatus);
        void UpdateJobProgressStatus(JobProgressStatus jobProgressStatus);        
    }
}
