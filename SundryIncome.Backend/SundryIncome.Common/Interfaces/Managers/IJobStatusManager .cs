﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface IJobStatusManager : IBaseManager
    {
        IList<JobStatus> GetallJobStatuses();

        JobStatus GetJobStatusById(int id);

        JobStatus AddJobStatus(JobStatus jStatus);

        void UpdateJobStatus(JobStatus jStatus);

        void DeleteJobStatus(int id);
    }
}
