﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface IAddressManager : IBaseManager
    {
        Address GetAddressById(int addressId);
        Address AddAddress(Address address);

        void UpdateAddress(Address address);
        void DeleteAddress(int id);
        IList<Address> GetAllAddresses();
        IList<Address> GetAddressByCustomerId(int customerId);
    }
}
