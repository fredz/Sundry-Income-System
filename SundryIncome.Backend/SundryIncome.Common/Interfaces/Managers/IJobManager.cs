﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface IJobManager : IBaseManager
    {
        Job GetJobById(int id);
        Job AddJob(Job job);

        void UpdateJob(Job job);
        void DeleteCreatedJob(int id);
        IList<Job> GetAllJobs();
    }
}
