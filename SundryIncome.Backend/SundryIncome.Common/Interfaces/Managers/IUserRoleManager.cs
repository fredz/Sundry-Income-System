﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface IUserRoleManager : IBaseManager
    {
        IList<UserRole> GetUserRolesByUserId(int id);

        UserRole AddUserRole(UserRole userRole);

        int UpdateUserRole(UserRole userRole);

        void DeleteUserRole(int userId, int roleId);
    }
}
