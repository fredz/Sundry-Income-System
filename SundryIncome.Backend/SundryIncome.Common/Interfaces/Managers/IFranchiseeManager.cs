﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface IFranchiseeManager : IBaseManager
    {
        Franchisee GetFranchiseeById(int roleId);
        Franchisee AddFranchisee(Franchisee franchisee);

        void UpdateFranchisee(Franchisee franchisee);
        void DeleteFranchisee(int id);
        IList<Franchisee> GetAllFranchisees();
    }
}
