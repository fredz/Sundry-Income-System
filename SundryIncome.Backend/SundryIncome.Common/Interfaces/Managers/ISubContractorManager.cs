﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface ISubContractorManager : IBaseManager
    {
        SubContractor GetSubContractorById(int roleId);
        SubContractor AddSubContractor(SubContractor sContractor);

        void UpdateSubContractor(SubContractor sContractor);
        void DeleteSubContractor(int id);
        IList<SubContractor> GetAllSubContractors();
    }
}
