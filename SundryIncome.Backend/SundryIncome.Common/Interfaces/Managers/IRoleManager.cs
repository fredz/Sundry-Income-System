﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface IRoleManager : IBaseManager
    {
        IList<Role> GetAllRoles();

        Role GetRoleById(int id);

        Role AddRole(Role role);

        void UpdateRole(Role role);

        void DeleteRole(int id);
    }
}
