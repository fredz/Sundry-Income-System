﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface IJobAssignmentManager : IBaseManager
    {

        JobAssignment AddJobAssignment(JobAssignment jobAssignment);

        void UpdateJobAssignment(JobAssignment jobAssignment);
        void UpdateJobAssignmentAmount(int jobAssignmentId, float amount);
        void UpdateJobAssignmentPrimary(int jobAssignmentId, bool primary);
        void DeleteJobAssignment(int id);
        IList<JobAssignment> GetJobAssignmentByJobId(int id);
    }
}
