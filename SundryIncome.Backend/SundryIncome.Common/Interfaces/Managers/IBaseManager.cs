﻿using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface IBaseManager
    {
        ISundryIncomeLogger Logger { get; }

        void Configure(ISundryIncomeLogger logger);
    }
}
