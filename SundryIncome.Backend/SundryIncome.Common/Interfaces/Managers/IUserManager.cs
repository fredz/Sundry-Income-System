﻿using SundryIncome.Domain.Entities;
using System.Collections.Generic;

namespace SundryIncome.Common.Interfaces.Managers
{
    public interface IUserManager : IBaseManager
    {
        User GetUserById(int userId);
        User AddUser(User user);
        IList<User> GetAllUsers();
        void UpdateUser(User user);
        void DeleteUser(int id);
        bool VerifyIfUserNameExists(string userName);
        void LogoutUser(string userName, int userId);
        User ValidateUser(string userName, string password);
    }
}
