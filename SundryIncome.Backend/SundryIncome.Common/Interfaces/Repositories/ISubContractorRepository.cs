﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Domain.Entities;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface ISubContractorRepository : IBaseRepository
    {
        SubContractor GetById(int roleId);
        SubContractor AddSubContractor(SubContractor sContractor);

        void Update(SubContractor sContractor);
        void Delete(int id);
        IList<SubContractor> GetAllSubContractors();
    }
}
