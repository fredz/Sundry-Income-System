﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Domain.Entities;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface IJobAssignmentRepository : IBaseRepository
    {   
        JobAssignment AddJobAssignment(JobAssignment jobAssignment);

        void UpdateJobAssignment(JobAssignment jobAssignment);
        void UpdateJobAssignmentAmount(int jobAssignmentId, float amount);
        void UpdateJobAssignmentPrimary(int jobAssignmentId, bool primary);
        void DeleteJobAssignment(int id);
        IList<JobAssignment> GetJobAssignmentByJobId(int id);
    }
}
