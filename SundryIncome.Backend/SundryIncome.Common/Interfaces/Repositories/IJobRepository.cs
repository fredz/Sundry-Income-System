﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Domain.Entities;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface IJobRepository : IBaseRepository
    {
        Job GetById(int id);
        Job CreateJob(Job job);

        void UpdateCreateJob(Job job);
        void Delete(int id);
        IList<Job> GetAllJobs();
    }
}
