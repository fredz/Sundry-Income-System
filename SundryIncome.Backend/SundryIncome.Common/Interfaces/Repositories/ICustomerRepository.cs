﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Domain.Entities;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface ICustomerRepository : IBaseRepository
    {
        Customer GetById(int frId);
        Customer AddCustomer(Customer customer);

        void Update(Customer customer);
        void Delete(int id);
        IList<Customer> GetAllCustomers();
    }
}
