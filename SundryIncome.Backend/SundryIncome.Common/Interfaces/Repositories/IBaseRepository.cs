﻿using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface IBaseRepository
    {
        ISundryIncomeLogger Logger { get; }

        void Configure(ISundryIncomeLogger Logger);
    }
}
