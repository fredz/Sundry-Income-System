﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface IRoleRepository : IBaseRepository
    {
        Role GetById(int roleId);
        Role AddRole(Role role);

        void Update(Role role);
        void Delete(int id);
        IList<Role> GetAllRoles();
    }
}
