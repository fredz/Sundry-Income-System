﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Domain.Entities;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface IFranchiseeRepository : IBaseRepository
    {
        Franchisee GetById(int frId);
        Franchisee AddFranchisee(Franchisee franchisee);

        void Update(Franchisee franchisee);
        void Delete(int id);
        IList<Franchisee> GetAllFranchisees();
    }
}
