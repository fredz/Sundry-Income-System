﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface IUserRepository : IBaseRepository
    {
        User GetUserById(int userId);
        User AddUser(User user);

        void UpdateUser(User role);
        void DeleteUser(int id);
        IList<User> GetAllUsers();
        bool VerifyIfUserNameExists(string userName);
        void Logout(string userName, int userId);
        User Validate(string userName, string password);
    }
}
