﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Domain.Entities;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface IJobProgressStatusRepository : IBaseRepository
    {
        IList<JobProgressStatus> GetAllJobProgressStatus();
        JobProgressStatus AddJobProgressStatus(JobProgressStatus jobProgressStatus);
        void UpdateJobProgressStatus(JobProgressStatus jobProgressStatus);
    }
}
