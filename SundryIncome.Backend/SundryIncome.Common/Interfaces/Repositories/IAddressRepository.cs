﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Domain.Entities;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface IAddressRepository : IBaseRepository
    {
        Address GetById(int addressId);
        Address AddAddress(Address address);

        void Update(Address address);
        void Delete(int id);
        IList<Address> GetAllAddresses();
        IList<Address> GetAddressByCustomerId(int customerId);
    }
}
