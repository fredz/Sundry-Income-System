﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface IUserRoleRepository : IBaseRepository
    {
        IList<UserRole> GetByUserId(int userId);
        UserRole AddUserRole(UserRole userRole);

        int Update(UserRole userRole);
        void Delete(int userId, int roleId);
    }
}
