﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Interfaces.Repositories
{
    public interface IJobStatusRepository : IBaseRepository
    {
        JobStatus GetById(int roleId);
        JobStatus AddJobStatus(JobStatus jStatus);

        void Update(JobStatus jStatus);
        void Delete(int id);
        IList<JobStatus> GetAllJobStatuses();
    }
}
