﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Common.Exceptions
{
    public class SundryIncomeRepositoryException : SundryIncomeException
    {
        public SundryIncomeRepositoryException()
        { }

        public SundryIncomeRepositoryException(string message) : base(message)
        { }

        public SundryIncomeRepositoryException(string message, Exception innerException) : base(message, innerException)
        { }
    }
}
