﻿using System;

namespace SundryIncome.Common.Exceptions
{
   public class SundryIncomeException : Exception
    {
        public SundryIncomeException() { }

        public SundryIncomeException(string message) : base(message) { }

        public SundryIncomeException(string message, Exception innerException) : base(message, innerException) { }
    }
}
