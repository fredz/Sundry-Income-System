﻿using log4net;
using System;
using SundryIncome.Common.Interfaces.Helpers;

namespace SundryIncome.Common.Helpers
{
   public class Log4NetSundryIncomeLogger : ISundryIncomeLogger
    {
        private static readonly ILog PortalLogger = LogManager.GetLogger("PortalLogger");
        private static readonly ILog CommonLogger = LogManager.GetLogger("CommonLogger");

        public void DebugToPortal(string message)
        {
            PortalLogger.Debug(message);
        }

        public void InfoToPortal(string message)
        {
            PortalLogger.Info(message);
        }

        public void WarnToPortal(string message)
        {
            PortalLogger.Warn(message);
        }

        public void ErrorToPortal(string message)
        {
            PortalLogger.Error(message);
        }

        public void DebugToPortal(string message, Exception e)
        {
            PortalLogger.DebugFormat("{0}.\nException: {1}", message, e);
        }

        public void InfoToPortal(string message, Exception e)
        {
            PortalLogger.InfoFormat("{0}.\nException: {1}", message, e);
        }

        public void WarnToPortal(string message, Exception e)
        {
            PortalLogger.WarnFormat("{0}.\nException: {1}", message, e);
        }

        public void ErrorToPortal(string message, Exception e)
        {
            PortalLogger.ErrorFormat("{0}.\nException: {1}", message, e);
        }

        public void DebugToLog(string message)
        {
            CommonLogger.Debug(message);
        }

        public void InfoToLog(string message)
        {
            CommonLogger.Info(message);
        }

        public void WarnToLog(string message)
        {
            CommonLogger.Warn(message);
        }

        public void ErrorToLog(string message)
        {
            CommonLogger.Error(message);
        }

        public void DebugToLog(string message, Exception e)
        {
            CommonLogger.DebugFormat("{0}.\nException: {1}", message, e);
        }

        public void InfoToLog(string message, Exception e)
        {
            CommonLogger.InfoFormat("{0}.\nException: {1}", message, e);
        }

        public void WarnToLog(string message, Exception e)
        {
            CommonLogger.WarnFormat("{0}.\nException: {1}", message, e);
        }

        public void ErrorToLog(string message, Exception e)
        {
            CommonLogger.ErrorFormat("{0}.\nException: {1}", message, e);
        }
    }
}
