﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Repositories;

namespace SundryIncome.Repositories
{
    public class RoleRepository : BaseRepository, IRoleRepository
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["conString"].ToString();
        // crud
        public void Update(Role role)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateRole", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", role.RoleId);
                        cm.Parameters.AddWithValue("@Name", role.Name);
                        cm.Parameters.AddWithValue("@Rank", role.Rank);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating role on database", e);
                throw new SundryIncomeRepositoryException("Error updating role on database", e);
            }
        }

        public Role GetById(int id)
        {
            Role resultRole  = new Role();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetRoleById", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            resultRole.RoleId = int.Parse(rd[0].ToString());
                            resultRole.Name = (rd[1].ToString());
                            resultRole.Rank = int.Parse(rd[2].ToString());
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting role from database", e);
                throw new SundryIncomeRepositoryException("Error getting role from database", e);
            }
            return resultRole;
        }

        public IList<Role> GetAllRoles()
        {
            IList<Role> resList = new List<Role>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAllRoles", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            Role rl = new Role();
                            rl.RoleId = int.Parse(rd[0].ToString());
                            rl.Name = (rd[1].ToString());
                            rl.Rank = int.Parse(rd[2].ToString());

                            resList.Add(rl);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all roles from database", e);
                throw new SundryIncomeRepositoryException("Error getting all roles from database", e);
            }
            return resList.ToList();
        }

        public Role AddRole(Role role)
        {
            Role resultRole = new Role();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_AddRole", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Name", role.Name);
                        cm.Parameters.AddWithValue("@Rank", role.Rank);
                        cm.ExecuteNonQuery();
                        
                        resultRole.Name = cm.Parameters["@Name"].Value.ToString();
                        resultRole.Rank = (int)cm.Parameters["@Rank"].Value;
                        cn.Close();
                    }
                }
                
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error adding role on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error adding role on database", exception);
            }

            return resultRole;
        }

        public void Delete(int id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_DeleteRole", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        //cm.Parameters.AddWithValue("@Name", role.Name);
                        //cm.Parameters.AddWithValue("@Rank", role.Rank);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error deleting role on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error deleting role on database", exception);
            }
        }
    }
}
