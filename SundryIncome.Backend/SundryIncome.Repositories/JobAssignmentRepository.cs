﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Repositories;

namespace SundryIncome.Repositories
{
    public class JobAssignmentRepository : BaseRepository, IJobAssignmentRepository
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["conString"].ToString();
        // crud
        public void UpdateJobAssignment(JobAssignment jobAssignment)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateJobAssignment", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@JobAssignmentId", jobAssignment.JobAssignId);
                        cm.Parameters.AddWithValue("@JobId", jobAssignment.JobId);
                        cm.Parameters.AddWithValue("@FranchiseeId", jobAssignment.FranchiseeId);
                        cm.Parameters.AddWithValue("@SubcontractorId", jobAssignment.SubcontractorId);
                        cm.Parameters.AddWithValue("@Amount", jobAssignment.Amount);
                        cm.Parameters.AddWithValue("@PurchaseInvoiceNo", jobAssignment.PurchaseInvoiceNo);
                        cm.Parameters.AddWithValue("@Primary", jobAssignment.Primary); 
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating jobAssignment on database", e);
                throw new SundryIncomeRepositoryException("Error updating jobAssignment on database", e);
            }
        }

        public void UpdateJobAssignmentAmount(int jobAssignmentId, float amount)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateJobAssignmentAmount", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@JobAssignmentId", jobAssignmentId);
                        cm.Parameters.AddWithValue("@Amount", amount);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating jobAssignment amount on database", e);
                throw new SundryIncomeRepositoryException("Error updating jobAssignment amount on database", e);
            }
        }


        public void UpdateJobAssignmentPrimary(int jobAssignmentId, bool primary)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateJobAssignmentPrimary", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@JobAssignmentId", jobAssignmentId);
                        cm.Parameters.AddWithValue("@Primary", primary);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating jobAssignment primary on database", e);
                throw new SundryIncomeRepositoryException("Error updating jobAssignment primary on database", e);
            }
        }


        public IList<JobAssignment> GetJobAssignmentByJobId(int jobId)
        {
            IList<JobAssignment> resList = new List<JobAssignment>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetJobAssignmentByJobId", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@JobId", jobId);
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            JobAssignment jobAssignment = new JobAssignment
                            {
                                JobAssignId = int.Parse(rd[0].ToString()),
                                JobId = int.Parse(rd[1].ToString()),
                                FranchiseeId = int.Parse(rd[2].ToString()),
                                SubcontractorId = (rd[3] == DBNull.Value ? (int?)null : int.Parse(rd[3].ToString())),                                
                                Amount = (rd[4] == DBNull.Value ? (float?)null : float.Parse(rd[4].ToString())),
                                PurchaseInvoiceNo = int.Parse(rd[5].ToString()),
                                Primary = bool.Parse(rd[6].ToString()),
                                FranchiseeFullName = rd[7].ToString(),
                                CompanyFranchisee = rd[8].ToString(),
                                SubcontractorFullName = rd[9].ToString(),
                                CompanySubcontractor = rd[10].ToString()
                            };
                               
                            resList.Add(jobAssignment);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting jobAssignment from database", e);
                throw new SundryIncomeRepositoryException("Error getting  jobAssignment from database", e);
            }
            return resList.ToList();
        }

        public JobAssignment AddJobAssignment(JobAssignment jobAssignment)
        {
            JobAssignment nJobAssignment = new JobAssignment();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_AddJobAssignment", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();

                        cm.Parameters.AddWithValue("@JobId", jobAssignment.JobId);
                        cm.Parameters.AddWithValue("@FranchiseeId", jobAssignment.FranchiseeId);
                        cm.Parameters.AddWithValue("@SubcontractorId", jobAssignment.SubcontractorId);
                        cm.Parameters.AddWithValue("@Amount", jobAssignment.Amount);
                        cm.Parameters.AddWithValue("@PurchaseInvoiceNo", jobAssignment.PurchaseInvoiceNo);
                        cm.Parameters.AddWithValue("@Primary", jobAssignment.Primary);
                        cm.ExecuteNonQuery();

                        nJobAssignment.JobId = (int)cm.Parameters["@JobId"].Value;
                        nJobAssignment.FranchiseeId = (int) cm.Parameters["@FranchiseeId"].Value;                        
                        nJobAssignment.SubcontractorId = (Convert.ToInt32(cm.Parameters["@SubcontractorId"].Value) == 0 ? (int?)null : (int)cm.Parameters["@SubcontractorId"].Value);
                        nJobAssignment.Amount = (cm.Parameters["@Amount"].Value == DBNull.Value ? (float?)null : (float)cm.Parameters["@Amount"].Value);                        
                        nJobAssignment.PurchaseInvoiceNo = (int) cm.Parameters["@PurchaseInvoiceNo"].Value;
                        nJobAssignment.Primary = (bool) cm.Parameters["@Primary"].Value;
                        cn.Close();
                    }
                }
                
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error adding JobAssignment on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error adding JobAssignment on database", exception);
            }

            return nJobAssignment;
        }

        public void DeleteJobAssignment(int id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_DeleteJobAssignment", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error deleting jobAssignment on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error deleting jobAssignment on database", exception);
            }
        }
    }
}
