﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Repositories;

namespace SundryIncome.Repositories
{
    public class JobRepository : BaseRepository, IJobRepository
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["conString"].ToString();
        // crud
        public void UpdateCreateJob(Job job)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateCreateJob", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@JobId", job.JobId);
                        cm.Parameters.AddWithValue("@CustomerId", job.CustomerId);
                        cm.Parameters.AddWithValue("@AreaManagerId", job.AreaManagerId);
                        cm.Parameters.AddWithValue("@OrderNo", job.OrderNo);
                        cm.Parameters.AddWithValue("@Description", job.Description);
                        cm.Parameters.AddWithValue("@RequestedDate", job.RequestedDate);
                        cm.Parameters.AddWithValue("@DateFrom", job.DateFrom);
                        cm.Parameters.AddWithValue("@DateTo", job.DateTo);
                        //cm.Parameters.AddWithValue("@DateCompleted", job.DateCompleted == DateTime.MinValue ? null : job.DateCompleted);
                        cm.Parameters.AddWithValue("@AMCAmount", job.AMCAmount);
                        cm.Parameters.AddWithValue("@ModifiedBy", job.ModifiedBy);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating job on database", e);
                throw new SundryIncomeRepositoryException("Error updating job on database", e);
            }
        }

        public Job GetById(int id)
        {
            Job job  = new Job();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetCreateJobById", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            job.JobId = int.Parse(rd[0].ToString());
                            job.CustomerId = int.Parse(rd[1].ToString());
                            job.CustomerFullName = (rd[2].ToString());
                            job.AreaManagerId = int.Parse(rd[3].ToString());
                            job.AManagerFullName = (rd[4].ToString());
                            job.OrderNo = int.Parse(rd[5].ToString());
                            job.Description = rd[6].ToString();
                            job.RequestedDate = (rd[7] == DBNull.Value? (DateTime?) null : DateTime.Parse(rd[7].ToString()));
                            job.DateFrom = (rd[8] == DBNull.Value ? (DateTime?) null : DateTime.Parse(rd[8].ToString()));
                            job.DateTo = (rd[9] == DBNull.Value ? (DateTime?) null : DateTime.Parse(rd[9].ToString()));
                            job.DateCompleted = (rd[10] == DBNull.Value? (DateTime?) null : DateTime.Parse(rd[10].ToString()));
                            job.AMCAmount = (rd[11] == DBNull.Value ? (float?) null : float.Parse(rd[11].ToString()));
                            job.SalesInvoiceNo = (rd[12] == DBNull.Value ? (int?) null : int.Parse(rd[12].ToString()));
                            job.SalesInvoiceDate = (rd[13] == DBNull.Value? (DateTime?) null : DateTime.Parse(rd[13].ToString()));
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting job from database", e);
                throw new SundryIncomeRepositoryException("Error getting job from database", e);
            }
            return job;
        }

        public IList<Job> GetAllJobs()
        {
            IList<Job> resList = new List<Job>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAllJobs", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            Job job = new Job
                            {
                                JobId = int.Parse(rd[0].ToString()),
                                CustomerId = int.Parse(rd[1].ToString()),
                                CustomerFullName = (rd[2].ToString()),
                                AreaManagerId = int.Parse(rd[3].ToString()),
                                AManagerFullName = (rd[4].ToString()),
                                OrderNo = int.Parse(rd[5].ToString()),
                                Description = rd[6].ToString(),
                                RequestedDate = (rd[7] == DBNull.Value ? (DateTime?)null : DateTime.Parse(rd[7].ToString())),
                                DateFrom = (rd[8] == DBNull.Value ? (DateTime?)null : DateTime.Parse(rd[8].ToString())),
                                DateTo = (rd[9] == DBNull.Value ? (DateTime?)null : DateTime.Parse(rd[9].ToString())),
                                DateCompleted = (rd[10] == DBNull.Value ? (DateTime?)null : DateTime.Parse(rd[10].ToString())),
                                AMCAmount = (rd[11] == DBNull.Value ? (float?)null : float.Parse(rd[11].ToString())),
                                SalesInvoiceNo = (rd[12] == DBNull.Value ? (int?)null : int.Parse(rd[12].ToString())),
                                SalesInvoiceDate = (rd[13] == DBNull.Value ? (DateTime?)null : DateTime.Parse(rd[13].ToString()))
                            };
                               
                            resList.Add(job);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all jobs from database", e);
                throw new SundryIncomeRepositoryException("Error getting all jobs from database", e);
            }
            return resList.ToList();
        }

        public Job CreateJob(Job job)
        {
            Job nJob = new Job();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_CreateJob", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();

                        cm.Parameters.AddWithValue("@CustomerId", job.CustomerId);
                        cm.Parameters.AddWithValue("@AreaManagerId", job.AreaManagerId);
                        cm.Parameters.AddWithValue("@OrderNo", job.OrderNo);
                        cm.Parameters.AddWithValue("@Description", job.Description);
                        cm.Parameters.AddWithValue("@RequestedDate", job.RequestedDate);
                        cm.Parameters.AddWithValue("@DateFrom", job.DateFrom);
                        cm.Parameters.AddWithValue("@DateTo", job.DateTo);
                        //cm.Parameters.AddWithValue("@DateCompleted", job.DateCompleted);
                        cm.Parameters.AddWithValue("@AMCAmount", job.AMCAmount);
                       // cm.Parameters.AddWithValue("@CreatedOn", job.CreatedOn);
                        cm.Parameters.AddWithValue("@CreatedBy", job.CreatedBy);
                       // cm.Parameters.AddWithValue("@ModifiedOn", job.ModifiedOn);
                       // cm.Parameters.AddWithValue("@ModifiedBy", job.ModifiedBy);
                       // cm.Parameters.AddWithValue("@SalesInvoiceNo", job.SalesInvoiceNo);
                       // cm.Parameters.AddWithValue("@SalesInvoiceDate", job.SalesInvoiceDate);

                        cm.ExecuteNonQuery();

                        nJob.AreaManagerId =(int)cm.Parameters["@AreaManagerId"].Value;
                        nJob.OrderNo = (int) cm.Parameters["@OrderNo"].Value;
                        nJob.Description = cm.Parameters["@Description"].Value.ToString();
                        nJob.RequestedDate = (cm.Parameters["@RequestedDate"].Value == DBNull.Value ? (DateTime?)null : (DateTime)cm.Parameters["@RequestedDate"].Value);
                        nJob.DateFrom = (cm.Parameters["@DateFrom"].Value == DBNull.Value ? (DateTime?)null : (DateTime)cm.Parameters["@DateFrom"].Value);
                        nJob.DateTo = (cm.Parameters["@DateTo"].Value == DBNull.Value ? (DateTime?)null : (DateTime)cm.Parameters["@DateTo"].Value);
                       // nJob.DateCompleted = (cm.Parameters["@DateCompleted"].Value == DBNull.Value ? (DateTime?)null : (DateTime)cm.Parameters["@DateCompleted"].Value);
                        nJob.AMCAmount = (cm.Parameters["@AMCAmount"].Value == DBNull.Value ? (float?)null : (float)cm.Parameters["@AMCAmount"].Value);
                       // nJob.CreatedOn = (DateTime)cm.Parameters["@CreatedOn"].Value;
                        nJob.CreatedBy = (cm.Parameters["@CreatedBy"].Value == DBNull.Value ? (int?)null : (int)cm.Parameters["@CreatedBy"].Value);
                       // nJob.ModifiedOn = (DateTime)cm.Parameters["@ModifiedOn"].Value;
                      //  nJob.ModifiedBy = (cm.Parameters["@ModifiedBy"].Value == DBNull.Value ? (int?)null : (int) cm.Parameters["@ModifiedBy"].Value);
                       // nJob.SalesInvoiceNo = (cm.Parameters["@SalesInvoiceNo"].Value == DBNull.Value ? (int?)null : (int) cm.Parameters["@SalesInvoiceNo"].Value);
                      //  nJob.SalesInvoiceDate = (cm.Parameters["@SalesInvoiceDate"].Value == DBNull.Value ? (DateTime?)null : (DateTime)cm.Parameters["@SalesInvoiceDate"].Value);
                        
                        cn.Close();
                    }
                }
                
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error adding job on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error adding job on database", exception);
            }

            return nJob;
        }

        public void Delete(int id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_DeleteCreateJob", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error deleting job on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error deleting job on database", exception);
            }
        }
    }
}
