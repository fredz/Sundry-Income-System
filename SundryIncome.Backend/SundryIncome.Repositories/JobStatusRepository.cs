﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Repositories;

namespace SundryIncome.Repositories
{
    public class JobStatusRepository : BaseRepository, IJobStatusRepository
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["conString"].ToString();
        // crud
        public void Update(JobStatus jStatus)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateJobStatus", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", jStatus.JobStatusId);
                        cm.Parameters.AddWithValue("@Name", jStatus.Name);
                        cm.Parameters.AddWithValue("@ColorCode", jStatus.ColorCode);
                        cm.Parameters.AddWithValue("@Rank", jStatus.Rank);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating jobstatus on database", e);
                throw new SundryIncomeRepositoryException("Error updating jobstatus on database", e);
            }
        }

        public JobStatus GetById(int id)
        {
            JobStatus resJobStatus  = new JobStatus();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetJobStatusById", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            resJobStatus.JobStatusId = int.Parse(rd[0].ToString());
                            resJobStatus.Name = (rd[1].ToString());
                            resJobStatus.ColorCode = (rd[2].ToString());
                            resJobStatus.Rank = int.Parse(rd[3].ToString());
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting jobstatus from database", e);
                throw new SundryIncomeRepositoryException("Error getting jobstatus from database", e);
            }
            return resJobStatus;
        }

        public IList<JobStatus> GetAllJobStatuses()
        {
            IList<JobStatus> resList = new List<JobStatus>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAllJobStatuses", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            JobStatus js = new JobStatus
                            {
                                JobStatusId = int.Parse(rd[0].ToString()),
                                Name = (rd[1].ToString()),
                                ColorCode = (rd[2].ToString()),
                                Rank = int.Parse(rd[3].ToString())
                            };

                            resList.Add(js);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all jobstatus from database", e);
                throw new SundryIncomeRepositoryException("Error getting all jobstatus from database", e);
            }
            return resList.ToList();
        }

        public JobStatus AddJobStatus(JobStatus jStatus)
        {
            JobStatus resJobStatus = new JobStatus();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_AddJobStatus", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Name", jStatus.Name);
                        cm.Parameters.AddWithValue("@ColorCode", jStatus.ColorCode);
                        cm.Parameters.AddWithValue("@Rank", jStatus.Rank);
                        cm.ExecuteNonQuery();
                        
                        resJobStatus.Name = cm.Parameters["@Name"].Value.ToString();
                        resJobStatus.ColorCode = cm.Parameters["@ColorCode"].Value.ToString();
                        resJobStatus.Rank = (int)cm.Parameters["@Rank"].Value;
                        cn.Close();
                    }
                }
                
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error adding jobstatus on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error adding jobstatus on database", exception);
            }

            return resJobStatus;
        }

        public void Delete(int id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_DeleteJobStatus", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error deleting jobstatus on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error deleting jobstatus on database", exception);
            }
        }
    }
}
