use [ServiceAccessPoint_Sandbox]
GO
/******
Adding Initial Script   Miguel   07-06-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_GetJobStatusById]
@Id nvarchar(50)
AS
BEGIN
      SET NOCOUNT ON;
      Select * from [dbo].[JobStatus]  WHERE JobStatusID = @ID
END

GO