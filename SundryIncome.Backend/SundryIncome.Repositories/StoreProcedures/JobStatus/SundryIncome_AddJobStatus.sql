use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   07-06-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_AddJobStatus]
      @Name varchar(100),
	  @ColorCode varchar(100),
      @Rank int
AS
BEGIN
      SET NOCOUNT ON;
      INSERT INTO [dbo].[JobStatus] 
      VALUES (@Name, @ColorCode, @Rank)
END
GO

