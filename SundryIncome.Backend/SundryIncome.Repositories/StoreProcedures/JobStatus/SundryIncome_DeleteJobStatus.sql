use [ServiceAccessPoint_Sandbox]
GO
/******
Adding Initial Script   Miguel   07-06-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_DeleteJobStatus]
@Id nvarchar(50)
AS
BEGIN
      SET NOCOUNT ON;
      Delete from [dbo].[JobStatus]  WHERE JobStatusID = @ID
END

GO
