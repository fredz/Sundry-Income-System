use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   07-06-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_UpdateJobStatus]
      @Id varchar(100),
	  @Name varchar(100),
      @ColorCode varchar(100),
	  @Rank int
   
AS
BEGIN
      SET NOCOUNT ON;
      UPDATE  [dbo].[JobStatus] SET Name = @Name, ColorCode = @ColorCode, Rank = @Rank
      WHERE JobStatusID = @Id 
END

GO


