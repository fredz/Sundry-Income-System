use [ServiceAccessPoint_Sandbox]
GO

alter PROCEDURE [dbo].[SundryIncome_UpdateJobProgressStatus]
	  @JobId INT,
      @JobProgressId INT,
      @JobStatusId  INT   
AS
BEGIN
      SET NOCOUNT ON;
      UPDATE [dbo].[JobProgress] SET 
		JobStatusId = @JobStatusId
      WHERE JobID = @JobId AND JobProgressID = @JobProgressId
END

GO


