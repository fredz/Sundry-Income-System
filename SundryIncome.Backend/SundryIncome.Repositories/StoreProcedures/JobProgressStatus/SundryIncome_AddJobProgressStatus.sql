use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Fredy   01-06-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_AddJobProgressStatus]
      @CustomerID INT,
      @AreaManagerID  INT,
      @OrderNo INT,
      @Description text,
      @RequestedDate date,
      @DateFrom date,
      @DateTo date,      
      @AMCAmount float,	  
	  @CreatedBy INT,
	  @StepNo INT,
	  @JobStatusId INT	  

AS
BEGIN
	-- transaction
	BEGIN TRY
		BEGIN TRANSACTION 

		DECLARE @JobId INT		

		  SET NOCOUNT ON;
		  INSERT INTO [dbo].[Job]
		  (
			CustomerID,
			AreaManagerID,
			OrderNo,
			[Description],
			RequestedDate,
			DateFrom,
			DateTo,
			DateCompleted,
			AMCAmount,		
			CreatedBy,		
			ModifiedBy,
			SalesInvoceNo,
			SalesInovoiceDate,
			CreatedOn,
			ModifiedOn
		  )
		  VALUES (
			@CustomerID,
			@AreaManagerID,
			@OrderNo,
			@Description,
			@RequestedDate,
			@DateFrom,
			@DateTo,
			NULL,--@DateCompleted
			@AMCAmount,		
			@CreatedBy,		
			NULL, --@ModifiedBy,
			NULL, --@SalesInvoceNo,
			NULL, --@SalesInovoiceDate
			GETDATE() , --@CreatedOn,
			NULL  --@ModifiedOn
			)
	
		SELECT @JobId = SCOPE_IDENTITY() 

			--Insert JobProgres table
		INSERT INTO [dbo].[JobProgress]
			   ([JobID]
			   ,[JobStatusID]
			   ,[CreatedOn]
			   ,[CreatedBy]
			   ,[StepNo])
		 VALUES
			   (@JobId
			   ,@JobStatusId
			   ,GETDATE()
			   ,@CreatedBy
			   ,@StepNo)


		COMMIT
	END TRY
	BEGIN CATCH
	
		SELECT   
			ERROR_NUMBER() AS ErrorNumber  
			,ERROR_MESSAGE() AS ErrorMessage; 

		IF @@TRANCOUNT > 0
			ROLLBACK
	END CATCH

END


GO


