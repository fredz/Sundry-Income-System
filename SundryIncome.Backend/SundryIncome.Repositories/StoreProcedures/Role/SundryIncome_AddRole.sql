use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   17-05-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_AddRole]
      @Name varchar(50),
      @Rank int
AS
BEGIN
      SET NOCOUNT ON;
      INSERT INTO Roles 
      VALUES (@Name, @Rank)
END
GO


