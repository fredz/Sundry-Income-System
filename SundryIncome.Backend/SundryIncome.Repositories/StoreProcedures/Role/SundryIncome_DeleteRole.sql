use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   17-05-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_DeleteRole]
@Id nvarchar(50)
AS
BEGIN
      SET NOCOUNT ON;
      Delete from Roles  WHERE Id=@ID
END
GO


