use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   17-05-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_UpdateRole]
      @Id varchar(50),
	  @Name varchar(50),
      @Rank int
   
AS
BEGIN
      SET NOCOUNT ON;
      UPDATE  Roles SET Name = @Name, Rank = @Rank
      WHERE Id=@ID 
END
GO