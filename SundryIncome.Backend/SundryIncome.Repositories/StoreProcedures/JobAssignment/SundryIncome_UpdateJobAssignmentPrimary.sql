USE [ServiceAccessPoint_Sandbox]
GO

/****** Object:  StoredProcedure [dbo].[SundryIncome_UpdateJobAssignmentPrimary]    Script Date: 17/07/2017 06:25:18 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[SundryIncome_UpdateJobAssignmentPrimary]
      @JobAssignmentId int, 
	  @Primary tinyint
AS
BEGIN
            
	  UPDATE  dbo.JobAssignment set [Primary]= @Primary WHERE JobAssignID = @JobAssignmentId

END


