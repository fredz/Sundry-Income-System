USE [ServiceAccessPoint_Sandbox]
GO

/****** Object:  StoredProcedure [dbo].[SundryIncome_DeleteJobAssignment]    Script Date: 17/07/2017 06:41:08 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_DeleteJobAssignment]
@Id nvarchar(50)
AS
BEGIN
      SET NOCOUNT ON;
      Delete from dbo.JobAssignment WHERE JobAssignID = @Id
END


GO


