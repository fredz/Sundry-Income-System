USE [ServiceAccessPoint_Sandbox]
GO

/****** Object:  StoredProcedure [dbo].[SundryIncome_UpdateJobAssignment]    Script Date: 17/07/2017 06:34:24 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




create PROCEDURE [dbo].[SundryIncome_UpdateJobAssignment]
      @JobAssignmentId int,
	  @JobId int,
	  @FranchiseeId int,
	  @SubcontractorId int,
	  @Amount float,
	  @PurchaseInvoiceNo int,
	  @Primary bit
AS
BEGIN
            
	  UPDATE  dbo.JobAssignment 
	  set 		
		JobId = @JobId,
		FranchiseeId = @FranchiseeId,
		SubcontractorId = @SubcontractorId,
		Amount = @Amount,
		PurchaseInvoiceNo = @PurchaseInvoiceNo,
		[Primary] = @Primary
	  WHERE JobAssignID = @JobAssignmentId

END




GO


