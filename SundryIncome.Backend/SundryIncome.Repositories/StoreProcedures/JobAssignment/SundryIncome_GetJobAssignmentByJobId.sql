USE [ServiceAccessPoint_Sandbox]
GO

/****** Object:  StoredProcedure [dbo].[SundryIncome_GetAllAddresses]    Script Date: 17/07/2017 06:18:26 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SundryIncome_GetJobAssignmentByJobId]
@JobId int
AS
BEGIN
    
	SET NOCOUNT ON;

	SELECT 
	jas.JobAssignID,
	jo.JobID,
	fa.FranchiseeID,
	sb.SubcontractorID,
	jas.Amount,
	jas.PurchaseInvoiceNo,
	jas.[Primary],
	fa.FirstName +' '+ fa.LastName as 'FranchiseeFullName', 
	fa.CompanyName as 'CompanyFranchisee',
	sb.FirstName +' '+ sb.LastName as 'SubcontractorFullName',
	sb.CompanyName as 'CompanySubcontractor'

	FROM 
	dbo.JobAssignment as jas 
	LEFT JOIN dbo.Franchisee as fa ON jas.FranchiseeID = fa.FranchiseeID 
	LEFT JOIN dbo.Subcontractor as sb ON jas.SubcontractorID = sb.SubcontractorID
	JOIN dbo.Job as jo ON jas.JobID = jo.JobID 

	WHERE	jo.JobID = @JobId

END


GO


