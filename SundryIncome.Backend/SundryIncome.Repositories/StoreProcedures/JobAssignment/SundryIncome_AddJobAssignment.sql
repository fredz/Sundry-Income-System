USE [ServiceAccessPoint_Sandbox]
GO

/****** Object:  StoredProcedure [dbo].[SundryIncome_AddJobAssignment]    Script Date: 17/07/2017 06:24:01 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[SundryIncome_AddJobAssignment]
      @JobId int,
	  @FranchiseeId int,
	  @SubcontractorId int,
	  @Amount float,
	  @PurchaseInvoiceNo int,
	  @Primary bit
      
AS
BEGIN
    SET NOCOUNT ON;

	INSERT INTO [dbo].[JobAssignment]
           ([JobID]
		   ,[FranchiseeID]
           ,[SubcontractorID]
           ,[Amount]
           ,[PurchaseInvoiceNo]
           ,[Primary])
     VALUES
           (@JobId
		   ,NULLIF(@FranchiseeID, 0)
           ,NULLIF(@SubcontractorId, 0)
           ,@Amount
           ,@PurchaseInvoiceNo
           ,@Primary)


END



GO


