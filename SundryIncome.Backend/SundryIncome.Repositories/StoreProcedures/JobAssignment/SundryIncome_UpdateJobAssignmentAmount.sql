USE [ServiceAccessPoint_Sandbox]
GO

/****** Object:  StoredProcedure [dbo].[SundryIncome_UpdateJobAssignmentAmount]    Script Date: 17/07/2017 06:24:49 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SundryIncome_UpdateJobAssignmentAmount]
      @JobAssignmentId int, 
	  @Amount float
AS
BEGIN
            
	  UPDATE  dbo.JobAssignment set Amount= @Amount WHERE JobAssignID = @JobAssignmentId

END



GO


