use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   01-06-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_AddCustomer]
      @FirstName varchar(100),
      @LastName  varchar(100),
      @BillingName varchar(100),
      @Email varchar(100),
      @Phone int,
      @MobileNo int,
      @PurchaseOrderRequired bit,
      @AMCInvoiceOnly bit,
      @DefaultInvoiceDescription text      
AS
BEGIN
      SET NOCOUNT ON;
      INSERT INTO [dbo].[Customer]
      VALUES (@FirstName,@LastName, @BillingName, @Email, @Phone, @MobileNo, @PurchaseOrderRequired, @AMCInvoiceOnly, @DefaultInvoiceDescription)
END


GO


