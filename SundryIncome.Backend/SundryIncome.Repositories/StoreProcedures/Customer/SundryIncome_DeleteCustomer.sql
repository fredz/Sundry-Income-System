use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   01-06-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_DeleteCustomer]
@Id nvarchar(50)
AS
BEGIN
      SET NOCOUNT ON;
      Delete from [dbo].[Customer] WHERE CustomerID = @ID
END

GO