use [ServiceAccessPoint_Sandbox]
GO

CREATE PROCEDURE [dbo].[SundryIncome_UpdateCustomer]
      @CustomerId int,
	  @FirstName varchar(100),
      @LastName  varchar(100),
      @BillingName varchar(100),
      @Email varchar(100),
      @Phone int,
      @MobileNo int,
      @PurchaseOrderRequired bit,
      @AMCInvoiceOnly bit,
      @DefaultInvoiceDescription text  
   
AS
BEGIN
      SET NOCOUNT ON;
      UPDATE [dbo].[Customer] SET FirstName = @FirstName, LastName = @LastName, BillingName = @BillingName ,Email = @Email, Phone = @Phone,
	  MobileNo = @MobileNo, PurchaseOrderRequired = @PurchaseOrderRequired, AMCInvoiceOnly = @AMCInvoiceOnly, 
	  DefaultInvoiceDescription = @DefaultInvoiceDescription
      WHERE CustomerID =@CustomerId
END

GO


