use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   30-05-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_UpdateFranchisee]
      @FranchiseeId varchar(50),
	  @Code int,
	  @FirstName varchar(100),
      @LastName varchar(100),
	  @ContactName varchar(100),
	  @CompanyName varchar(100)
   
AS
BEGIN
      SET NOCOUNT ON;
      UPDATE [dbo].[Franchisee] SET Code = @Code , FirstName = @FirstName, LastName = @LastName, ContactName = @ContactName ,CompanyName = @CompanyName
      WHERE FranchiseeID =@FranchiseeId
END


GO


