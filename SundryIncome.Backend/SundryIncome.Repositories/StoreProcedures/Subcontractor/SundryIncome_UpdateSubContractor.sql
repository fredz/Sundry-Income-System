use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   30-05-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_UpdateSubContractor]
      @SubContractorId varchar(50),
	  @FirstName varchar(100),
      @LastName varchar(100),
	  @CompanyName varchar(100)
   
AS
BEGIN
      SET NOCOUNT ON;
      UPDATE  [dbo].[Subcontractor] SET FirstName = @FirstName, LastName = @LastName, CompanyName = @CompanyName
      WHERE SubcontractorID =@SubContractorId
END

GO


