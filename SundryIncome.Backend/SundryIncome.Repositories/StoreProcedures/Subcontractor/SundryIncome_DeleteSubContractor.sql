use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   30-05-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_DeleteSubContractor]
@Id nvarchar(50)
AS
BEGIN
      SET NOCOUNT ON;
      Delete from [dbo].[Subcontractor]  WHERE SubcontractorID = @ID
END

GO


