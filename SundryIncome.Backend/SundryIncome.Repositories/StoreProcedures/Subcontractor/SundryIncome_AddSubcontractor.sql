use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   30-05-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_AddSubcontractor]
      @FirstName varchar(100),
	  @LastName varchar(100),
	  @CompanyName varchar(100)
      
AS
BEGIN
      SET NOCOUNT ON;
      INSERT INTO [dbo].[Subcontractor] 
      VALUES (@FirstName, @LastName, @CompanyName)
END

GO


