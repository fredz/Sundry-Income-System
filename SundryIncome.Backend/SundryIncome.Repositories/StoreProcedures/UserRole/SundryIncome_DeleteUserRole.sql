use [ServiceAccessPoint_Sandbox]
GO
/******
Adding Initial Script   Miguel   25-05-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_DeleteUserRole]
      @UserID int, 
	  @RoleID int
AS
BEGIN

      SET NOCOUNT ON;      
	  DELETE FROM UserRole  WHERE UserID = @UserID AND RoleID = @RoleID

END

GO