use [ServiceAccessPoint_Sandbox]
GO
/******
Adding Initial Script   Miguel   25-05-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_GetUserRolesByUserID]
      @UserID int
AS
BEGIN
      SET NOCOUNT ON;
      SELECT  ur.UserRoleID, ur.UserID, ur.RoleID, ur.Disabled, ro.Name, ro.Rank from UserRole as ur, Roles as ro  WHERE ur.RoleID = ro.RoleID ORDER BY ro.Rank

END

GO


