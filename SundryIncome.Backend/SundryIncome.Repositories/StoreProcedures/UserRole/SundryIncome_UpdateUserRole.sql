use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   25-05-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_UpdateUserRole]
      @UserID int, 
	  @RoleID int, 
      @Disabled tinyint
AS
BEGIN
      SET NOCOUNT ON;
      UPDATE UserRole  set Disabled = @Disabled WHERE UserID = @UserID AND RoleID = @RoleID

END

GO
