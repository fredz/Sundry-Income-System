use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   25-05-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_AddUserRole]
      @UserID int,
      @RoleID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
      SET NOCOUNT ON;
      INSERT INTO UserRole (UserID, RoleID, Disabled)
      VALUES (@UserID, @RoleID, 0)	

END

