use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Fredy   01-06-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_CreateJob]
      @CustomerID INT,
      @AreaManagerID  INT,
      @OrderNo INT,
      @Description text,
      @RequestedDate date,
      @DateFrom date,
      @DateTo date,      
      @AMCAmount float,	  
	  @CreatedBy INT	

AS
BEGIN
      SET NOCOUNT ON;
      INSERT INTO [dbo].[Job]
	  (
		CustomerID,
		AreaManagerID,
		OrderNo,
		[Description],
		RequestedDate,
		DateFrom,
		DateTo,
		DateCompleted,
		AMCAmount,		
		CreatedBy,		
		ModifiedBy,
		SalesInvoceNo,
		SalesInovoiceDate,
		CreatedOn,
		ModifiedOn
	  )
      VALUES (
		@CustomerID,
		@AreaManagerID,
		@OrderNo,
		@Description,
		@RequestedDate,
		@DateFrom,
		@DateTo,
		NULL,--@DateCompleted
		@AMCAmount,		
		@CreatedBy,		
		NULL, --@ModifiedBy,
		NULL, --@SalesInvoceNo,
		NULL, --@SalesInovoiceDate
		GETDATE() , --@CreatedOn,
		NULL  --@ModifiedOn
		)
END


GO


