use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Fredy   15-06-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_DeleteCreateJob]
@Id nvarchar(50)
AS
BEGIN
      SET NOCOUNT ON;
      Delete from [dbo].[Job] WHERE JobID = @ID
END

GO