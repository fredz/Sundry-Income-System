use [ServiceAccessPoint_Sandbox]
GO

CREATE PROCEDURE [dbo].[SundryIncome_UpdateCreateJob]
	  @JobID INT,
      @CustomerID INT,
      @AreaManagerID  INT,
      @OrderNo INT,
      @Description text,
      @RequestedDate date,
      @DateFrom date,
      @DateTo date,      
      @AMCAmount float,
	  @ModifiedBy INT	  
   
AS
BEGIN
      SET NOCOUNT ON;
      UPDATE [dbo].[Job] SET 
		CustomerID = @CustomerID,
		AreaManagerID = @AreaManagerID,
		OrderNo = @OrderNo,
		[Description] = @Description,
		RequestedDate = @RequestedDate,
		DateFrom = @DateFrom,
		DateTo = @DateTo,		
		AMCAmount = @AMCAmount,				
		ModifiedBy = @ModifiedBy,						
		ModifiedOn = GETDATE()  
      WHERE JobID = @JobID
END

GO


