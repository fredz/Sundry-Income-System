use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   01-06-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_GetCreateJobById]
      @Id int
AS
BEGIN

    SET NOCOUNT ON;
	SELECT 
	jo.JobID,
	jo.CustomerID,
	cu.FirstName +' '+ cu.LastName AS 'Customer',
	jo.AreaManagerID,
	us.FirstName +' '+ us.LastName AS 'AreaManager',
	jo.OrderNo,
	jo.[Description],
	jo.RequestedDate,
	jo.DateFrom,
	jo.DateTo,
	jo.DateCompleted,
	jo.AMCAmount,
	jo.SalesInvoceNo,
	jo.SalesInovoiceDate 
	FROM dbo.Job AS jo, dbo.Customer AS cu , dbo.[User] AS us
	WHERE 
	jo.CustomerID = cu.CustomerID AND
	jo.AreaManagerID =  us.UserID AND
	jo.JobID = @Id


END

GO