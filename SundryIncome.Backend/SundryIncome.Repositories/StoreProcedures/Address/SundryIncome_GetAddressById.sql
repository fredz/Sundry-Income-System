use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   01-06-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_GetAddressById]
@Id int
AS
BEGIN
      SET NOCOUNT ON;
      SELECT * FROM [dbo].[Address] Where AddressID = @Id
END

GO
