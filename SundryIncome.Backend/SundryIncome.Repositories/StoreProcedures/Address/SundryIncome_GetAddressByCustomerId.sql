use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   22-06-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_GetAddressByCustomerId]
      @Id int
AS
BEGIN

    SET NOCOUNT ON;
	SELECT 
	ad.AddressID,
	ad.[Address],
	ad.Locality,
	ad.[State],
	ad.PostalCode,
	ad.[CustomerId],
	ad.[Type],
	cu.BillingName AS 'Billing Name'
	
	FROM  dbo.Customer AS cu ,[dbo].[Address] AS ad
	WHERE 
	ad.CustomerID = cu.CustomerID AND ad.CustomerId = @Id
	

END


GO


