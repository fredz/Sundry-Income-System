use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   01-06-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_UpdateAddress]
      @AddressId int,
	  @Address varchar(100),
      @Locality  varchar(100),
      @State varchar(100),
      @PostalCode varchar(100),
	  @CustomerId int,
	  @Type varchar(150)
AS
BEGIN
      SET NOCOUNT ON;
      UPDATE [dbo].[Address] SET Address = @Address , Locality = @Locality, PostalCode = @PostalCode, CustomerId = @CustomerId, [Type] = @Type
      WHERE AddressID = @AddressId
END


GO
