use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   24-05-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_UpdateUser]
    @UserId  int,
	@FirstName varchar(100),
	@LastName  varchar(100),
	@Email     varchar(100),
	@MobileNo  varchar(100), 
	@Phone     varchar(100),
	@UserName   varchar(100),
	@Password   varchar(100)
	--@Uid         varchar(100)
AS
BEGIN
      SET NOCOUNT ON;
      UPDATE [dbo].[User]  SET  
      UserName = @UserName , Password = @Password, FirstName = @FirstName,
	  LastName = @LastName, Email = @Email, Phone = @Phone, MobileNo = @MobileNo
	  WHERE UserID = @UserID 
END

GO