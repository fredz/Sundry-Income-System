use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   26-05-2017

******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_ValidateUser]
@Username nvarchar(100),
@Password nvarchar(100)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [User] SET
		Uid = NEWID()
	WHERE Username=@Username AND Password = @Password 
	
	SELECT 
	UserID,
	[FirstName],
	[LastName], 
	[Email], 
	[Phone], 
	[MobileNo], 
	[Username],
	[Uid]
	FROM [dbo].[User] as us
	WHERE 
	us.Username=@Username AND 
	us.Password = @Password 
END

