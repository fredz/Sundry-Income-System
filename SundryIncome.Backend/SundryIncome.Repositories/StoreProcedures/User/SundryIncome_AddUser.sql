use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   24-05-2017

******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SundryIncome_AddUser]
    @FirstName varchar(100),
	@LastName  varchar(100),
	@Email     varchar(100),
	@MobileNo  varchar(100), 
	@Phone     varchar(100),
	@UserName   varchar(100),
	@Password   varchar(100),
	@Uid         varchar(100)
AS
BEGIN
      INSERT INTO [dbo].[User] 
      VALUES (@UserName, @Password, @FirstName, @LastName, @Email, @Phone, @MobileNo, @Uid)

	  SELECT SCOPE_IDENTITY()
END

GO

