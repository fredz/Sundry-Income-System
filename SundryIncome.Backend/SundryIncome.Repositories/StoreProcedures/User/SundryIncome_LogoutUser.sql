use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   26-05-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SundryIncome_LogoutUser]
      @UserName varchar(100),
	  @UserId varchar(100)
      
AS
BEGIN
      SET NOCOUNT ON;
      UPDATE [dbo].[User] SET [Uid] = 'NULL' WHERE Username = @UserName AND UserId = @UserId
END
GO