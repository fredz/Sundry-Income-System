use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   26-05-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[SundryIncome_VerifyUserByUserName]
	-- Add the parameters for the stored procedure here
      @UserName varchar(100)
      
AS
BEGIN
     SET NOCOUNT ON;
     IF EXISTS (SELECT * FROM [dbo].[User] WHERE UserName = @UserName)
     RETURN(1)
     ELSE
     RETURN(0)

END


GO


