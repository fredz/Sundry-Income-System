use [ServiceAccessPoint_Sandbox]
GO

/******
Adding Initial Script   Miguel   24-05-2017

******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SundryIncome_GetAllUsers]

AS
BEGIN
      SET NOCOUNT ON;
      select [UserID],[FirstName],[LastName], [Email], [Phone], [MobileNo], [Username] from [dbo].[User]  
END

GO