﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Repositories;

namespace SundryIncome.Repositories
{
    public class FranchiseeRepository : BaseRepository, IFranchiseeRepository
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["conString"].ToString();
        // crud
        public void Update(Franchisee franchisee)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateFranchisee", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@FranchiseeId", franchisee.FranchiseeId);
                        cm.Parameters.AddWithValue("@Code", franchisee.Code);
                        cm.Parameters.AddWithValue("@FirstName", franchisee.FirstName);
                        cm.Parameters.AddWithValue("@LastName", franchisee.LastName);
                        cm.Parameters.AddWithValue("@ContactName", franchisee.ContactName);
                        cm.Parameters.AddWithValue("@CompanyName", franchisee.CompanyName);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating franchisee on database", e);
                throw new SundryIncomeRepositoryException("Error updating franchisee on database", e);
            }
        }

        public Franchisee GetById(int id)
        {
            Franchisee franchisee  = new Franchisee();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetFranchiseeById", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            franchisee.FranchiseeId = int.Parse(rd[0].ToString());
                            franchisee.Code = int.Parse(rd[1].ToString());
                            franchisee.FirstName = (rd[2].ToString());
                            franchisee.LastName = (rd[3].ToString());
                            franchisee.ContactName = (rd[4].ToString());
                            franchisee.CompanyName = (rd[5].ToString());

                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting franchisee from database", e);
                throw new SundryIncomeRepositoryException("Error getting franchisee from database", e);
            }
            return franchisee;
        }

        public IList<Franchisee> GetAllFranchisees()
        {
            IList<Franchisee> resList = new List<Franchisee>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAllFranchisees", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            Franchisee franchisee = new Franchisee
                            {
                                FranchiseeId = int.Parse(rd[0].ToString()),
                                Code = int.Parse(rd[1].ToString()),
                                FirstName = (rd[2].ToString()),
                                LastName = (rd[3].ToString()),
                                ContactName = (rd[4].ToString()),
                                CompanyName = (rd[5].ToString())
                            };

                            resList.Add(franchisee);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all franchisees from database", e);
                throw new SundryIncomeRepositoryException("Error getting all franchisees from database", e);
            }
            return resList.ToList();
        }

        public Franchisee AddFranchisee(Franchisee franchisee)
        {
            Franchisee fr = new Franchisee();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_AddFranchisee", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Code", franchisee.Code);
                        cm.Parameters.AddWithValue("@FirstName", franchisee.FirstName);
                        cm.Parameters.AddWithValue("@LastName", franchisee.LastName);
                        cm.Parameters.AddWithValue("@ContactName", franchisee.ContactName);
                        cm.Parameters.AddWithValue("@CompanyName", franchisee.CompanyName);

                        cm.ExecuteNonQuery();

                        fr.Code = (int)cm.Parameters["@Code"].Value;
                        fr.FirstName = cm.Parameters["@FirstName"].Value.ToString();
                        fr.LastName = cm.Parameters["@LastName"].Value.ToString();
                        fr.ContactName = cm.Parameters["@ContactName"].Value.ToString();
                        fr.CompanyName = cm.Parameters["@CompanyName"].Value.ToString();
                        cn.Close();
                    }
                }
                
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error adding franchisee on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error adding franchisee on database", exception);
            }

            return fr;
        }

        public void Delete(int id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_DeleteFranchisee", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error deleting franchisee on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error deleting franchisee on database", exception);
            }
        }
    }
}
