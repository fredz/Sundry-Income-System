﻿using SundryIncome.Common.Interfaces.Helpers;
using SundryIncome.Common.Interfaces.Repositories;

namespace SundryIncome.Repositories
{
   public class BaseRepository : IBaseRepository
    {
        protected ISundryIncomeLogger _logger;

        public ISundryIncomeLogger Logger => _logger;

        public virtual void Configure(ISundryIncomeLogger logger)
        {
            _logger = logger;
        }
    }
}
