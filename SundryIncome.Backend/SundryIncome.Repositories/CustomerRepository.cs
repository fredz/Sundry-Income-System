﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Repositories;

namespace SundryIncome.Repositories
{
    public class CustomerRepository : BaseRepository, ICustomerRepository
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["conString"].ToString();
        // crud
        public void Update(Customer customer)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateCustomer", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cm.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cm.Parameters.AddWithValue("@LastName", customer.LastName);
                        cm.Parameters.AddWithValue("@BillingName", customer.BillingName);
                        cm.Parameters.AddWithValue("@Email", customer.Email);
                        cm.Parameters.AddWithValue("@Phone", customer.Phone);
                        cm.Parameters.AddWithValue("@MobileNo", customer.MobileNo);
                        cm.Parameters.AddWithValue("@PurchaseOrderRequired", customer.PurchaseOrderRequired);
                        cm.Parameters.AddWithValue("@AMCInvoiceOnly", customer.AMCInvoiceOnly);
                        cm.Parameters.AddWithValue("@DefaultInvoiceDescription", customer.DefaultInvoiceDescription);

                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating customer on database", e);
                throw new SundryIncomeRepositoryException("Error updating customer on database", e);
            }
        }

        public Customer GetById(int id)
        {
            Customer customer  = new Customer();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetCustomerById", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            customer.CustomerId = int.Parse(rd[0].ToString());
                            customer.FirstName = rd[1].ToString();
                            customer.LastName = rd[2].ToString();
                            customer.BillingName = rd[3].ToString();
                            customer.Email = rd[4].ToString();
                            customer.Phone= int.Parse(rd[5].ToString());
                            customer.MobileNo = int.Parse(rd[6].ToString());
                            customer.PurchaseOrderRequired = bool.Parse(rd[7].ToString());
                            customer.AMCInvoiceOnly = bool.Parse(rd[8].ToString());
                            customer.DefaultInvoiceDescription = rd[9].ToString();

                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting customer from database", e);
                throw new SundryIncomeRepositoryException("Error getting customer from database", e);
            }
            return customer;
        }

        public IList<Customer> GetAllCustomers()
        {
            IList<Customer> resList = new List<Customer>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAllCustomers", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            Customer customer = new Customer
                            {
                                CustomerId = int.Parse(rd[0].ToString()),
                                FirstName = rd[1].ToString(),
                                LastName = rd[2].ToString(),
                                BillingName = rd[3].ToString(),
                                Email = rd[4].ToString(),
                                Phone = int.Parse(rd[5].ToString()),
                                MobileNo = int.Parse(rd[6].ToString()),
                                PurchaseOrderRequired = bool.Parse(rd[7].ToString()),
                                AMCInvoiceOnly = bool.Parse(rd[8].ToString()),
                                DefaultInvoiceDescription = rd[9].ToString()
                            };
                               
                            resList.Add(customer);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all customers from database", e);
                throw new SundryIncomeRepositoryException("Error getting all customers from database", e);
            }
            return resList.ToList();
        }

        public Customer AddCustomer(Customer customer)
        {
            Customer cst = new Customer();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_AddCustomer", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                       
                        cm.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cm.Parameters.AddWithValue("@LastName", customer.LastName);
                        cm.Parameters.AddWithValue("@BillingName", customer.BillingName);
                        cm.Parameters.AddWithValue("@Email", customer.Email);
                        cm.Parameters.AddWithValue("@Phone", customer.Phone);
                        cm.Parameters.AddWithValue("@MobileNo", customer.MobileNo);
                        cm.Parameters.AddWithValue("@PurchaseOrderRequired", customer.PurchaseOrderRequired);
                        cm.Parameters.AddWithValue("@AMCInvoiceOnly", customer.AMCInvoiceOnly);
                        cm.Parameters.AddWithValue("@DefaultInvoiceDescription", customer.DefaultInvoiceDescription);

                        cm.ExecuteNonQuery();

                        cst.FirstName = cm.Parameters["@FirstName"].Value.ToString();
                        cst.LastName = cm.Parameters["@LastName"].Value.ToString();
                        cst.BillingName = cm.Parameters["@BillingName"].Value.ToString();
                        cst.Email = cm.Parameters["@Email"].Value.ToString();
                        cst.Phone= (int)cm.Parameters["@Phone"].Value;
                        cst.MobileNo =(int) cm.Parameters["@MobileNo"].Value;
                       
                        cn.Close();
                    }
                }
                
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error adding customer on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error adding customer on database", exception);
            }

            return cst;
        }

        public void Delete(int id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_DeleteCustomer", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error deleting customer on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error deleting customer on database", exception);
            }
        }
    }
}
