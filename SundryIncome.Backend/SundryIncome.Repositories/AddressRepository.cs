﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Repositories;

namespace SundryIncome.Repositories
{
    public class AddressRepository : BaseRepository, IAddressRepository
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["conString"].ToString();
        // crud
        public void Update(Address address)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateAddress", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@AddressId", address.AddressId);
                        cm.Parameters.AddWithValue("@Address", address.Addresss);
                        cm.Parameters.AddWithValue("@Locality", address.Locality);
                        cm.Parameters.AddWithValue("@State", address.State);
                        cm.Parameters.AddWithValue("@PostalCode", address.PostalCode);
                        cm.Parameters.AddWithValue("@CustomerId", address.CustomerId);
                        cm.Parameters.AddWithValue("@Type", address.Type);

                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating address on database", e);
                throw new SundryIncomeRepositoryException("Error updating address on database", e);
            }
        }

        public Address GetById(int id)
        {
            Address ad  = new Address();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAddressById", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            ad.AddressId = int.Parse(rd[0].ToString());
                            ad.Addresss = (rd[1].ToString());
                            ad.Locality = (rd[2].ToString());
                            ad.State = (rd[3].ToString());
                            ad.PostalCode = (rd[4].ToString());
                            ad.CustomerId = int.Parse(rd[5].ToString());
                            ad.Type = (rd[6].ToString());
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting address from database", e);
                throw new SundryIncomeRepositoryException("Error getting address from database", e);
            }
            return ad;
        }
        public IList<Address> GetAddressByCustomerId(int customerId)
        {
            IList<Address> resList = new List<Address>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAddressByCustomerId", cn))// customerid -> todos sus address
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", customerId);
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            Address address = new Address
                            {
                                AddressId = int.Parse(rd[0].ToString()),
                                Addresss = (rd[1].ToString()),
                                Locality = (rd[2].ToString()),
                                State = (rd[3].ToString()),
                                PostalCode = (rd[4].ToString()),
                                CustomerId = int.Parse(rd[5].ToString()),
                                Type = (rd[6].ToString()),
                                BillingName = (rd[7].ToString())
                            };

                            resList.Add(address);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all address by customer id from database", e);
                throw new SundryIncomeRepositoryException("Error getting all address by customer id from database", e);
            }
            return resList.ToList();
        }

        public IList<Address> GetAllAddresses()
        {
            IList<Address> resList = new List<Address>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAllAddresses", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            Address address = new Address
                            {
                                AddressId = int.Parse(rd[0].ToString()),
                                Addresss = (rd[1].ToString()),
                                Locality = (rd[2].ToString()),
                                State = (rd[3].ToString()),
                                PostalCode = (rd[4].ToString()),
                                CustomerId = int.Parse(rd[0].ToString()),
                                Type = (rd[4].ToString())
                            };

                            resList.Add(address);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all address from database", e);
                throw new SundryIncomeRepositoryException("Error getting all address from database", e);
            }
            return resList.ToList();
        }

        public Address AddAddress(Address address)
        {
            Address ad = new Address();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_AddAddress", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Address", address.Addresss);
                        cm.Parameters.AddWithValue("@Locality", address.Locality);
                        cm.Parameters.AddWithValue("@State", address.State);
                        cm.Parameters.AddWithValue("@PostalCode", address.PostalCode);
                        cm.Parameters.AddWithValue("@CustomerId", address.CustomerId);
                        cm.Parameters.AddWithValue("@Type", address.Type);
                        cm.ExecuteNonQuery();

                        ad.Addresss = cm.Parameters["@Address"].Value.ToString();
                        ad.Locality = cm.Parameters["@Locality"].Value.ToString();
                        ad.State = cm.Parameters["@State"].Value.ToString();
                        ad.PostalCode = cm.Parameters["@PostalCode"].Value.ToString();
                        ad.CustomerId = (int)cm.Parameters["@CustomerId"].Value;
                        ad.Type = cm.Parameters["@Type"].Value.ToString();
                        cn.Close();
                    }
                }
                
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error adding address on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error adding address on database", exception);
            }

            return ad;
        }

        public void Delete(int id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_DeleteAddress", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error deleting address on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error deleting address on database", exception);
            }
        }
    }
}
