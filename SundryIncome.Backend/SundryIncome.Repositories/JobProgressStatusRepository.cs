﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Repositories;

namespace SundryIncome.Repositories
{
    public class JobProgressStatusRepository : BaseRepository, IJobProgressStatusRepository
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["conString"].ToString();
        // crud
        
        public IList<JobProgressStatus> GetAllJobProgressStatus()
        {
            IList<JobProgressStatus> resList = new List<JobProgressStatus>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAllJobProgressStatus", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            JobProgressStatus jobProgressStatus = new JobProgressStatus
                            {
                                JobId = int.Parse(rd[0].ToString()),
                                CustomerId = int.Parse(rd[1].ToString()),
                                CustomerFullName = (rd[2].ToString()),
                                AreaManagerId = int.Parse(rd[3].ToString()),
                                AManagerFullName = (rd[4].ToString()),
                                OrderNo = int.Parse(rd[5].ToString()),
                                Description = rd[6].ToString(),
                                RequestedDate = (rd[7] == DBNull.Value ? (DateTime?)null : DateTime.Parse(rd[7].ToString())),
                                DateFrom = (rd[8] == DBNull.Value ? (DateTime?)null : DateTime.Parse(rd[8].ToString())),
                                DateTo = (rd[9] == DBNull.Value ? (DateTime?)null : DateTime.Parse(rd[9].ToString())),
                                DateCompleted = (rd[10] == DBNull.Value ? (DateTime?)null : DateTime.Parse(rd[10].ToString())),
                                AMCAmount = (rd[11] == DBNull.Value ? (float?)null : float.Parse(rd[11].ToString())),
                                SalesInvoiceNo = (rd[12] == DBNull.Value ? (int?)null : int.Parse(rd[12].ToString())),
                                SalesInvoiceDate = (rd[13] == DBNull.Value ? (DateTime?)null : DateTime.Parse(rd[13].ToString())),
                                JobProgressId = int.Parse(rd[14].ToString()),
                                StepNo = int.Parse(rd[15].ToString()),
                                JobStatusId = int.Parse(rd[16].ToString()),
                                Name = rd[17].ToString(),
                                ColorCode = rd[18].ToString(),
                                Rank = int.Parse(rd[19].ToString())
                            };
                               
                            resList.Add(jobProgressStatus);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all JobProgressStatus from database", e);
                throw new SundryIncomeRepositoryException("Error getting all JobProgressStatus from database", e);
            }
            return resList.ToList();
        }

        public JobProgressStatus AddJobProgressStatus(JobProgressStatus jobProgressStatus)
        {
            JobProgressStatus nJobProgressStatus = new JobProgressStatus();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_AddJobProgressStatus", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();

                        cm.Parameters.AddWithValue("@CustomerId", jobProgressStatus.CustomerId);
                        cm.Parameters.AddWithValue("@AreaManagerId", jobProgressStatus.AreaManagerId);
                        cm.Parameters.AddWithValue("@OrderNo", jobProgressStatus.OrderNo);
                        cm.Parameters.AddWithValue("@Description", jobProgressStatus.Description);
                        cm.Parameters.AddWithValue("@RequestedDate", jobProgressStatus.RequestedDate);
                        cm.Parameters.AddWithValue("@DateFrom", jobProgressStatus.DateFrom);
                        cm.Parameters.AddWithValue("@DateTo", jobProgressStatus.DateTo);
                        //cm.Parameters.AddWithValue("@DateCompleted", job.DateCompleted);
                        cm.Parameters.AddWithValue("@AMCAmount", jobProgressStatus.AMCAmount);
                        // cm.Parameters.AddWithValue("@CreatedOn", job.CreatedOn);
                        cm.Parameters.AddWithValue("@CreatedBy", jobProgressStatus.CreatedBy);
                        cm.Parameters.AddWithValue("@StepNo", jobProgressStatus.StepNo);
                        cm.Parameters.AddWithValue("@JobStatusId", jobProgressStatus.JobStatusId);
                        // cm.Parameters.AddWithValue("@ModifiedOn", job.ModifiedOn);
                        // cm.Parameters.AddWithValue("@ModifiedBy", job.ModifiedBy);
                        // cm.Parameters.AddWithValue("@SalesInvoiceNo", job.SalesInvoiceNo);
                        // cm.Parameters.AddWithValue("@SalesInvoiceDate", job.SalesInvoiceDate);

                        cm.ExecuteNonQuery();

                        nJobProgressStatus.AreaManagerId = (int)cm.Parameters["@AreaManagerId"].Value;
                        nJobProgressStatus.OrderNo = (int)cm.Parameters["@OrderNo"].Value;
                        nJobProgressStatus.Description = cm.Parameters["@Description"].Value.ToString();
                        nJobProgressStatus.RequestedDate = (cm.Parameters["@RequestedDate"].Value == DBNull.Value ? (DateTime?)null : (DateTime)cm.Parameters["@RequestedDate"].Value);
                        nJobProgressStatus.DateFrom = (cm.Parameters["@DateFrom"].Value == DBNull.Value ? (DateTime?)null : (DateTime)cm.Parameters["@DateFrom"].Value);
                        nJobProgressStatus.DateTo = (cm.Parameters["@DateTo"].Value == DBNull.Value ? (DateTime?)null : (DateTime)cm.Parameters["@DateTo"].Value);
                        // nJob.DateCompleted = (cm.Parameters["@DateCompleted"].Value == DBNull.Value ? (DateTime?)null : (DateTime)cm.Parameters["@DateCompleted"].Value);
                        nJobProgressStatus.AMCAmount = (cm.Parameters["@AMCAmount"].Value == DBNull.Value ? (float?)null : (float)cm.Parameters["@AMCAmount"].Value);
                        // nJob.CreatedOn = (DateTime)cm.Parameters["@CreatedOn"].Value;                        
                        nJobProgressStatus.CreatedBy = (cm.Parameters["@CreatedBy"].Value == DBNull.Value ? (int?)null : (int)cm.Parameters["@CreatedBy"].Value);
                        nJobProgressStatus.StepNo = (int)cm.Parameters["@StepNo"].Value;
                        nJobProgressStatus.JobStatusId = (int)cm.Parameters["@JobStatusId"].Value;
                        // nJob.ModifiedOn = (DateTime)cm.Parameters["@ModifiedOn"].Value;
                        //  nJob.ModifiedBy = (cm.Parameters["@ModifiedBy"].Value == DBNull.Value ? (int?)null : (int) cm.Parameters["@ModifiedBy"].Value);
                        // nJob.SalesInvoiceNo = (cm.Parameters["@SalesInvoiceNo"].Value == DBNull.Value ? (int?)null : (int) cm.Parameters["@SalesInvoiceNo"].Value);
                        //  nJob.SalesInvoiceDate = (cm.Parameters["@SalesInvoiceDate"].Value == DBNull.Value ? (DateTime?)null : (DateTime)cm.Parameters["@SalesInvoiceDate"].Value);

                        cn.Close();
                    }
                }

            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error adding jobProgressStatus on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error adding jobProgressStatus on database", exception);
            }

            return nJobProgressStatus;
        }

        public void UpdateJobProgressStatus(JobProgressStatus jobProgressStatus)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateJobProgressStatus", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@JobId", jobProgressStatus.JobId);
                        cm.Parameters.AddWithValue("@JobProgressId", jobProgressStatus.JobProgressId);
                        cm.Parameters.AddWithValue("@JobStatusId", jobProgressStatus.JobStatusId);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating jobProgressStatus on database", e);
                throw new SundryIncomeRepositoryException("Error updating jobProgressStatus on database", e);
            }
        }

    }
}
