﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Interfaces.Repositories;
using System.Configuration;
using SundryIncome.Domain.Entities;
using System.Data.SqlClient;
using System.Data;
using SundryIncome.Common.Exceptions;

namespace SundryIncome.Repositories
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["conString"].ToString();

        // crud
        public void UpdateUser(User user)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateUser", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@UserID", user.UserId);
                        cm.Parameters.AddWithValue("@Username", user.Username);
                        cm.Parameters.AddWithValue("@Password", user.Password);
                        cm.Parameters.AddWithValue("@FirstName", user.FirstName);
                        cm.Parameters.AddWithValue("@LastName", user.LastName);
                        cm.Parameters.AddWithValue("@Email", user.Email);
                        cm.Parameters.AddWithValue("@Phone", user.Phone);
                        cm.Parameters.AddWithValue("@MobileNo", user.MobileNo);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating user on database", e);
                throw new SundryIncomeRepositoryException("Error updating user on database", e);
            }
        }

        public User GetUserById(int id)
        {
            User resultUser = new User();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetUserById", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            resultUser.UserId = int.Parse(rd[0].ToString());
                            resultUser.FirstName = rd[1].ToString();
                            resultUser.LastName = rd[2].ToString();
                            resultUser.Email = rd[3].ToString();
                            resultUser.Phone = rd[4].ToString();
                            resultUser.MobileNo = rd[5].ToString();
                            resultUser.Username = rd[6].ToString();
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting user from database", e);
                throw new SundryIncomeRepositoryException("Error getting user from database", e);
            }
            return resultUser;
        }

        public User AddUser(User user)
        {
            User resultUser = new User();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_AddUser", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Username", user.Username);
                        cm.Parameters.AddWithValue("@Password", user.Password);
                        cm.Parameters.AddWithValue("@FirstName", user.FirstName);
                        cm.Parameters.AddWithValue("@LastName", user.LastName);
                        cm.Parameters.AddWithValue("@Email", user.Email);
                        cm.Parameters.AddWithValue("@Phone", user.Phone);
                        cm.Parameters.AddWithValue("@MobileNo", user.MobileNo);
                        cm.Parameters.AddWithValue("@Uid", user.Uid);

                        var rd = cm.ExecuteReader();
                        rd.Read();
                        resultUser.UserId = int.Parse(rd[0].ToString());

                        resultUser.FirstName = cm.Parameters["@FirstName"].Value.ToString();
                        resultUser.LastName = cm.Parameters["@LastName"].Value.ToString();
                        resultUser.Email = cm.Parameters["@Email"].Value.ToString();
                        resultUser.Phone = cm.Parameters["@Phone"].Value.ToString();
                        resultUser.MobileNo = cm.Parameters["@MobileNo"].Value.ToString();

                        cn.Close();
                    }
                }

            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error adding user on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error adding user on database", exception);
            }

            return resultUser;
        }

        public void DeleteUser(int id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_DeleteUser", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error deleting user on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error deleting user on database", exception);
            }
        }

        public IList<User> GetAllUsers()
        {
            IList<User> resList = new List<User>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAllUsers", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            User us = new User();
                            us.UserId = int.Parse(rd[0].ToString());
                            us.FirstName = rd[1].ToString();
                            us.LastName = rd[2].ToString();
                            us.Email = rd[3].ToString();
                            us.Phone = rd[4].ToString();
                            us.MobileNo = rd[5].ToString();
                            us.Username = rd[6].ToString();
                            resList.Add(us);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all users from database", e);
                throw new SundryIncomeRepositoryException("Error getting all users from database", e);
            }
            return resList.ToList();
        }

        public bool VerifyIfUserNameExists(string userName)
        {
            bool result;
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_VerifyUserByUserName", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@UserName", userName);

                        SqlParameter returnValueParam = cm.Parameters.Add("@return_value", SqlDbType.Bit);
                        returnValueParam.Direction = ParameterDirection.ReturnValue;
                        cm.ExecuteNonQuery();

                        result = Convert.ToBoolean(returnValueParam.Value);
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error verifying if user name exists  on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error verifying if user name exists  on database ", exception);
            }
            return result;
        }

        public void Logout(string userName, int userId)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_LogoutUser", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@UserName", userName);
                        cm.Parameters.AddWithValue("@UserId", userId);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error trying to logout user on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error trying to logout user on database", exception);
            }
        }

        public User Validate(string userName, string password)
        {
            User resultUser = new User();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_ValidateUser", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@UserName", userName);
                        cm.Parameters.AddWithValue("@Password", password);
                        
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            resultUser.UserId = int.Parse(rd[0].ToString());
                            resultUser.FirstName = rd[1].ToString();
                            resultUser.LastName = rd[2].ToString();
                            resultUser.Email = rd[3].ToString();
                            resultUser.Phone = rd[4].ToString();
                            resultUser.MobileNo = rd[5].ToString();
                            resultUser.Username = rd[6].ToString();
                            resultUser.Uid = rd[7].ToString();
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error trying to logout user on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error trying to logout user on database", exception);
            }
            return resultUser;
        }
    }
}
