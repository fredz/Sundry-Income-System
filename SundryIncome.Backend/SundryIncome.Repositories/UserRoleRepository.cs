﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Repositories;

namespace SundryIncome.Repositories
{
    public class UserRoleRepository : BaseRepository, IUserRoleRepository
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["conString"].ToString();
        // crud
        public int Update(UserRole userRole)
        {
            int rows = -1;
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateUserRole", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@UserId", userRole.UserId);
                        cm.Parameters.AddWithValue("@RoleId", userRole.RoleId);
                        cm.Parameters.AddWithValue("@Disabled", userRole.Disabled);
                        rows = cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating UserRole on database", e);
                throw new SundryIncomeRepositoryException("Error updating UserRole on database", e);
            }

            return rows;
        }

        public IList<UserRole> GetByUserId(int id)
        {
            IList<UserRole> resList = new List<UserRole>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetUserRolesByUserID", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cm.Parameters.AddWithValue("@UserId", id);

                        cn.Open();
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            UserRole rl = new UserRole();
                            rl.UserRoleId = int.Parse(rd[0].ToString());
                            rl.UserId = int.Parse(rd[1].ToString());
                            rl.RoleId = int.Parse(rd[2].ToString());
                            rl.Disabled = int.Parse(rd[3].ToString());
                            rl.RoleName = rd[4].ToString();
                            rl.RoleRank = int.Parse(rd[5].ToString());

                            resList.Add(rl);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all UserRoles from database", e);
                throw new SundryIncomeRepositoryException("Error getting all UserRoles from database", e);
            }
            return resList.ToList();
        }

        public IList<UserRole> GetAllUserRoles()
        {
            IList<UserRole> resList = new List<UserRole>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAllUserRoles", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            UserRole rl = new UserRole();
                            rl.UserRoleId = int.Parse(rd[0].ToString());
                            rl.UserId = int.Parse(rd[0].ToString());
                            rl.RoleId = int.Parse(rd[0].ToString());
                            rl.Disabled = int.Parse(rd[0].ToString());

                            resList.Add(rl);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all UserRoles from database", e);
                throw new SundryIncomeRepositoryException("Error getting all UserRoles from database", e);
            }

            return resList.ToList();
        }

        public UserRole AddUserRole(UserRole userRole)
        {
            UserRole resultRole = new UserRole();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_AddUserRole", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@UserId", userRole.UserId);
                        cm.Parameters.AddWithValue("@RoleId", userRole.RoleId);
                        cm.ExecuteNonQuery();

                        resultRole.UserId = (int) cm.Parameters["@UserId"].Value;
                        resultRole.RoleId = (int) cm.Parameters["@RoleId"].Value;
                        resultRole.Disabled = 0;

                        cn.Close();
                    }
                }
                
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error adding USerRole on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error adding UserRole on database", exception);
            }

            return resultRole;
        }

        public void Delete(int userId, int roleId)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_DeleteUserRole", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@UserId", userId);
                        cm.Parameters.AddWithValue("@RoleId", roleId);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error deleting UserRole on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error deleting UserRole on database", exception);
            }
        }
    }
}
