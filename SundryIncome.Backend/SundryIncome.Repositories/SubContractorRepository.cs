﻿using SundryIncome.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SundryIncome.Common.Exceptions;
using SundryIncome.Common.Interfaces.Repositories;

namespace SundryIncome.Repositories
{
    public class SubContractorRepository : BaseRepository, ISubContractorRepository
    {
        private readonly string _connStr = ConfigurationManager.ConnectionStrings["conString"].ToString();
        // crud
        public void Update(SubContractor sContractor)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_UpdateSubContractor", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@SubContractorId", sContractor.SubContractorId);
                        cm.Parameters.AddWithValue("@FirstName", sContractor.FirstName);
                        cm.Parameters.AddWithValue("@LastName", sContractor.LastName);
                        cm.Parameters.AddWithValue("@CompanyName", sContractor.CompanyName);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error updating subcontractor on database", e);
                throw new SundryIncomeRepositoryException("Error updating subcontractor on database", e);
            }
        }

        public SubContractor GetById(int id)
        {
            SubContractor rSubContractor  = new SubContractor();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetSubContractorById", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            rSubContractor.SubContractorId = int.Parse(rd[0].ToString());
                            rSubContractor.FirstName = (rd[1].ToString());
                            rSubContractor.LastName = (rd[2].ToString());
                            rSubContractor.CompanyName = (rd[3].ToString());
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting subcontractor from database", e);
                throw new SundryIncomeRepositoryException("Error getting subcontractor from database", e);
            }
            return rSubContractor;
        }

        public IList<SubContractor> GetAllSubContractors()
        {
            IList<SubContractor> resList = new List<SubContractor>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_GetAllSubContractors", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        SqlDataReader rd = cm.ExecuteReader();
                        while (rd.Read())
                        {
                            SubContractor sc = new SubContractor();
                            sc.SubContractorId = int.Parse(rd[0].ToString());
                            sc.FirstName = (rd[1].ToString());
                            sc.LastName =  (rd[2].ToString());
                            sc.CompanyName = (rd[3].ToString());

                            resList.Add(sc);
                        }
                        cn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.ErrorToPortal("Error getting all subcontractors from database", e);
                throw new SundryIncomeRepositoryException("Error getting all subcontractors from database", e);
            }
            return resList.ToList();
        }

        public SubContractor AddSubContractor(SubContractor sContractor)
        {
            SubContractor resContractor = new SubContractor();
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_AddSubContractor", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@FirstName", sContractor.FirstName);
                        cm.Parameters.AddWithValue("@LastName", sContractor.LastName);
                        cm.Parameters.AddWithValue("@CompanyName", sContractor.CompanyName);
                        cm.ExecuteNonQuery();

                        resContractor.FirstName = cm.Parameters["@FirstName"].Value.ToString();
                        resContractor.LastName = cm.Parameters["@LastName"].Value.ToString();
                        resContractor.CompanyName = cm.Parameters["@CompanyName"].Value.ToString();
                        cn.Close();
                    }
                }
                
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error adding subcontractor on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error adding subcontractor on database", exception);
            }

            return resContractor;
        }

        public void Delete(int id)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(_connStr))
                {
                    using (SqlCommand cm = new SqlCommand("SundryIncome_DeleteSubContractor", cn))
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cn.Open();
                        cm.Parameters.AddWithValue("@Id", id);
                        cm.ExecuteNonQuery();
                        cn.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                var errors = string.Empty;
                _logger.ErrorToPortal("Error deleting subcontractor on database " + errors, exception);
                throw new SundryIncomeRepositoryException("Error deleting subcontractor on database", exception);
            }
        }
    }
}
