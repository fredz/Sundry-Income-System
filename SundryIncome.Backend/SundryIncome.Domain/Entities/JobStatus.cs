﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Domain.Entities
{
   public class JobStatus
    {
        public int JobStatusId { get; set; }
        public string Name { get; set; }
        public string ColorCode { get; set; }
        public int Rank { get; set; }
    }
}
