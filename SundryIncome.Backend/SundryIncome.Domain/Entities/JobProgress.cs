﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Domain.Entities
{
   public class JobProgress
    {
        public int JobProgressId { get; set; }
        public int JobId { get; set; }
        public int JobStatusId { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy  { get; set; }
        public int StepNo { get; set; }
    }
}
