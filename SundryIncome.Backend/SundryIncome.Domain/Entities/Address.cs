﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Domain.Entities
{
    public class Address
    {
        public int AddressId { get; set; }
        public string Addresss { get; set; }
        public string Locality { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public int CustomerId { get; set; }
        public string Type { get; set; }
        public string BillingName { get; set; }

    }
}
