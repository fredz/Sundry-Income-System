﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Domain.Entities
{
    public class Franchisee
    {
        public int FranchiseeId { get; set; }
        public int Code { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactName { get; set; }
        public string CompanyName { get; set; }

    }
}
