﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SundryIncome.Domain.Entities
{
    public class Notification
    {
        public int NotificationId { get; set; }
        public int JobId { get; set; }
        public DateTime Date { get; set; }
        public string Content { get; set; }
        public string SendTo { get; set; }
        public string TypeNotification { get; set; }
    }
}
