import * as React from 'react';
import { Grid } from 'semantic-ui-react';
import { connect } from 'react-redux';

import Body from './Layout/Body';
import Login from '../root/Login';
import { pages } from '../constants/pages';
import { Loadable } from './Generic/Loadable';
import Alert from 'react-s-alert';

export interface IMainProps {}

interface IConnectedProps {
  pageCode: pages;
  isLoading: boolean;  
}

class Main extends React.Component<IMainProps & IConnectedProps, {}> {
  constructor(props) {
    super(props);

    this.checkIfLogged = this.checkIfLogged.bind(this);
  }

  checkIfLogged() {
    const { pageCode } = this.props;
    if (pageCode === pages.general_login) {
      return <Login/>;
    }

    return <Body/>;
  }

  render() {
    const component = this.checkIfLogged();    

    return(       
      <Loadable isLoading={this.props.isLoading} text="Loading">
        <div style={{width: '50%' }}>
          <Alert stack={{limit: 3}} />
        </div>
        <Grid className="MainGrid">
          {component}
        </Grid>
      </Loadable> 
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    isLoading: state.app.isLoading,    
  };
}

export default
connect(mapStateToProps)(Main) as React.ComponentClass<IMainProps>;