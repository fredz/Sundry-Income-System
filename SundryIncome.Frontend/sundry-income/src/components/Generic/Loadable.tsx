import * as React from 'react';
import { Dimmer, Loader, Image, Segment, Grid } from 'semantic-ui-react';

export interface ILoadableProps {  
  isLoading: boolean;
  text: string;  
}

export interface ILoadableState {    
}

export class Loadable extends React.Component<ILoadableProps, ILoadableState> {
  constructor(props) {
    super(props);
  }  

  render() {
    const { isLoading, text } = this.props;

    return (
      <div style={{width: '100%', height: '100%'}} >
        <Dimmer active={this.props.isLoading}>
          <Loader>{text}</Loader>
        </Dimmer>
        {this.props.children}
      </div>
    );
  }

}