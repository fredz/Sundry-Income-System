import * as React from 'react';
import { Header, Table, Rating, Icon } from 'semantic-ui-react';

export interface IListingProps {
  columns: any;
  data: any;
  rowOptions?: any;
}

export interface IListingState {    
}

export class Listing extends React.Component<IListingProps, IListingState> {
  constructor(props) {
    super(props);
  }

  headerList() {  
    const itemsHeader = this.props.columns;
    const listHeader = itemsHeader.map((item, index) =>
      (
        <Table.HeaderCell key={index} {...item.options} >
          {item.header}
        </Table.HeaderCell>        
      )
    );
    
    return (
      <Table.Row>{listHeader}</Table.Row>
    );
  }

  dataList() {      
    const itemsData = this.props.data;   
    let cells: any;
   
    const tableBody = itemsData.map((row, index) => {      
      cells = this.prepareCell(row);
      const options = this.prepareRowOptions(index);    
      
      return (
        <Table.Row {...options} key={index}>{cells}</Table.Row>
      );
    });    

    return tableBody;  
  }

  prepareRowOptions(index) {
    const optionArray = this.props.rowOptions; 
    let options = {};    

    if (optionArray !== undefined) {
      if (optionArray[index] !== undefined) {
        options = optionArray[index];
      }
    } 

    return options;
  }

  prepareCell(row) {
    const itemsHeader = this.props.columns;  
    let cellValue: any; 
    let foo: any; 

    const cells = itemsHeader.map((rowHeader, key) => {
      
      cellValue = row[rowHeader.dataField];
      foo = rowHeader.dataFormat;      

      if (typeof foo === 'function') {                  
        cellValue = foo(cellValue, row);
      }

      return (
        <Table.Cell key={key} {...rowHeader.options}>
          {cellValue}
        </Table.Cell>          
      );
    });

    return cells;
  }      

  buildTable = () => {

    const columns = this.headerList();
    const datas = this.dataList();

    return (
      <Table celled={true} padded={true} selectable={true}>
        <Table.Header>
          {columns}
        </Table.Header>

        <Table.Body>
          {datas}
        </Table.Body>
      </Table>
    );
  } 

  render() {
    const tableContent = this.buildTable();

    return (
      <div>
        {tableContent}
      </div>
    );
  }

}