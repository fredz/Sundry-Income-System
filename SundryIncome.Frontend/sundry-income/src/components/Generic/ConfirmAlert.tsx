import * as React from 'react';
import { Modal, Button } from 'semantic-ui-react';

export interface IConfirmAlertProps {  
}

export interface IConfirmAlertState {   
  open: boolean;
  size: any;    
  message: string;    
  title: string;
  yesAction: () => void;
}

export class ConfirmAlert extends React.Component<IConfirmAlertProps, IConfirmAlertState> {
  constructor(props) {
    super(props);

    this.state = {       
      open: false,
      size: 'small',
      message: '',
      title: ''     
    } as IConfirmAlertState;  
    
    this.handleConfirm = this.handleConfirm.bind(this);
    this.close = this.close.bind(this);
  }

  handleConfirm(title: string, message: string, open: boolean, yesAction: any, noAction: any = {}) {
    this.setState({
      message,
      title,
      yesAction,
      open,
    } as IConfirmAlertState);
  }

  close() {
    this.setState({
      open: false,
    } as IConfirmAlertState);    
  }

  render() {
    const {message, title, yesAction, size, open} = this.state;

    return (

      <Modal size={size} open={open} onClose={this.close}>
        <Modal.Header>
          {title}
        </Modal.Header>
        <Modal.Content>
          <p>{message}</p>
        </Modal.Content>
        <Modal.Actions>
          <Button negative={true} onClick={(e) => {this.close(); }}>
            No
          </Button>
          <Button positive icon="checkmark" 
            labelPosition="right" 
            content="Yes" 
            onClick={(e) => {this.close(); yesAction()}} 
          />
        </Modal.Actions>
      </Modal>

    );

  }

}