import * as React from 'react';
import { Message } from 'semantic-ui-react';

export interface IAlertProps {  
  header: string;
  content: string;  
  visible: boolean;   
  type: string;  
}

export interface IAlertState {   
  isVisible: boolean;    
}

export class Alert extends React.Component<IAlertProps, IAlertState> {
  constructor(props) {
    super(props);

    this.state = {       
      isVisible: false      
    } as IAlertState;     

  }

  componentWillReceiveProps() {

    this.setState({ isVisible: true });

    setTimeout(function() {      
      this.setState({isVisible: false});
    }.bind(this), 12000);     
  }

  handleDismiss = () => {
    this.setState({ isVisible: false });    
  }  

  render() {

    const { header, content, type } = this.props;

    const iconAlert =  {
      info: 'warning sign',
      success: 'check circle',
      error: 'remove circle'
    };    
    
    const typeMessage: any = {};
    typeMessage[type] = true; 

    return (
      <Message 
        icon={iconAlert[type]}
        header={header}        
        content={content} {...typeMessage}
        hidden={!this.state.isVisible} 
        visible={false}        
        onDismiss={this.handleDismiss} />      
    );

  }

}