import * as React from 'react';
import { Input } from 'semantic-ui-react';

export interface IInputCellProps {  
  onBlur: (value: string, rowId: string) => void;
  rowId: string;
  value: string;
}

export interface IInputCellState {   
  value: string;
  showInput: boolean;  
}

export class InputCell extends React.Component<IInputCellProps, IInputCellState> {
  constructor(props) {
    super(props);

    this.state = {       
      value: '',
      showInput: false,  
      focus: true
    } as IInputCellState;

    if (this.props.value !== undefined) {
      this.state = {
        value: this.props.value
      } as IInputCellState;      
    }
    
    this.handleInputChange = this.handleInputChange.bind(this);
    this.toogleLabel = this.toogleLabel.bind(this);
  }
  
  handleInputChange(prop, value) {
    const state = {};
    state[prop] = value;

    this.setState(state as IInputCellState);
  }  

  toogleLabel() {    
    this.setState({
      showInput: !this.state.showInput
    });    
  }

  render() {
    const {showInput, value} = this.state;
    
    if (showInput) {
      return (
        <Input
          id="value"
          type="text"        
          value={value}        
          onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}          
          onBlur={(e: any) => { this.props.onBlur(this.state.value, this.props.rowId); this.toogleLabel(); }}  
          autoFocus={true}
        />
      );
    }else {
      return(
        <div><a href="#" onClick={this.toogleLabel}>{value}</a></div>
      );      
    }
  }
}