import * as React from 'react';
import { Sidebar, Segment, Button, Menu, Image, Icon, Header, Grid } from 'semantic-ui-react';
import { AMCHeader } from './AMCHeader';

import { connect } from 'react-redux';
import { navigateTo, showLoadable } from '../../actions/app';
import { logoutUser, getPage } from '../../utils/sessionManager';
import { pages } from '../../constants/pages';

/** General Pages */
import Maintenance from '../../root/Maintenance/Maintenance';
import SundryJobs from '../../root/SundryJobs/SundryJobs';

export interface IBodyState {
  visible: boolean;
}

export interface IBodyProps { }

interface IConnectedProps {
  pageCode: pages;  
}

interface IConnectedDispatch {
  handleNavigateTo: (value: pages) => void;
  showLoadable: (isLoading: boolean) => void;
}

class Body extends React.Component<IBodyProps & IConnectedProps & IConnectedDispatch, IBodyState> {
  constructor(props) {
    super(props);

    this.state = { 
      visible: getPage() !== null, 
    };

    this.toggleVisibility = this.toggleVisibility.bind(this);
    this.logout = this.logout.bind(this);
    this.getContent = this.getContent.bind(this);
    this.goTo = this.goTo.bind(this);
  }

  toggleVisibility = () => this.setState({ visible: !this.state.visible });

  logout() {

    this.props.showLoadable(true);
    logoutUser(() => {
      this.props.showLoadable(false);
      this.props.handleNavigateTo(pages.general_login);
    });    
  }

  getContent() {
    const { pageCode } = this.props;

    switch (pageCode) {
      case pages.general_main: 
        return <span>Welcome Page</span>;

      case pages.general_sundryjobs:
        return <SundryJobs/>;

      case pages.general_maintenance:
        return <Maintenance/>;

      default: 
        return <span>Content to be completed</span>;
    }
  }

  goTo(pageCode: pages) {
    this.props.handleNavigateTo(pageCode);

    this.setState({
      visible: !this.state.visible
    });
  }

  render() {
    const { visible } = this.state;

    const content = this.getContent();

    return (      
      <Grid.Column stretched={true} className="Reset">        
        <AMCHeader onLogoClick={this.toggleVisibility} buttonVisible={true}/>
        <Sidebar.Pushable as={Segment} className="Reset">
          <Sidebar as={Menu} animation="overlay" width="thin" 
            visible={visible} icon="labeled" vertical={true} inverted={true}>
            <Menu.Item name="close" onClick={this.toggleVisibility}>
              <Icon name="chevron left" />
              Close
            </Menu.Item>
            <Menu.Item name="dashboard" onClick={() => this.goTo(pages.general_dashboard)}>
              <Icon name="dashboard" />
              Dashboard
            </Menu.Item>
            <Menu.Item name="jobs" onClick={() => this.goTo(pages.general_sundryjobs)}>
              <Icon name="tasks" />
              Sundry Jobs
            </Menu.Item>
            <Menu.Item name="maintenance" onClick={() => this.goTo(pages.general_maintenance)}>
              <Icon name="database" />
              Maintenance
            </Menu.Item>            

            <Menu.Item name="maintenance" onClick={this.logout}>
              <Icon name="sign out" />
              Logout
            </Menu.Item>
          </Sidebar>
          <Sidebar.Pusher>
            <Segment basic={true} className="Reset">                
              {content}
            </Segment>
          </Sidebar.Pusher>
        </Sidebar.Pushable>        
      </Grid.Column>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode    
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    handleNavigateTo: (value: pages) => dispatch(navigateTo(value)),
    showLoadable: (isLoading: boolean) => dispatch(showLoadable(isLoading))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Body) as React.ComponentClass<IBodyProps>;
