import * as React from 'react';
import { Sidebar, Segment, Button, Menu, Image, Icon, Header, Grid } from 'semantic-ui-react';

export interface ITopmenuProps {  
  menu: any[];
  onItemClick: (name: string) => void;
}

export interface ITopmenuState {  
  activeItem: string;  
}

export class Topmenu extends React.Component<ITopmenuProps, ITopmenuState> {
  constructor(props) {
    super(props);

    this.state = { 
      activeItem: 'Home',      
    };

    this.handleItemClick = this.handleItemClick.bind(this);

  }

  handleItemClick(e, { name }) {
    this.setState({ activeItem: name });

    this.props.onItemClick(name);
  }

  render() {
    const { activeItem } = this.state;
    return (
      <Menu inverted={true} className="Reset">
        {
          this.props.menu.map((item, index) => {
              return (
                <Menu.Item 
                  key={index}
                  index={index} 
                  name={item.name} 
                  active={activeItem === item.name} 
                  onClick={this.handleItemClick} />
              );
          })
        }
      </Menu>
    );
  }

}