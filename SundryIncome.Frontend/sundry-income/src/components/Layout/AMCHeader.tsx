import * as React from 'react';
import { Sidebar, Segment, Button, Menu, Image, Icon, Header, Grid } from 'semantic-ui-react';

export interface IAMCHeaderProps {
  onLogoClick: () => void;
  buttonVisible?: boolean;
}

export class AMCHeader extends React.Component<IAMCHeaderProps, {}> {
  constructor(props) {
    super(props);
  }

  render() {
    const withButtons = this.props.buttonVisible !== undefined ? this.props.buttonVisible : false;
    return (
      <Segment className="Reset">
        <div style={{height: '80px', background: 'rgba(66, 162, 220, 0.5)'}}>

          { withButtons ?
            <div style={{
              fontFamily: 'Josefin Slab, serif', fontSize: '20px', color: '#666', 
              padding: '20px', paddingTop: '25px', float: 'left', cursor: 'pointer'
            }} onClick={() => this.props.onLogoClick()}>
              Toggle Menu <Icon className="ellipsis vertical" />
            </div> : null 
            }
          <div style={{
            fontFamily: 'Satisfy, cursive', fontSize: '40px', color: '#666', 
            padding: '20px', paddingTop: '25px', float: 'right'
          }}>
            Service Access Point
          </div>

        </div>
        {this.props.children}
      </Segment>
    );
  }
}
