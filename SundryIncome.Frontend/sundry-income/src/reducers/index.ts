import { combineReducers } from 'redux';
import { handleActions, Action } from 'redux-actions';
import { IAppState, ISubPageData, IAlertState } from '../state';
import { pages, subPages } from '../constants/pages';

import { NAVIGATE_TO, SWITCH_SUB_PAGE, DISPLAY_ALERT, SHOW_LOADABLE } from '../actions/app';
import { setPage, setSubPage, getSubPage, getPage } from '../utils/sessionManager';

const DEFAULT_STATE: IAppState = {
  pageCode: pages.general_login,
  subPageCode: null,  
  pageData: null,
  subPageData: null,
  isLoading: false,
};

const DEFAULT_ALERT_STATE: IAlertState = {
  visible: false,
  header: '',
  content: '',
  type: ''
};

const app = handleActions<IAppState, any>({
  [NAVIGATE_TO]: (state: IAppState, action: Action<NAVIGATE_TO>): IAppState => {
    setPage(action.payload.pageCode);

    return Object.assign({}, state, {
      pageCode: action.payload.pageCode      
    }) as IAppState;
  },

  [SWITCH_SUB_PAGE]: (state: IAppState, action: Action<SWITCH_SUB_PAGE>): IAppState => {
    setSubPage(action.payload.subPageCode);

    return Object.assign({}, state, {
      subPageCode: action.payload.subPageCode,
      subPageData: action.payload.subPageData      
    }) as IAppState;
  },

  [SHOW_LOADABLE]: (state: IAppState, action: Action<SHOW_LOADABLE>): IAppState => {
    
    return Object.assign({}, state, {
      isLoading: action.payload.isLoading,
    }) as IAppState;
  }  
}, DEFAULT_STATE);

const alert = handleActions<IAlertState, any>({
  [DISPLAY_ALERT]: (state: IAlertState, action: Action<DISPLAY_ALERT>): IAlertState => {
    return Object.assign({}, state, { 
        visible: action.payload.alert.visible,
        header: action.payload.alert.header,
        content: action.payload.alert.content,
        type: action.payload.alert.type,      
    }) as IAlertState;
  }  
}, DEFAULT_ALERT_STATE);

export default combineReducers<IAppState>({
  app,
  alert
});