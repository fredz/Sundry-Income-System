export enum pages {
  general_login,
  general_main,
  general_dashboard,
  general_sundryjobs,
  general_maintenance,
} 

export enum subPages {
  maintenance_user,
  maintenance_customers,
  maintenance_franchisees,
  maintenance_subcontractors,  
  maintenance_addresscustomers,
  sundryjobs_job,
}

export type IAddressCustomerPageData = { 
  customerId: string,
  companyName: string 
};
