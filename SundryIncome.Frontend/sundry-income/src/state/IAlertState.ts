export interface IAlertState {
  visible: boolean;
  header: string;
  content: string;
  type: string;
}