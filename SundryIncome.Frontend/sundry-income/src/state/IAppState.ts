import { pages, IAddressCustomerPageData } from '../constants/pages';

export interface ISubPageData { 
  data: IAddressCustomerPageData;
}

export interface IAppState {
  /** The current page of project */
  pageCode: pages;

  /** Current subpage if present */
  subPageCode: pages | null;

  /** Page Data */
  pageData: any;

  /** SubPage Data */
  subPageData: ISubPageData;

  /** Loading state */
  isLoading: boolean;
}