// import * as fetchUtils from '../Utils/fetchUtils';

// // const url = window.location.toString();
// const url = 'http://qldgas.ascendis.com.au/intranet/'; //Uncomment when developing 

// export const fetch = (formData: FormData, method: string = 'POST') => {
//   return fetchUtils.fetchJson(url, method, formData);
// };

// export const fetchCityCodes = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'City/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchTariff = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'Tariff/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchComplex = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'Complex/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchComplexScheduled = (ComplexNo: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'ComplexScheduled/');
//   formData.append('ComplexNo', ComplexNo);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchBottledComplexes = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'BottledComplexes/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchMeteredComplexes = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'MeteredComplexes/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchService = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'Service/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchGLChart = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'GLChart/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchTaxType = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'TaxType/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchState = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'State/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchComplexUnit = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'ComplexUnit/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchCustomer = (Search) => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'Customer/');
//   formData.append('Search', Search);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchCustomerRental = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'CustomerRental/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchReading = (ComplexNo: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'Reading/');
//   formData.append('ComplesNo', ComplexNo);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchTaxInvoice = (Search: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'Rental/');
//   formData.append('Search', Search);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchReminderTerms = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'ReminderTerms/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchUnitForRental = () => {
//   let formData = new FormData();
//   formData.append('Action', 'List');
//   formData.append('Path', 'UnitRental/');
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchTaxInvoices = (Search: string, SearchType: string, FromDate: string, ToDate: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'Fetch');
//   formData.append('Path', 'Invoices/');
//   formData.append('Search', Search);  
//   formData.append('FromDate', FromDate);
//   formData.append('ToDate', ToDate);
//   formData.append('SearchType', SearchType);     
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const fetchProcessedInvoice = (InvoiceNo: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'Fetch');
//   formData.append('Path', 'ProcessedInvoiceView/');
//   formData.append('InvoiceNo', InvoiceNo);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export interface IVerifyData {
//   name: string;
//   value: any;
// }

// export const verify = (path: string, values: Array<IVerifyData>) => {
//   let formData = new FormData();
//   formData.append('Action', 'Verify');
//   formData.append('Path', path);

//   for (let i = 0; i < values.length; i++) {
//     const { name, value } = values[i];
//     formData.append(name, value);
//   }
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const nextID = (column: string, table: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'Get');
//   formData.append('Path', 'NextID/');
//   formData.append('Column', column);
//   formData.append('Table', table);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const canDelete = (id: string, method: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'Get');
//   formData.append('Path', 'CanDelete/');    
//   formData.append('ID', id);  
//   formData.append('Method', method);  
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const updateTempReading = (unitCode: string, readingDate: string, reading: string, final: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'Exec');
//   formData.append('Path', 'TempReading/');    
//   formData.append('UnitCode', unitCode);  
//   formData.append('ReadingDate', readingDate);
//   formData.append('Reading', reading);
//   formData.append('Final', final);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const getTempReadingFromDate = (dateFrom: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'Exec');
//   formData.append('Path', 'PreviewTempReadingsFromDate/');    
//   formData.append('DateFrom', dateFrom);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const getTempReadingForComplex = (complexNo: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'Exec');
//   formData.append('Path', 'PreviewTempReadingsForComplex/');    
//   formData.append('ComplexNo', complexNo);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const getTempReadingForCustomer = (customerNo: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'Exec');
//   formData.append('Path', 'PreviewTempReadingsForCustomer/');    
//   formData.append('CustomerNo', customerNo);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const processInvoices = (fromDate: string, complexNo: string, customerNo: string, message: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'Exec');
//   formData.append('Path', 'ProcessInvoices/');    
//   formData.append('CustomNo', customerNo);
//   formData.append('FromDate', fromDate);
//   formData.append('ComplexNo', complexNo);
//   formData.append('Message', message);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const sendInvoiceEmail = (invoiceNo: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'Invoices');
//   formData.append('Path', 'Email/');    
//   formData.append('InvoiceNo', invoiceNo);
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };

// export const sendLetterEmail = (InvoiceNo: string, CustNo: string, Type: string) => {
//   let formData = new FormData();
//   formData.append('Action', 'Invoices');
//   formData.append('Path', 'EmailDebt/');    
//   formData.append('CustNo', CustNo);
//   formData.append('InvoiceNo', InvoiceNo);
//   formData.append('Type', Type);    
  
//   return fetchUtils.fetchJson(url, 'POST', formData);
// };
