import * as React from 'react';
import { Segment } from 'semantic-ui-react';
import Main from '../components/Main';
import { Loadable } from '../components/Generic/Loadable';

export interface IAppProps { }

interface IConnectedProps {
  pageCode?: string;
}

interface IConnectedDispatch {
  onSetPageCode?: (value: string) => void;
}

class App extends React.Component<IAppProps & IConnectedProps & IConnectedDispatch, any> {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Main/>
    );
  }
}

export default App;
