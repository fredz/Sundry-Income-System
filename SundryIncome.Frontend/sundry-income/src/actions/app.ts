import { Action } from 'redux-actions';
import { IAppState, ISubPageData } from '../state/IAppState';
import { IAlertState } from '../state/IAlertState';
import { pages, subPages, IAddressCustomerPageData } from '../constants/pages';

export const NAVIGATE_TO = 'App/NAVIGATE_TO';
export type NAVIGATE_TO = {
  pageCode: pages;  
};
export function navigateTo(pageCode: pages, data: ISubPageData = null) {  
  return (dispatch, getState: () => IAppState) => {
    dispatch({
      type: NAVIGATE_TO,
      payload: {
        pageCode        
      }
    } as Action<NAVIGATE_TO>);
  };
}

export const SWITCH_SUB_PAGE = 'App/SWITCH_SUB_PAGE';
export type SWITCH_SUB_PAGE = {
  subPageCode: subPages;
  subPageData: ISubPageData;  
};
export function switchSubPage(subPageCode: subPages | null, subPageData: ISubPageData = null) {
  
  return (dispatch, getState: () => IAppState) => {
    dispatch({
      type: SWITCH_SUB_PAGE,      
      payload: {
        subPageCode,
        subPageData        
      }
    } as Action<SWITCH_SUB_PAGE>);
  };
}

export const DISPLAY_ALERT = 'alert/DISPLAY_ALERT';
export type DISPLAY_ALERT = {
  alert: IAlertState
};

export function displayAlert(alert: IAlertState) {
  return (dispatch, getState: () => IAlertState) => {
    dispatch({
      type: DISPLAY_ALERT,
      payload: {
        alert
      }
    } as Action<DISPLAY_ALERT>);
  };
}

export const SHOW_LOADABLE = 'loadable/SHOW_LOADABLE';
export type SHOW_LOADABLE = {
  isLoading: boolean
};

export function showLoadable(isLoading: boolean) {
  return (dispatch, getState: () => IAppState) => {
    dispatch({
      type: SHOW_LOADABLE,
      payload: {
        isLoading
      }
    } as Action<SHOW_LOADABLE>);
  };
}