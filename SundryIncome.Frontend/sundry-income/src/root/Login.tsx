import * as React from 'react';
import { connect } from 'react-redux';
import { navigateTo, showLoadable } from '../actions/app';
import { Grid, Segment, Input, Button, Header, Image, Form, Message } from 'semantic-ui-react';

import { AMCHeader } from '../components/Layout/AMCHeader';
import { pages } from '../constants/pages';
import { getUser, logUser, getPage, LogResult } from '../utils/sessionManager';

export interface ILoginProps {}

interface IConnectedProps {
  pageCode: pages;
}

interface IConnectedDispatch {
  handleNavigateTo: (value: pages) => void;
  showLoadable: (isLoading: boolean) => void;
}

interface ILoginState {
  disabled: boolean;
  username: string;
  password: string;
}

class Login extends React.Component<ILoginProps & IConnectedProps & IConnectedDispatch, ILoginState> {
  constructor(props) {
    super(props);

    this.state = {
      disabled: false,
      username: '',
      password: ''
    };

    this.handleLogin = this.handleLogin.bind(this);
  }

  componentDidMount() {
    const user = getUser();

    if (user) {
      this.props.handleNavigateTo(getPage() !== null ? getPage() : pages.general_main);
    }
  }

  handleLogin(event) {
    event.preventDefault();
    this.props.showLoadable(true);

    logUser(this.state.username, this.state.password)
    .then( (res) => {

      this.props.showLoadable(false);

      if (res === LogResult.invalid) {
        alert('User/Password invalid');

        this.setState({
          disabled: false
        });
        return;
      }

      if (res === LogResult.noAccess) {
        alert('Your account is disabled to access the system');

        this.setState({
          disabled: false
        });
        return;
      }

      if (res === LogResult.success) {
        this.props.handleNavigateTo(pages.general_main);
      }
    })
    .catch( (res) => {
      alert('Cannot connect to system, unknown error');
    });

    this.setState({
      disabled: true
    });
  }

  render() {
    return (
      <Grid.Column className="Reset">
        <AMCHeader onLogoClick={() => {}}/>
        
        <Grid centered={true} verticalAlign="middle">
          <Grid.Column textAlign="center" width={6}>
            <div style={{marginTop: '1em'}}>
              <Header as="h2" color="blue">
                Log-in to your account
              </Header>
              <Form size="large">
                <Segment stacked={true}>
                  <Form.Field>
                    <Input fluid={true} icon="user" iconPosition="left" placeholder="Username" 
                     onChange={(e, d) => this.setState({username: d.value})}/>
                  </Form.Field>
                  <Form.Field>
                    <Input fluid={true} icon="lock" iconPosition="left" placeholder="Password" type="password" 
                      onChange={(e, d) => this.setState({password: d.value})}/>
                  </Form.Field>
                  <Button disabled={this.state.disabled} fluid={true} 
                    color="blue" size="large" onClick={this.handleLogin}>LOGIN</Button>
                </Segment>
              </Form>
            </div>
          </Grid.Column>
        </Grid>

      </Grid.Column>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    handleNavigateTo: (value: pages) => dispatch(navigateTo(value)),
    showLoadable: (isLoading: boolean) => dispatch(showLoadable(isLoading))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Login) as React.ComponentClass<ILoginProps>;
