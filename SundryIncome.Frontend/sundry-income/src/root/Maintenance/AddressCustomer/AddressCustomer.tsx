import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react';

import { navigateTo, switchSubPage, showLoadable } from '../../../actions/app';

import { pages, subPages } from '../../../constants/pages';
import { ISubPageData } from '../../../state/IAppState';

/** Modes */
import { AddressCustomerForm } from './components/AddressCustomerForm';
import { AddressCustomerListing } from './components/AddressCustomerListing';
import * as fetchUtils from '../../../utils/fetchUtils';

export interface IAddressCustomerProps {  
  onEditData?: (list) => void;
  onCancel?: () => void;  
}

interface IConnectedProps {
  pageCode: pages;
  subPageData: ISubPageData;  
}

interface IConnectedDispatch {
  handleNavigateTo: (value: pages) => void;
  handleSwitchSubPage: (value: subPages) => void;
  showLoadable: (isLoading: boolean) => void;
}

const enum Modes {
  list,
  new,
  update
}

interface IAddressCustomerState {
  mode: Modes;
  editData: any;
  addressId: string;
}

class AddressCustomer extends React.Component<IAddressCustomerProps & 
IConnectedProps & IConnectedDispatch, IAddressCustomerState> {
  constructor(props) {
    super(props);

    this.state = {
      mode: Modes.list,
    } as IAddressCustomerState;

    this.getContent = this.getContent.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleSaveData = this.handleSaveData.bind(this);
    this.handleUpdateData = this.handleUpdateData.bind(this);
    this.handleShowLoadable = this.handleShowLoadable.bind(this);
  }

  handleEditData(list) {
    
    this.setState({
      addressId: list.AddressId,
      editData: list,
      mode: Modes.update,
    } as IAddressCustomerState);
  }

  handleSaveData(data) {
    this.handleShowLoadable(true);

    const address: fetchUtils.IAddress = {
      CustomerId: this.props.subPageData.data.customerId,
      Type: data.type,
      Addresss: data.address,
      Locality: data.locality,
      State: data.state,
      PostalCode: data.postalCode
    };

    fetchUtils.AddAddress(address).then((res) => {
      this.setState({
        mode: Modes.list
      });
    }).catch( (error) => alert('An Error Occured'));
  }    

  handleUpdateData(data, addressId) {
    this.handleShowLoadable(true);

    const address: fetchUtils.IUpdateAddress = {
      CustomerId: this.props.subPageData.data.customerId,
      Type: data.type,
      Addresss: data.address,
      Locality: data.locality,
      State: data.state,
      PostalCode: data.postalCode,
      AddressId: addressId,
      Uid: null
    };

    fetchUtils.UpdateAddress(address).then((res) => {
      this.setState({
        mode: Modes.list
      });
    }).catch( (error) => alert('An error occured while updating'));
  }  

  handleShowLoadable(isLoading: boolean) {
    this.props.showLoadable(isLoading);
  }     

  getContent() {
    
    switch (this.state.mode) {
      default:
      case Modes.list: 
        return (          
          <AddressCustomerListing 
            onEditData={this.handleEditData} 
            customerId={this.props.subPageData.data.customerId} 
            onCancel={() => {this.props.handleSwitchSubPage(subPages.maintenance_customers); }} 
            companyName={this.props.subPageData.data.companyName}
            showLoading={this.handleShowLoadable}  
            />
        );

      case Modes.new:
        return (
          <AddressCustomerForm 
            isEdit={false} 
            onCancel={() => this.setState({mode: Modes.list})}  
            onSave={this.handleSaveData} 
            customerId={this.props.subPageData.data.customerId}
            onUpdate={null}/>
        );
      
      case Modes.update:
        return (
          <AddressCustomerForm 
            data={this.state.editData} 
            isEdit={true} 
            onCancel={() => this.setState({mode: Modes.list})}  
            onSave={null}
            onUpdate={this.handleUpdateData}
            addressId={this.state.addressId}
            customerId={this.props.subPageData.data.customerId} 
            />          
        );
        
    }
  }

  render() {
    const content = this.getContent();

    return (
      <div> 
        <Menu attached="top">
          <Menu.Item onClick={() => this.setState({mode: Modes.list})} active={this.state.mode === Modes.list}
            icon="list layout" color="blue"/>

          <Dropdown item={true} icon="wrench" simple={true} >
            <Dropdown.Menu>
              <Dropdown.Header>General</Dropdown.Header>
              <Dropdown.Item onClick={() => this.setState({mode: Modes.new})}>Create New</Dropdown.Item>
              <Dropdown.Divider />
            </Dropdown.Menu>
          </Dropdown>

          {/*<Menu.Menu position="right">
            <div className="ui right aligned category search item">
              <div className="ui transparent icon input">
                <input className="prompt" type="text" placeholder="Search Users..." />
                <i className="search link icon" />
              </div>
              <div className="results"/>
            </div>
          </Menu.Menu>*/}
        </Menu>

        <Segment attached="bottom">
          {content}
        </Segment>

      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
    subPageData: state.app.subPageData
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    handleNavigateTo: (value: pages) => dispatch(navigateTo(value)),
    handleSwitchSubPage: (value: subPages) => dispatch(switchSubPage(value)),
    showLoadable: (isLoading: boolean) => dispatch(showLoadable(isLoading))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(AddressCustomer) as React.ComponentClass<IAddressCustomerProps>;
