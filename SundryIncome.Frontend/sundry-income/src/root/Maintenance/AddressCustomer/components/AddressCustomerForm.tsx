import * as React from 'react';
import { Button, Form, Grid, Input, Label, Select } from 'semantic-ui-react';
import * as fetchUtils from '../../../../utils/fetchUtils';

export interface IAddressCustomerProps {
  onCancel: () => void;
  onSave: (data) => void;
  onUpdate: (data, addressid) => void;
  data?: any;
  isEdit: boolean;
  addressId?: string;
  customerId: string;
}

export interface IAddressCustomerFormState {
  addressId: string;
  address: string;
  locality: string;
  state: string;
  postalCode: string;
  type: string;
  types: any;
  error?: string[];
}

export class AddressCustomerForm extends React.Component<IAddressCustomerProps, IAddressCustomerFormState> {
  constructor(props) {
    super(props);

    this.state = {
      addressId: '',
      address: '',
      locality: '',
      state: '',
      postalCode: '',
      error: [],
      type: 'Physical',
      types: []
    };

    if (this.props.isEdit) {
      const { data } = this.props;

      this.state = {
        addressId: data.AddressId,
        address: data.Addresss,
        locality: data.Locality,
        state: data.State,
        postalCode: data.PostalCode,
        type: data.Type,
        error: [],
        types: [] 
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.validateAddressType = this.validateAddressType.bind(this);
  }

  componentWillMount() {
    this.setState({
      types: [
        {key: 0, text: 'Physical', value: 'Physical'},
        {key: 1, text: 'Postal', value: 'Postal'}        
      ]
    });
  }

  handleInputChange(prop, value) {

    const state = {};
    state[prop] = value;

    this.setState(state as IAddressCustomerFormState);
  }
  
  required() {
    const error = [];

    if (!this.state.type || this.state.type.toString().trim().length === 0) {
      error.push('type');
    }

    if (!this.state.address || this.state.address.toString().trim().length === 0) {
      error.push('address');
    }

    if (!this.state.locality || this.state.locality.toString().trim().length === 0) {
      error.push('locality');
    }

    if (!this.state.state || this.state.state.toString().trim().length === 0) {
      error.push('state');
    }

    if (!this.state.postalCode || this.state.postalCode.toString().trim().length === 0) {
      error.push('postalCode');
    }                       

    return error;
  }  

  verifydata() {

    const required = this.required.bind(this)();

    if (required.length > 0) {
      this.setState({
        error: required
      });
      return;
    }    

    if (this.props.isEdit) {
      this.validateAddressType( this.state.type, () => {
        this.props.onUpdate(this.state, this.props.addressId);
      });
    } else {      
      this.validateAddressType(this.state.type, () => {
        this.props.onSave(this.state);
      });      
    }
  }

  validateAddressType(type: string, callback) {
    const { GetAddressByCustomerId } = fetchUtils;    
    let addressObj: any;

    GetAddressByCustomerId(this.props.customerId)
      .then((res) => {

        addressObj = res.find((element) => {
          return element.Type === type;          
        });        

        if (addressObj === undefined) {
          if (typeof callback === 'function') {
            callback();
          }                    
        }else {          
          fetchUtils.showAlert('error', 'The "Type" field only allows two values as maximum "Physical" and "Postal"');
        }
        
      }).catch( (err) => {                
        fetchUtils.showAlert('error', 'An Unknow error ocurred while querying  Address to validate Type');
    });    
  }  

  render() {
    
    return (
      <div>
        <Grid centered={true} verticalAlign="middle">
          <Grid.Column textAlign="left" width={6}>
            <Form size="large">

              <Form.Field>
                <label >Type</label>
                <Select fluid={true} search={true}
                  options={this.state.types} 
                  onChange={(e, d) => this.setState({type: d.value.toString()})}
                  defaultValue={this.state.type === '' ? this.state.types[0].value : this.state.type}
                  error={this.state.error.findIndex( (x) => x === 'type') > -1}
                />
              </Form.Field>               

              <Form.Field >
                <label >Address</label>
                <Input
                  id="address"
                  placeholder="Address"
                  label={{ icon: 'asterisk' }}
                  labelPosition="right corner"
                  value={this.state.address}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'address') > -1}
                />
              </Form.Field>

              <Form.Field>
                <label >Locality</label>
                <Input
                  id="locality"
                  placeholder="Locality"
                  value={this.state.locality}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'locality') > -1}
                />
              </Form.Field>

              <Form.Field>
                <label >State</label>
                <Input
                  id="state"
                  placeholder="State"
                  value={this.state.state}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'state') > -1}
                />
              </Form.Field>

              <Form.Field>
                <label >Postal Code</label>
                <Input
                  id="postalCode"
                  placeholder="Postal Code"
                  value={this.state.postalCode}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'postalCode') > -1}
                />
              </Form.Field>            

              <Button.Group floated="right">
                <Button onClick={(e: any) => { this.props.onCancel(); }} >
                  Cancel
                </Button>
                <Button.Or />
                <Button type="submit" onClick={(e) => { e.preventDefault(); this.verifydata(); }} positive={true} >
                  {this.props.isEdit ? 'Update' : 'Save'}
                </Button>
              </Button.Group>
            </Form>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}
