import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment, Container, Button } from 'semantic-ui-react';
import { Listing } from '../../../../components/Generic/Listing';
import * as fetchUtils from '../../../../utils/fetchUtils';

interface IAddressCustomerListingProps {
  customerId: string;
  onEditData: (list) => void;
  onCancel: () => void;
  companyName: string;
  showLoading: (isLoading: boolean) => void;
}
interface IAddressCustomerListingState {
  data: any;
  columns: any;
}

export class AddressCustomerListing extends React.Component<IAddressCustomerListingProps , 
IAddressCustomerListingState> {

  constructor(props) {
    super(props);

    this.state = {
      data: []
    } as IAddressCustomerListingState;

    this.handleEditData = this.handleEditData.bind(this);    
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.prepareAccionButton = this.prepareAccionButton.bind(this);
  }

  componentDidMount() {
    this.refreshList();
  }  

  handleEditData(list) {    
    this.props.onEditData(list);
  }

  handleDeleteData(list) {

    const { AddressId } = list;
    this.props.showLoading(true);
    
    fetchUtils.DeleteAddress({
        AddressId
    }).then( (res) => {        
        fetchUtils.showAlert('success', 'Address ' + AddressId + ' was deleted');
        this.setState({
            data: []
        });
        this.refreshList();
    }).catch( (error) => {      
        alert('Un Unknow Error Occurred when deleting...');
    });
  }   

  prepareAccionButton(row) {
    return (        
      <span>
        <a href="#" title="Edit" onClick={(e) => {e.preventDefault(); this.handleEditData(row); }} >
          <Icon color="blue" name="edit" size="large"  />
        </a>

        <a href="#" title="Delete" onClick={(e) => {e.preventDefault(); this.handleDeleteData(row); }}>
          <Icon color="red" name="trash outline" size="large"  />
        </a>
      </span>      
    );
  } 

  refreshList() {
    const { GetAddressByCustomerId } = fetchUtils;
    this.props.showLoading(true);

    GetAddressByCustomerId(this.props.customerId)
      .then((res) => {
        this.setState({
          data: res
        });
        this.props.showLoading(false);
      }).catch( () => {
        this.setState({
          data: []
        });        
        fetchUtils.showAlert('error', 'An Unknow error ocurred while querying user Address Customer');
    });
  }    

  render() {

    const columns = [
      {dataField: 'Type', header: 'Type'},
      {dataField: 'Addresss', header: 'Address'},
      {dataField: 'Locality', header: 'Locality'},
      {dataField: 'State', header: 'State'},      
      {dataField: 'PostalCode', header: 'Postal Code'},      
      {dataField: '', header: 'Action', dataFormat: (value, row) => {
        return this.prepareAccionButton(row);
      }, textAlign: 'center'},      
    ];    

    const data = [
      {AddressID: '1', Address: 'Ballivian street 2', Locality: 'Texas', State: 'Mexico', PostalCode: '1245'},
      {AddressID: '2', Address: 'america 441', Locality: 'Arizona', State: 'New York', PostalCode: '254'},
    ];

    return (
      <div>
      <Segment raised={true}>
        Company Name: <b>{this.props.companyName}</b>
      </Segment>                
        <Listing data={this.state.data} columns={columns} />
        <Container textAlign="right" fluid={true}>
          <Button color="red" onClick={this.props.onCancel} >Go Back</Button>
        </Container>      
      </div>
    );
  }

}
