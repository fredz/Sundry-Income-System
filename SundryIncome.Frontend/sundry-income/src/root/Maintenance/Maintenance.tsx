import * as React from 'react';
import { connect } from 'react-redux';
import { switchSubPage, showLoadable } from '../../actions/app';
import { Grid, Segment, Input, Button, Header, Image, Form } from 'semantic-ui-react';

import { Topmenu } from '../../components/Layout/Topmenu';
import { subPages } from '../../constants/pages';

/** SubPages */
import User from './User/User';
import Customer from './Customer/Customer';
import Franchisee from './Franchisee/Franchisee';
import Subcontractor from './Subcontractor/Subcontractor';
import AddressCustomer from './AddressCustomer/AddressCustomer';

export interface IMaintenanceProps {}

interface IConnectedProps {
  subPageCode: subPages;  
}

interface IConnectedDispatch {
  handleSwitchSubPage: (value: subPages) => void;  
}

class Maintenance extends React.Component<IMaintenanceProps & IConnectedProps & IConnectedDispatch, {}> {
  topMenuList = [
    {name: 'Users'},
    {name: 'Customers'},
    {name: 'Franchisees'},
    {name: 'Subcontractors'}
  ];

  constructor(props) {
    super(props);

    this.handleItemClick = this.handleItemClick.bind(this);
    this.getContent = this.getContent.bind(this);
  }

  componentDidMount() {
    
  }

  handleItemClick(name: string) {
    const menu = this.topMenuList.find( (x) => x.name === name);

    let code: subPages;
    switch (name) {
      default:
      case this.topMenuList[0].name: 
        code = subPages.maintenance_user;
        break;
      
      case this.topMenuList[1].name:
        code = subPages.maintenance_customers;
        break;

      case this.topMenuList[2].name:
        code = subPages.maintenance_franchisees;
        break;

      case this.topMenuList[3].name:
        code = subPages.maintenance_subcontractors;
        break;
    }
    
    this.props.handleSwitchSubPage(code);    
  }

  getContent() {
    switch (this.props.subPageCode) {
      default:
      case subPages.maintenance_user: 
        return <User/>;
      
      case subPages.maintenance_customers:
        return <Customer/>;

      case subPages.maintenance_franchisees:
        return <Franchisee/>;

      case subPages.maintenance_subcontractors:
        return <Subcontractor/>;

      case subPages.maintenance_addresscustomers:
        return <AddressCustomer/>;        
    }
  }

  render() {
    const content = this.getContent();    

    return (
      <Grid.Column>
        <Topmenu menu={this.topMenuList} onItemClick={this.handleItemClick}/>    
        {content}
      </Grid.Column>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  
  return {
    subPageCode: state.app.subPageCode,        
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    handleSwitchSubPage: (value: subPages) => dispatch(switchSubPage(value))    
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Maintenance) as React.ComponentClass<IMaintenanceProps>;
