import * as React from 'react';
import { Button, Form, Grid, Input, Label } from 'semantic-ui-react';

export interface IFranchiseeFormProps {
  onCancel: () => void;
  onSave: (data) => void;
  onUpdate: (data, userid) => void;
  data?: any;
  isEdit: boolean;
  franchiseeId?: string;
}

export interface IFranchiseeFormState {
  franchiseeId: string;
  code: string;
  firstName: string;
  lastName: string;
  contactName: string;
  companyName: string;  
  error?: string[];
}

export class FranchiseeForm extends React.Component<IFranchiseeFormProps, IFranchiseeFormState> {
  constructor(props) {
    super(props);

    this.state = {
      franchiseeId: '',
      code: '',
      firstName: '',
      lastName: '',
      contactName: '',
      companyName: '',
      error: [],
    };

    if (this.props.isEdit) {
      const { data } = this.props;

      this.state = {
        franchiseeId: data.FranchiseeId,
        code: data.Code,
        firstName: data.FirstName,
        lastName: data.LastName,
        contactName: data.ContactName,
        companyName: data.CompanyName,
        error: [],
      } as IFranchiseeFormState;
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);

  }

  handleInputChange(prop, value) {

    const state = {};
    state[prop] = value;

    this.setState(state as IFranchiseeFormState);
  }

  required() {
    const error = [];

    if (!this.state.code || this.state.code.toString().trim().length === 0) {
      error.push('code');
    }

    if (!this.state.firstName || this.state.firstName.trim().length === 0) {
      error.push('firstName');
    }

    if (!this.state.lastName || this.state.lastName.trim().length === 0) {
      error.push('lastName');
    }

    return error;
  }    

  verifydata() {

    const required = this.required.bind(this)();

    if (required.length > 0) {
      this.setState({
        error: required
      });
      return;
    }

    if (this.props.isEdit) {
      this.props.onUpdate(this.state, this.props.franchiseeId);
    } else {
      this.props.onSave(this.state);
    }
  }

  render() {
    return (
      <div>
        <Grid centered={true} verticalAlign="middle">
          <Grid.Column textAlign="left" width={6}>
            <Form size="large">

              <Form.Field>
                <label >Code</label>
                <Input
                  id="code"
                  placeholder="Code"
                  label={{ icon: 'asterisk' }}
                  labelPosition="right corner"
                  value={this.state.code}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'code') > -1}
                />
              </Form.Field>

              <Form.Field>
                <label>Franchisee Name</label>
                <Input
                  id="contactName"
                  placeholder="Franchisee Name"
                  value={this.state.contactName}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                />
              </Form.Field>
              <Form.Field>
                <label >Contact Name</label>
              </Form.Field>
              <Form.Group inline={true}>
                <Input
                  id="firstName"
                  placeholder="First Name"
                  label={{ icon: 'asterisk' }}
                  labelPosition="right corner"
                  value={this.state.firstName}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'firstName') > -1}
                />

                <label/>
                <Input
                  id="lastName"
                  placeholder="Last Name"
                  label={{ icon: 'asterisk' }}
                  labelPosition="right corner"
                  value={this.state.lastName}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'lastName') > -1}
                />
              </Form.Group>

              <Form.Field>
                <label>Company Name</label>
                <Input
                  id="companyName"
                  placeholder="Company Name"
                  value={this.state.companyName}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                />
              </Form.Field>

              <Button.Group floated="right">
                <Button onClick={(e) => { this.props.onCancel(); }} >
                  Cancel
                </Button>
                <Button.Or />
                <Button type="submit" onClick={(e) => { e.preventDefault(); this.verifydata(); }} positive={true} >
                  {this.props.isEdit ? 'Update' : 'Save'}
                </Button>
              </Button.Group>
            </Form>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}
