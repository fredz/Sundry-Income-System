import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react';
import { Listing } from '../../../../components/Generic/Listing';
import * as fetchUtils from '../../../../utils/fetchUtils';

interface IFranchiseeListingProps {
  onEditData: (list) => void;
  showLoading: (isLoading: boolean) => void;
}

interface IFranchiseeListingState {
  data: any;
  columns: any;
}

export class FranchiseeListing extends React.Component<IFranchiseeListingProps , IFranchiseeListingState> {

  constructor(props) {
    super(props);

    this.state = {
      data: []
    } as IFranchiseeListingState;

    this.handleEditData = this.handleEditData.bind(this);
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.prepareActionButtons = this.prepareActionButtons.bind(this);
  }

  componentDidMount() {
    this.props.showLoading(true);
    fetchUtils.GetAllFranchisees().then( (franchisees) => {
      this.setState({
        data: franchisees
      });
      this.props.showLoading(false);
    }).catch((error) => alert('An error occurred when listing franchisees'));
  }  

  handleEditData(list) {    
    this.props.onEditData(list);
  }

  handleDeleteData(list) {}

  prepareActionButtons(row) {
    return (        
      <span>
        <a href="#" title="Edit" onClick={(e) => {e.preventDefault(); this.handleEditData(row); }} >
          <Icon color="blue" name="edit" size="large"  />
        </a>

        <a href="#" title="Delete" onClick={(e) => {e.preventDefault(); this.handleDeleteData(row); }}>
          <Icon color="red" name="trash outline" size="large"  />
        </a>
      </span>      
    );
  }      

  render() {

    const columns = [
      {dataField: 'Code', header: 'Code'},
      {dataField: 'ContactName', header: 'Franchisee Name'},
      {dataField: 'FirstName', header: 'Contact Name', dataFormat: (value, row) => {
        return <span>{row.FirstName} {row.LastName}</span>;
      }},
      {dataField: 'CompanyName', header: 'Company Name'},
      {dataField: '', header: 'Action', dataFormat: (value, row) => {
        return this.prepareActionButtons(row);
      }, textAlign: 'center'},       
    ];    

    return (
      <Listing data={this.state.data} columns={columns} />
    );
  }

}
