import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react';

import { navigateTo, showLoadable } from '../../../actions/app';

import { pages } from '../../../constants/pages';

/** Modes */
import { FranchiseeForm } from './components/FranchiseeForm';
import { FranchiseeListing } from './components/FranchiseeListing';
import * as fetchUtils from '../../../utils/fetchUtils';

export interface IFranchiseeProps {}

interface IConnectedProps {
  pageCode: pages;
}

interface IConnectedDispatch {
  handleNavigateTo: (value: pages) => void;
  showLoadable: (isLoading: boolean) => void;
}

const enum Modes {
  list,
  new,
  update
}

interface IFranchiseeState {
  mode: Modes;
  editData: any; 
  franchiseeId: string; 
}

class Franchisee extends React.Component<IFranchiseeProps & IConnectedProps & IConnectedDispatch, IFranchiseeState> {
  constructor(props) {
    super(props);

    this.state = {
      mode: Modes.list,
    } as IFranchiseeState;

    this.getContent = this.getContent.bind(this);    
    this.handleSaveData = this.handleSaveData.bind(this);
    this.handleUpdateData = this.handleUpdateData.bind(this);  
    this.handleEditData = this.handleEditData.bind(this);
    this.handleShowLoadable = this.handleShowLoadable.bind(this);  
  }

  handleSaveData(data) {
    this.handleShowLoadable(true);

    const franchisee: fetchUtils.IFranchisee = {      
      Code: data.code,
      FirstName: data.firstName,
      LastName: data.lastName,
      ContactName: data.contactName,  
      CompanyName: data.companyName,       
    };

    fetchUtils.AddFranchisee(franchisee).then((res) => {
      this.setState({
        mode: Modes.list
      });
    }).catch( (error) => alert('An Error Occured'));
  }

  handleUpdateData(data, franchiseeId) {
    this.handleShowLoadable(true);

    const franchisee: fetchUtils.IUpdateFranchisee = {
      Code: data.code,
      FirstName: data.firstName,
      LastName: data.lastName,
      ContactName: data.contactName,  
      CompanyName: data.companyName, 
      FranchiseeId: franchiseeId,
      Uid: null
    };

    fetchUtils.UpdateFranchisee(franchisee).then((res) => {
      this.setState({
        mode: Modes.list
      });
    }).catch( (error) => alert('An error occured while updating'));
  } 

  handleEditData(list) {
    this.setState({
      editData: list,
      mode: Modes.update,
      franchiseeId: list.FranchiseeId,
    } as IFranchiseeState);
  }    

  handleShowLoadable(isLoading: boolean) {
    this.props.showLoadable(isLoading);
  }   

  getContent() {
    switch (this.state.mode) {
      default:
      case Modes.list: 
        return (
          <FranchiseeListing 
            onEditData={this.handleEditData} 
            showLoading={this.handleShowLoadable}
          />          
        );

      case Modes.new:
        return (
          <FranchiseeForm 
            isEdit={false} 
            onCancel={() => this.setState({mode: Modes.list})} 
            onSave={this.handleSaveData}
            onUpdate={null}/>
        );
      
      case Modes.update:
        return (
          <FranchiseeForm 
            data={this.state.editData} 
            isEdit={true} 
            onCancel={() => this.setState({mode: Modes.list})} 
            onSave={null} 
            onUpdate={this.handleUpdateData}
            franchiseeId = {this.state.franchiseeId}
            />          
        );
    }
  }

  render() {
    const content = this.getContent();

    return (
      <div>
        <Menu attached="top">
          <Menu.Item onClick={() => this.setState({mode: Modes.list})} active={this.state.mode === Modes.list}
            icon="list layout" color="blue"/>

          <Dropdown item={true} icon="wrench" simple={true} >
            <Dropdown.Menu>
              <Dropdown.Header>General</Dropdown.Header>
              <Dropdown.Item onClick={() => this.setState({mode: Modes.new})}>Create New</Dropdown.Item>
              <Dropdown.Divider />
            </Dropdown.Menu>
          </Dropdown>

          {/*<Menu.Menu position="right">
            <div className="ui right aligned category search item">
              <div className="ui transparent icon input">
                <input className="prompt" type="text" placeholder="Search Users..." />
                <i className="search link icon" />
              </div>
              <div className="results"/>
            </div>
          </Menu.Menu>*/}
        </Menu>

        <Segment attached="bottom">
          {content}
        </Segment>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    handleNavigateTo: (value: pages) => dispatch(navigateTo(value)),
    showLoadable: (isLoading: boolean) => dispatch(showLoadable(isLoading))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Franchisee) as React.ComponentClass<IFranchiseeProps>;
