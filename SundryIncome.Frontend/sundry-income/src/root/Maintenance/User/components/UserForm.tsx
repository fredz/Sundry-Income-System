import * as React from 'react';
import { Button, Form, Grid, Input, Label } from 'semantic-ui-react';
import * as fetchUtils from '../../../../utils/fetchUtils';
import * as emailUtils from '../../../../utils/emailUtils';

export interface IUserFormProps {
  onCancel: () => void;
  onSave: (data) => void;
  onUpdate: (data, userid) => void;
  data?: any;
  isEdit: boolean;
  UserID: number;
}

export interface IUserFormState {
  userID: string;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  mobileNo: string;

  error?: string[];
}

export class UserForm extends React.Component<IUserFormProps, IUserFormState> {
  constructor(props) {
    super(props);

    this.state = {
      userID: '',
      username: '',
      password: '',
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      mobileNo: '',
      error: [],
    };

    if (this.props.isEdit) {
      
      const { data } = this.props;

      this.state = {
        userID: data.UserID,
        username: data.Username,
        password: data.Password,
        firstName: data.FirstName,
        lastName: data.LastName,
        email: data.Email,
        phone: data.Phone,
        mobileNo: data.MobileNo,
        error: [],
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
  }

  handleInputChange(prop, value) {

    const state = {};
    state[prop] = value;

    this.setState(state as IUserFormState);
  }

  required() {
    const error = [];

    if (!this.state.username || this.state.username.trim().length === 0) {
      error.push('username');
    }

    if (!this.props.isEdit && (!this.state.password || this.state.password.trim().length === 0)) {
      error.push('password');
    } else if (!this.props.isEdit && (!this.state.password || this.state.password.trim().length < 8)) {
      error.push('password');
    }

    if (!this.state.firstName || this.state.firstName.trim().length === 0) {
      error.push('firstName');
    }

    if (!this.state.lastName || this.state.lastName.trim().length === 0) {
      error.push('lastName');
    }

    if (!this.state.email || this.state.email.trim().length === 0) {
      error.push('email');
    }

    return error;
  }

  verifydata() {
    const required = this.required.bind(this)();

    if (required.length > 0) {
      this.setState({
        error: required
      });
      return;
    }
    
    fetchUtils.VerifyIfUsernameExists({
      UserName: this.state.username
    }).then((res) => {
      res = res === 'true' ? true : false;
      if (res) {
        if (this.props.isEdit && this.props.data.Username !== this.state.username) {
          alert('Username already exists');
          return;
        } else if (!this.props.isEdit) {
          alert('Username already exists');
          return;
        }
      }
      this.notify();
    }).catch(() => {
      alert('Could not verify is username exists');
    });
  }

  notify() {
    if (this.props.isEdit) {
        this.props.onUpdate(this.state, this.props.UserID);
      } else {
        this.props.onSave(this.state);
      }
  }

  render() {
    return (
      <div>
        <Grid centered={true} verticalAlign="middle">
          <Grid.Column textAlign="left" width={6}>
            <Form size="large">
              <Form.Field >
                <label >Username</label>
                <Input
                  id="username"
                  placeholder="Username"
                  label={{ icon: 'asterisk' }}
                  labelPosition="right corner"
                  value={this.state.username}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'username') > -1}
                />
              </Form.Field>

              { !this.props.isEdit ?
              <Form.Field>
                <label >Password</label>
                <Input
                  id="password"
                  type="password"
                  placeholder="Password"
                  value={this.state.password}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'password') > -1}
                  action={{content: 'Generate', icon: 'privacy', color: 'green', 
                  onClick: (e) => {
                      e.preventDefault();
                      this.setState({
                        password: Math.random().toString(36).slice(-8)
                      });
                    } 
                  }}
                />
              </Form.Field>
              : null
              }
              <Form.Field>
                <label >First Name</label>
                <Input
                  id="firstName"
                  placeholder="First Name"
                  label={{ icon: 'asterisk' }}
                  labelPosition="right corner"
                  value={this.state.firstName}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'firstName') > -1}
                />
              </Form.Field>

              <Form.Field>
                <label >Last Name</label>
                <Input
                  id="lastName"
                  placeholder="Last Name"
                  label={{ icon: 'asterisk' }}
                  labelPosition="right corner"
                  value={this.state.lastName}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'lastName') > -1}
                />
              </Form.Field>

              <Form.Field>
                <label >Email</label>
                <Input
                  id="email"
                  type="email"
                  placeholder="Email"
                  label={{ icon: 'asterisk' }}
                  labelPosition="right corner"
                  value={this.state.email}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                  error={this.state.error.findIndex( (x) => x === 'email') > -1}
                />
              </Form.Field>

              <Form.Field>
                <label >Phone</label>
                <Input
                  id="phone"
                  placeholder="Phone"
                  value={this.state.phone}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                />
              </Form.Field>

              <Form.Field>
                <label >Mobile No</label>
                <Input
                  id="mobileNo"
                  placeholder="Mobile No"
                  value={this.state.mobileNo}
                  onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                />
              </Form.Field>

              <Button.Group floated="right">
                <Button onClick={(e) => { this.props.onCancel(); }} >
                  Cancel
                </Button>
                <Button.Or />
                <Button type="submit" onClick={(e) => { e.preventDefault(); this.verifydata(); }} positive={true} >
                  {this.props.isEdit ? 'Update' : 'Save'}
                </Button>
              </Button.Group>
            </Form>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}
