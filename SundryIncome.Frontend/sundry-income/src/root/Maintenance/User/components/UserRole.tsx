import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment, Input, Select, Button, Checkbox, Container } from 'semantic-ui-react';
import { Listing } from '../../../../components/Generic/Listing';
import * as fetchUtils from '../../../../utils/fetchUtils';

interface IUserRoleProps {
  userId: string;
  onEditData: (list) => void;
  onCancel: () => void;
  showLoading: (isLoading: boolean) => void;
}
interface IUserRoleState {
  data: any;
  headerColumns: any;
  roles: any[];
  selectedRole: any;
}

export class UserRole extends React.Component<IUserRoleProps , IUserRoleState> {

  constructor(props) {
    super(props);

    this.state = {
      roles: [],
      selectedRole: 1,
      data: null
    } as IUserRoleState;

    this.handleEditData = this.handleEditData.bind(this);    
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.prepareAccionButton = this.prepareAccionButton.bind(this);
    this.headerColumnArray = this.headerColumnArray.bind(this);
    this.handleEnableRole = this.handleEnableRole.bind(this);
    this.handleAddRole = this.handleAddRole.bind(this);
  }

  componentDidMount() {
    this.refreshList();
  }

  handleEditData(list) {    
    this.props.onEditData(list);
  }

  handleEnableRole(row, data) {
    fetchUtils.UpdateUserRole({
        Disabled: data.checked ? 1 : 0,
        UserId: this.props.userId,
        RoleId: row.RoleId
    }).then( (res) => {
        alert('User was updated');
        this.setState({
            data: null
        });
        this.refreshList();
    }).catch( () => {
        alert('An error occured while trying to update role');
    });
  }  

  handleDeleteData(list) {
    const {UserId, RoleId } = list;
    this.props.showLoading(true);
    
    fetchUtils.DeleteUserRole({
        UserId: this.props.userId,
        RoleId
    }).then( (res) => {
        alert('User Role was deleted');
        this.setState({
            data: null
        });
        this.refreshList();
    }).catch( () => {
        alert('Un Unknow Error Occurred when deleting...');
    });
  }    

  prepareAccionButton(row) {
    return (        
      <span>
        <a href="#" title="Delete" onClick={(e) => {e.preventDefault(); this.handleDeleteData(row); }}>
          <Icon color="red" name="trash outline" size="large"/>          
        </a>
      </span>      
    );
  } 

  headerColumnArray() {
    const column = [
      {dataField: 'RoleName', header: 'Role'},

      {dataField: 'Disabled', header: 'Disabled', dataFormat: (value, row) => {
        return (
          <Checkbox toggle={true} onChange={(e, d) => this.handleEnableRole(row, d)} 
            defaultChecked={row.Disabled === 1 ? true : false} />
        );
      }, options: {textAlign: 'center'}},      

      {dataField: '', header: 'Action', dataFormat: (value, row) => {
        return this.prepareAccionButton(row);
      }, options: {textAlign: 'center'}},      
    ]; 

    return column;  
  } 

  handleAddRole(e) {
    e.preventDefault();
    const role = this.state.data.find( (x) => x.RoleId === this.state.selectedRole);
    if (role) {
        alert('User has already that role');
        return;
    }

    this.props.showLoading(true);
    
    fetchUtils.AddUserRole({
        UserId: this.props.userId,
        RoleId: this.state.selectedRole,
        Disabled: 0
    }).then( (res) => {
        alert('User Role Was Added');
        this.setState({
            data: null,
        });
        this.refreshList();
    }).catch( () => {
        alert('An Unknown Error Occured while adding user');
    });
  }

  render() {
    if (this.state.data === null) {
      return null;
    }

    const headerColumns = this.headerColumnArray();

    return (
      <div>
        <Segment padded={true} textAlign="center" >
          <Input type="text" placeholder="Add Role..." action={true}>
            <div style={{width: '350px'}}>       
              <Select fluid={true} 
                options={this.state.roles} onChange={(e, d) => this.setState({selectedRole: d.value})}
                defaultValue={this.state.roles[0].value}/>
              <Button fluid={true} color="green" type="submit" onClick={this.handleAddRole}>Add</Button>
            </div>
          </Input>             
        </Segment> 
        <Listing data={this.state.data} columns={headerColumns} />
        <Container textAlign="right" fluid={true}>
          <Button color="red" onClick={this.props.onCancel} >Go Back</Button>
        </Container>
      </div>
    );
  }

  private refreshList() {
    const { GetAllRoles, GetUserRolesByUserId } = fetchUtils;
    this.props.showLoading(true);

    Promise.all([GetAllRoles(), GetUserRolesByUserId(this.props.userId)])
      .then((res) => {
        const roles = res[0];
        const userroles = res[1];
        this.setState({
            roles: (roles as any).map( (x, i) => {
                return {
                    key: i,
                    text: x.Name,
                    value: x.RoleId
                };
            }),
            data: (userroles as any).map( (x, i) => {
                return {
                    UserRoleId: x.UserRoleId,
                    RoleName: x.RoleName,
                    Disabled: x.Disabled,
                    RoleId: x.RoleId,
                    RoleRank: x.RoleRank,
                };
            }),
            selectedRole: roles[0].RoleId
        });

        this.props.showLoading(false);

      }).catch( () => {
        this.setState({
            roles: [],
        });
        alert('An Unknow error ocurred while querying user roles');
    });
  }
}
