import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react';
import { Listing } from '../../../../components/Generic/Listing';
import { ConfirmAlert } from '../../../../components/Generic/ConfirmAlert';

/** Fetch */
import * as fetchUtils from '../../../../utils/fetchUtils';
import * as emailUtils from '../../../../utils/emailUtils';

interface IUserListingProps {
  onEditData: (list) => void;
  onEditUserRole: (list) => void;
  showLoading: (isLoading: boolean) => void;
}
interface IUserListingState {
  data: any;
  columns: any;
  confirmResetPass: boolean;
}

export class UserListing extends React.Component<IUserListingProps , IUserListingState> {
  confirmAlertRef: any;
  constructor(props) {
    super(props);

    this.state = {
      data: null,
      columns: null,
      confirmResetPass: false
    };

    this.handleEditData = this.handleEditData.bind(this);    
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.handleEditUserRole = this.handleEditUserRole.bind(this);    
    this.prepareAccionButton = this.prepareAccionButton.bind(this);
    this.resetPassword = this.resetPassword.bind(this);
    this.prepareResetPassword = this.prepareResetPassword.bind(this);
  }

  componentDidMount() {
    this.props.showLoading(true);
    fetchUtils.GetAllUsers().then( (users) => {
      this.setState({
        data: users
      });
      this.props.showLoading(false);
    }).catch((error) => alert('An error occurred when listing users'));
  }

  handleEditData(list) {    
    this.props.onEditData(list);
  }
  
  handleEditUserRole(list) {    
    this.props.onEditUserRole(list);
  }  

  handleDeleteData(list) {}

  prepareResetPassword(row) {

    const title = 'Reset Password';
    const message = 'Are you sure you want to reset the user password?';
    const open = true;
    const yesAction = () => { this.resetPassword(row); };
    this.confirmAlertRef.handleConfirm(title, message, open, yesAction);
  }

  resetPassword(row) {
    const user: fetchUtils.IUpdateUser = {
      Username: row.Username,
      Password: Math.random().toString(36).slice(-8),
      FirstName: row.FirstName,
      LastName: row.LastName,
      Email: row.Email,
      Phone: row.Phone,
      MobileNo: row.MobileNo,
      UserId: row.UserId,
      Uid: null
    };

    fetchUtils.UpdateUser(user).then((res) => {      
      fetchUtils.showAlert('success', 'Password resetted, sending email...');
      emailUtils.sendNewPassword(user.UserId).then((email: any) => {
        if (email.error) {
          alert(email.message);
          return;
        }
      });
    }).catch( (error) => alert('An error occured while updating'));
  }    

  prepareAccionButton(row) {
    return (        
      <span>
        <a href="#" title="Reset Password" onClick={(e) => {e.preventDefault(); this.prepareResetPassword(row); }} >
          <Icon color="green" name="privacy" size="large"/>
        </a>

        <a href="#" title="Role" onClick={(e) => {e.preventDefault(); this.handleEditUserRole(row); }} >
          <Icon color="green" name="users" size="large"/>
        </a>

        <a href="#" title="Edit" onClick={(e) => {e.preventDefault(); this.handleEditData(row); }} >
          <Icon color="blue" name="edit" size="large"/>
        </a>

        {/*<a title="Delete" onClick={(e) => {e.preventDefault(); this.handleDeleteData(row); }}>
          <Icon color="red" name="trash outline" size="large"/>
        </a>*/}
      </span>      
    );
  }   

  render() {
    if (this.state.data === null) {
      return null;
    }

    const columns = [
      {dataField: 'Username', header: 'Username'},
      {dataField: 'FirstName', header: 'First Name'},
      {dataField: 'LastName', header: 'Last Name'},
      {dataField: 'Email', header: 'Email'},      
      {dataField: '', header: 'Action', dataFormat: (value, row) => {
        return this.prepareAccionButton(row);
      }, options: {textAlign: 'center'}},      
    ];    

    return (
      <div>        
        <ConfirmAlert ref={(x) => this.confirmAlertRef = x} />
        <Listing data={this.state.data} columns={columns} />
      </div>
    );
  }

}
