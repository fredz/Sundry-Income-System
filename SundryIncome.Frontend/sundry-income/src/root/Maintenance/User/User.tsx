import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react';

import { navigateTo, showLoadable } from '../../../actions/app';

import { pages } from '../../../constants/pages';
import { IAlertState } from '../../../state';

/** Modes */
import { UserForm } from './components/UserForm';
import { UserListing } from './components/UserListing';
import { UserRole } from './components/UserRole';

/** Fetch */
import * as fetchUtils from '../../../utils/fetchUtils';
import * as emailUtils from '../../../utils/emailUtils';

export interface IUserProps {}

interface IConnectedProps {
  pageCode: pages;
}

interface IConnectedDispatch {
  handleNavigateTo: (value: pages) => void;  
  showLoadable: (isLoading: boolean) => void;
}

const enum Modes {
  list,
  new,
  update,
  user_role
}

interface IUserState {
  mode: Modes;
  editData: any;
  userId: string;
}

class User extends React.Component<IUserProps & IConnectedProps & IConnectedDispatch, IUserState> {
  constructor(props) {
    super(props);

    this.state = {
      mode: Modes.list,
    } as IUserState;

    this.getContent = this.getContent.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleEditUserRole = this.handleEditUserRole.bind(this);    
    this.handleSaveUser = this.handleSaveUser.bind(this);
    this.handleUpdateData = this.handleUpdateData.bind(this);
    this.handleShowLoadable = this.handleShowLoadable.bind(this);
  }

  handleSaveUser(data) {
    this.handleShowLoadable(true);

    const user: fetchUtils.IUser = {
      Username: data.username,
      Password: data.password,
      FirstName: data.firstName,
      LastName: data.lastName,
      Email: data.email,
      Phone: data.phone,
      MobileNo: data.mobileNo,
    };

    fetchUtils.AddUser(user).then((res) => {      
      fetchUtils.showAlert('success', 'Sending new account email');
      emailUtils.sendNewAccount(res.UserId).then((newResult) => {
        this.setState({
          mode: Modes.list
        });
        fetchUtils.AddUserRole({
            UserId: res.UserId,
            RoleId: 1,
            Disabled: 0
        }).then( () => { }).catch( () => {
            alert('An error occured while assigning a role to the new user, please review it manually');
        });
      }).catch( (error) => alert('An error occured while sending email'));
    }).catch( (error) => alert('An Error Occured'));
  }

  handleShowLoadable(isLoading: boolean) {
    this.props.showLoadable(isLoading);
  } 

  getContent() {
    
    switch (this.state.mode) {
      default:
      case Modes.list:        
        return (
          <UserListing 
            onEditData={this.handleEditData} 
            onEditUserRole={this.handleEditUserRole}
            showLoading={this.handleShowLoadable} 
          /> 
        );

      case Modes.new:
        return (
          <UserForm 
            isEdit={false} 
            onCancel={() => this.setState({mode: Modes.list})} 
            onSave={this.handleSaveUser}
            onUpdate={null}
            UserID={null}/>
        );
      
      case Modes.update:
        return (
          <UserForm 
            data={this.state.editData} 
            isEdit={true} 
            onCancel={() => this.setState({mode: Modes.list})} 
            onSave={null} 
            onUpdate={this.handleUpdateData}
            UserID={this.state.editData.UserId}/>
        );

      case Modes.user_role:
        return (
          <UserRole 
            userId={this.state.userId} 
            onCancel={() => this.setState({mode: Modes.list})} 
            onEditData={() => this.setState({mode: Modes.list})}
            showLoading={this.handleShowLoadable} 
          />
        );

    }
  }

  handleEditData(list) {
    this.setState({
      editData: list,
      mode: Modes.update,
    } as IUserState);
  }  

  handleEditUserRole(list) {
    this.setState({
      userId: list.UserId,
      mode: Modes.user_role,
    } as IUserState);
  }

  handleUpdateData(data, userid) {
    this.handleShowLoadable(true);

    const user: fetchUtils.IUpdateUser = {
      Username: data.username,
      Password: '',
      FirstName: data.firstName,
      LastName: data.lastName,
      Email: data.email,
      Phone: data.phone,
      MobileNo: data.mobileNo,
      UserId: userid,
      Uid: null
    };

    fetchUtils.UpdateUser(user).then((res) => {
      this.setState({
        mode: Modes.list
      });
    }).catch( (error) => alert('An error occured while updating'));
  }  

  render() {
    const content = this.getContent();

    return (
      <div>
        <Menu attached="top">
          <Menu.Item onClick={() => this.setState({mode: Modes.list})} active={this.state.mode === Modes.list}
            icon="list layout" color="blue"/>

          <Dropdown item={true} icon="wrench" simple={true}>
            <Dropdown.Menu>
              <Dropdown.Header>General</Dropdown.Header>
              <Dropdown.Item onClick={() => this.setState({mode: Modes.new})}>Create New</Dropdown.Item>
              <Dropdown.Divider />
            </Dropdown.Menu>
          </Dropdown>

          {/*<Menu.Menu position="right">
            <div className="ui right aligned category search item">
              <div className="ui transparent icon input">
                <input className="prompt" type="text" placeholder="Search Users..." />
                <i className="search link icon" />
              </div>
              <div className="results"/>
            </div>
          </Menu.Menu>*/}
        </Menu>

        <Segment attached="bottom">
          {content}
        </Segment>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {  
  return {
    handleNavigateTo: (value: pages) => dispatch(navigateTo(value)),    
    showLoadable: (isLoading: boolean) => dispatch(showLoadable(isLoading))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(User) as React.ComponentClass<IUserProps>;
