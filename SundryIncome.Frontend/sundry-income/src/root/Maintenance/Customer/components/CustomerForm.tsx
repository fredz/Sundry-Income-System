import * as React from 'react';
import { Button, Form, Grid, Input, Label, TextArea, Header, Radio } from 'semantic-ui-react';

export interface ICustomerFormProps {
  onCancel: () => void;
  onSave: (data) => void;
  onUpdate: (data, userid) => void;
  data?: any;
  isEdit: boolean;
  customerId?: string;
}

export interface ICustomerFormState {
  customerId: string;
  firstName: string;
  lastName: string;
  billingName: string;
  email: string;
  phone: string;
  mobileNo: string;
  purchaseOrderRequired: boolean;
  aMCInvoiceOnly: boolean;
  defaultInvoiceDescription: string;
  error?: string[];
}

export class CustomerForm extends React.Component<ICustomerFormProps, ICustomerFormState> {
  constructor(props) {
    super(props);

    this.state = {
      customerId: '',
      firstName: '',
      lastName: '',
      billingName: '',
      email: '',
      phone: '',
      mobileNo: '',
      purchaseOrderRequired: false,
      aMCInvoiceOnly: false,
      defaultInvoiceDescription: '',
      error: [],
    };

    if (this.props.isEdit) {
      const { data } = this.props;

      this.state = {
        customerId: data.CustomerId,
        firstName: data.FirstName,
        lastName: data.LastName,
        billingName: data.BillingName,
        email: data.Email,
        phone: data.Phone,
        mobileNo: data.MobileNo,
        purchaseOrderRequired: data.PurchaseOrderRequired,
        aMCInvoiceOnly: data.AMCInvoiceOnly,
        defaultInvoiceDescription: data.DefaultInvoiceDescription,
        error: [],
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);

  }

  handleInputChange(prop, value) {

    const state = {};
    state[prop] = value;

    this.setState(state as ICustomerFormState);
  }

  required() {
    const error = [];

    if (!this.state.firstName || this.state.firstName.trim().length === 0) {
      error.push('firstName');
    }

    if (!this.state.lastName || this.state.lastName.trim().length === 0) {
      error.push('lastName');
    }

    if (!this.state.billingName || this.state.billingName.trim().length === 0) {
      error.push('billingName');
    }

    if (!this.state.email || this.state.email.trim().length === 0) {
      error.push('email');
    }

    return error;
  }   

  verifydata() {

    const required = this.required.bind(this)();

    if (required.length > 0) {
      this.setState({
        error: required
      });
      return;
    }

    if (this.props.isEdit) {
      this.props.onUpdate(this.state, this.props.customerId);
    } else {
      this.props.onSave(this.state);
    }
  }

  render() {
    return (
      <div>
        <Grid centered={true} verticalAlign="middle">
          <Grid.Column textAlign="left" width={'10'}>
            <Header as="h3" textAlign="center" >Customer</Header>
            <Form size="large">

              <Grid columns="equal">

                <Grid.Column>
                  <Form.Field>
                    <label>Company Name</label>
                    <Input
                      id="billingName"
                      placeholder="Company Name"
                      label={{ icon: 'asterisk' }}
                      labelPosition="right corner"
                      value={this.state.billingName}
                      onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                      error={this.state.error.findIndex( (x) => x === 'billingName') > -1}
                    />
                  </Form.Field>
                  <Form.Field>
                    <label >Contact Name</label>
                  </Form.Field>

                  <Form.Group inline={true}>                      
                    <Input
                      id="firstName"
                      placeholder="First Name"
                      label={{ icon: 'asterisk' }}
                      labelPosition="right corner"
                      value={this.state.firstName}
                      onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                      error={this.state.error.findIndex( (x) => x === 'firstName') > -1}
                    />
                    <label/>
                    <Input
                      id="lastName"
                      placeholder="Last Name"
                      label={{ icon: 'asterisk' }}
                      labelPosition="right corner"
                      value={this.state.lastName}
                      onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                      error={this.state.error.findIndex( (x) => x === 'lastName') > -1}
                    />
                  </Form.Group>
                  <Form.Field>
                    <label >Email</label>
                    <Input
                      id="email"
                      type="email"
                      placeholder="Email"
                      label={{ icon: 'asterisk' }}
                      labelPosition="right corner"
                      value={this.state.email}
                      onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                      error={this.state.error.findIndex( (x) => x === 'email') > -1}
                    />
                  </Form.Field>

                  <Form.Field>
                    <label >Phone</label>
                    <Input
                      id="phone"
                      placeholder="Phone"
                      value={this.state.phone}
                      onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                    />
                  </Form.Field>

                  <Form.Field>
                    <label >Mobile No</label>
                    <Input
                      id="mobileNo"
                      placeholder="Mobile No"
                      value={this.state.mobileNo}
                      onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                    />
                  </Form.Field>
                </Grid.Column>

                <Grid.Column>
                  <Form.Field>
                    <label >Purchase Order Required</label>
                    <Radio toggle={true} checked={this.state.purchaseOrderRequired} 
                      onChange={(e: any, v) => { this.handleInputChange('purchaseOrderRequired', v.checked); }}/>
                  </Form.Field>

                  <Form.Field>
                    <label>AMC Invoice Only</label>
                    <Radio toggle={true} checked={this.state.purchaseOrderRequired} 
                      onChange={(e: any, v) => { this.handleInputChange('aMCInvoiceOnly', v.checked); }}/>
                  </Form.Field>

                  <Form.Field>
                    <label >Default Invoice Description</label>
                    <TextArea
                      id="defaultInvoiceDescription"                  
                      placeholder="Default Invoice Description"
                      value={this.state.defaultInvoiceDescription}
                      onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                    />
                  </Form.Field>
                </Grid.Column>

              </Grid>

              <Button.Group floated="right">
                <Button onClick={(e) => { this.props.onCancel(); }} >
                  Cancel
                </Button>
                <Button.Or />
                <Button type="submit" onClick={(e) => { e.preventDefault(); this.verifydata(); }} positive={true} >
                  {this.props.isEdit ? 'Update' : 'Save'}
                </Button>
              </Button.Group>
            </Form>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}
