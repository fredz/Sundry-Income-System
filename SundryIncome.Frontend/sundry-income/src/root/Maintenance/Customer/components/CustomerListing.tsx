import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react';
import { Listing } from '../../../../components/Generic/Listing';
import * as fetchUtils from '../../../../utils/fetchUtils';

interface ICustomerListingProps {
  onEditData: (list) => void;
  onEditCustomerAddress: (list) => void;
  showLoading: (isLoading: boolean) => void;
}
interface ICustomerListingState {
  data: any;
  columns: any;
}

export class CustomerListing extends React.Component<ICustomerListingProps , ICustomerListingState> {

  constructor(props) {
    super(props);

    this.state = {
      data: []
    } as ICustomerListingState;    

    this.handleEditData = this.handleEditData.bind(this);    
    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.prepareAccionButton = this.prepareAccionButton.bind(this);   
    this.handleEditCustomerAddress = this.handleEditCustomerAddress.bind(this);
  }

  componentDidMount() {
    this.props.showLoading(true);
    fetchUtils.GetAllCustomers().then( (customers) => {      
      this.setState({
        data: customers
      });
      this.props.showLoading(false);
    }).catch((error) => alert('An error occurred when listing customers'));
  }   

  handleEditData(list) {    
    this.props.onEditData(list);
  }

  handleDeleteData(list) {}  
  
  handleEditCustomerAddress(list) {    
    this.props.onEditCustomerAddress(list);
  }    

  prepareAccionButton(row) {
    return (        
      <span>

        <a 
          href="#" 
          title="Edit Addresses" 
          onClick={(e) => {e.preventDefault(); this.handleEditCustomerAddress(row); }} >
          <Icon color="blue" name="marker" size="large"  />
        </a>

        <a href="#" title="Edit" onClick={(e) => {e.preventDefault(); this.handleEditData(row); }} >
          <Icon color="blue" name="edit" size="large"  />
        </a>

        <a href="#" title="Delete" onClick={(e) => {e.preventDefault(); this.handleDeleteData(row); }}>
          <Icon color="red" name="trash outline" size="large"  />
        </a>
      </span>      
    );
  }   

  render() {

    const columns = [
      {dataField: 'BillingName', header: 'Company Name'},
      {dataField: 'FirstName', header: 'Contact Name', dataFormat: (value, row) => {
        return <span>{row.FirstName} {row.Lastname}</span>;
      }},
      {dataField: 'Email', header: 'Email'},      
      {dataField: 'Phone', header: 'Phone', dataFormat: (value, row) => {        
        return (value.toString() === '0') ? '' : value;
      }},      
      {dataField: 'MobileNo', header: 'MobileNo', dataFormat: (value, row) => {        
        return (value.toString() === '0') ? '' : value;
      }}, 
      {dataField: '', header: 'Action', dataFormat: (value, row) => {
        return this.prepareAccionButton(row);
      }, textAlign: 'center'},              
    ];    
    return (
      <Listing data={this.state.data} columns={columns} />
    );
  }

}
