import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react';

import { navigateTo, switchSubPage, showLoadable } from '../../../actions/app';

import { pages, subPages, IAddressCustomerPageData } from '../../../constants/pages';

import { ISubPageData } from '../../../state/IAppState';

/** Modes */
import { CustomerForm } from './components/CustomerForm';
import { CustomerListing } from './components/CustomerListing';
import * as fetchUtils from '../../../utils/fetchUtils';
import AddressCustomer from '../AddressCustomer/AddressCustomer';

export interface ICustomerProps {}

interface IConnectedProps {
  pageCode: pages;  
}

interface IConnectedDispatch {
  handleNavigateTo: (value: pages) => void;
  handleSwitchSubPage: (value: subPages, data: ISubPageData) => void;
  showLoadable: (isLoading: boolean) => void;
}

const enum Modes {
  list,
  new,
  update
}

interface ICustomerState {
  mode: Modes;
  editData: any;
  customerId: string;
}

class Customer extends React.Component<ICustomerProps & IConnectedProps & IConnectedDispatch, ICustomerState> {
  constructor(props) {
    super(props);

    this.state = {
      mode: Modes.list,
    } as ICustomerState;

    this.getContent = this.getContent.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleUpdateData = this.handleUpdateData.bind(this);
    this.handleSaveData = this.handleSaveData.bind(this);
    this.handleEditCustomerAddress = this.handleEditCustomerAddress.bind(this);
    this.handleShowLoadable = this.handleShowLoadable.bind(this);
  }

  handleEditCustomerAddress(list) {   

    const subPageData: ISubPageData = {
      data: {
        customerId: list.CustomerId,
        companyName: list.BillingName
      }
    };

    this.props.handleSwitchSubPage(subPages.maintenance_addresscustomers, subPageData);
  }  

  handleEditData(list) {
    
    this.setState({
      customerId: list.CustomerId,
      editData: list,
      mode: Modes.update,
    } as ICustomerState);
  } 

  handleShowLoadable(isLoading: boolean) {
    this.props.showLoadable(isLoading);
  }  

  getContent() {
    switch (this.state.mode) {
      default:
      case Modes.list:
        return (
          <CustomerListing 
            onEditData={this.handleEditData}
            onEditCustomerAddress={this.handleEditCustomerAddress}
            showLoading={this.handleShowLoadable}
          />          
        );

      case Modes.new:
        return (
          <CustomerForm 
            isEdit={false} 
            onCancel={() => this.setState({mode: Modes.list})} 
            onSave={this.handleSaveData} 
            onUpdate={null}/>
        );
      
      case Modes.update:
        return (
          <CustomerForm 
            data={this.state.editData} 
            isEdit={true} 
            onCancel={() => this.setState({mode: Modes.list})}  
            onSave={null} 
            onUpdate={this.handleUpdateData}
            customerId={this.state.customerId}
            />          
        );
    }
  }

  handleUpdateData(data, customerId) {    

    this.handleShowLoadable(true);

    const customer: fetchUtils.IUpdateCustomer = {
      FirstName: data.firstName,
      LastName: data.lastName,
      BillingName: data.billingName,
      Email: data.email,
      Phone: data.phone,
      MobileNo: data.mobileNo,
      PurchaseOrderRequired: data.purchaseOrderRequired,
      AMCInvoiceOnly: data.aMCInvoiceOnly,
      DefaultInvoiceDescription: data.defaultInvoiceDescription,
      CustomerId: customerId,
      Uid: null
    };

    fetchUtils.UpdateCustomer(customer).then((res) => {
      this.setState({
        mode: Modes.list
      });      
    }).catch( (error) => alert('An error occured while updating'));
  } 

  handleSaveData(data) {
    this.handleShowLoadable(true);
    
    const customer: fetchUtils.ICustomer = {
      FirstName: data.firstName,
      LastName: data.lastName,
      BillingName: data.billingName,
      Email: data.email,
      Phone: data.phone,
      MobileNo: data.mobileNo,
      PurchaseOrderRequired: data.purchaseOrderRequired,
      AMCInvoiceOnly: data.aMCInvoiceOnly,
      DefaultInvoiceDescription: data.defaultInvoiceDescription,
    };

    fetchUtils.AddCustomer(customer).then((res) => {
      this.setState({
        mode: Modes.list
      });
    }).catch( (error) => alert('An Error Occured'));
  }   

  render() {
    const content = this.getContent();     

    return (
      <div>
        <Menu attached="top">
          <Menu.Item onClick={() => this.setState({mode: Modes.list})} active={this.state.mode === Modes.list}
            icon="list layout" color="blue"/>

          <Dropdown item={true} icon="wrench" simple={true} >
            <Dropdown.Menu>
              <Dropdown.Header>General</Dropdown.Header>
              <Dropdown.Item onClick={() => this.setState({mode: Modes.new})}>Create New</Dropdown.Item>
              <Dropdown.Divider />
            </Dropdown.Menu>
          </Dropdown>

          {/*<Menu.Menu position="right">
            <div className="ui right aligned category search item">
              <div className="ui transparent icon input">
                <input className="prompt" type="text" placeholder="Search Users..." />
                <i className="search link icon" />
              </div>
              <div className="results"/>
            </div>
          </Menu.Menu>*/}
        </Menu>

        <Segment attached="bottom">
          {content}
        </Segment>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    handleNavigateTo: (value: pages) => dispatch(navigateTo(value)),
    handleSwitchSubPage: (value: subPages, data: ISubPageData) => dispatch(switchSubPage(value, data)),
    showLoadable: (isLoading: boolean) => dispatch(showLoadable(isLoading))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Customer) as React.ComponentClass<ICustomerProps>;
