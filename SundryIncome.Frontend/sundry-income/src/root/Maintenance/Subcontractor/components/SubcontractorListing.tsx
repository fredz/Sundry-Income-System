import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react';
import { Listing } from '../../../../components/Generic/Listing';
import * as fetchUtils from '../../../../utils/fetchUtils';

interface ISubcontractorListingProps {
  onEditData: (list) => void;
  showLoading: (isLoading: boolean) => void;
}
interface ISubcontractorListingState {
  data: any;
  columns: any;
}

export class SubcontractorListing extends React.Component<ISubcontractorListingProps , ISubcontractorListingState> {

  constructor(props) {
    super(props);

    this.state = {
      data: []
    } as ISubcontractorListingState;

  }

  componentDidMount() {
    this.props.showLoading(true);
    
    fetchUtils.GetAllSubcontractors().then( (subcontractors) => {      
      this.setState({
        data: subcontractors
      });
      this.props.showLoading(false);
    }).catch((error) => alert('An error occurred when listing subcontractors'));
  }  

  handleEditData(list) {    
    this.props.onEditData(list);
  }  

  handleDeleteData(list) {} 

  prepareAccionButton(row) {
    return (        
      <span>
        <a href="#" title="Edit" onClick={(e) => {e.preventDefault(); this.handleEditData(row); }} >
          <Icon color="blue" name="edit" size="large"  />
        </a>

        <a href="#" title="Delete" onClick={(e) => {e.preventDefault(); this.handleDeleteData(row); }}>
          <Icon color="red" name="trash outline" size="large"  />
        </a>
      </span>      
    );
  }  

  render() {

    const columns = [
      {dataField: 'CompanyName', header: 'Company Name'},   
      {dataField: 'FirstName', header: 'Contact Name', dataFormat: (value, row) => {
        return <span>{row.FirstName} {row.LastName}</span>;
      }},
      {dataField: '', header: 'Action', dataFormat: (value, row) => {
        return this.prepareAccionButton(row);
      }, textAlign: 'center'},             
    ];    

    return (
      <Listing data={this.state.data} columns={columns} />
    );
  }

}
