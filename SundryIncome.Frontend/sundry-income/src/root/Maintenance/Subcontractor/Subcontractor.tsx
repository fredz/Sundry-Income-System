import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react';

import { navigateTo, showLoadable } from '../../../actions/app';

import { pages } from '../../../constants/pages';

/** Modes */
import { SubcontractorForm } from './components/SubcontractorForm';
import { SubcontractorListing } from './components/SubcontractorListing';
import * as fetchUtils from '../../../utils/fetchUtils';

export interface ISubcontractorProps {}

interface IConnectedProps {
  pageCode: pages;
}

interface IConnectedDispatch {
  handleNavigateTo: (value: pages) => void;
  showLoadable: (isLoading: boolean) => void;
}

const enum Modes {
  list,
  new,
  update
}

interface ISubcontractorState {
  mode: Modes;
  editData: any;
  subcontractorId: string;
}

class Subcontractor extends React.Component<
ISubcontractorProps & IConnectedProps & IConnectedDispatch, ISubcontractorState> {
  constructor(props) {
    super(props);

    this.state = {
      mode: Modes.list,
    } as ISubcontractorState;

    this.getContent = this.getContent.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleUpdateData = this.handleUpdateData.bind(this);
    this.handleSaveData = this.handleSaveData.bind(this);
    this.handleShowLoadable = this.handleShowLoadable.bind(this);
  }

  handleEditData(list) {
    
    this.setState({
      editData: list,
      subcontractorId: list.SubContractorId,
      mode: Modes.update,
    } as ISubcontractorState);
  }

  handleShowLoadable(isLoading: boolean) {
    this.props.showLoadable(isLoading);
  }     

  getContent() {
    switch (this.state.mode) {
      default:
      case Modes.list: 
        return (
          <SubcontractorListing 
            onEditData={this.handleEditData} 
            showLoading={this.handleShowLoadable} 
          />
        );

      case Modes.new:
        return (
          <SubcontractorForm 
            isEdit={false} 
            onCancel={() => this.setState({mode: Modes.list})} 
            onSave={this.handleSaveData} 
            onUpdate={null}/>
        );
      
      case Modes.update:
        return (
          <SubcontractorForm 
            data={this.state.editData} 
            isEdit={true} 
            onCancel={() => this.setState({mode: Modes.list})} 
            onSave={null} 
            onUpdate={this.handleUpdateData}
            subcontractorId={this.state.subcontractorId}
          />
        );
    }
  }

  handleUpdateData(data, subcontractorId) {
    this.handleShowLoadable(true);

    const subcontractor: fetchUtils.IUpdateSubcontractor = {
      FirstName: data.firstName,      
      LastName: data.lastName,
      CompanyName: data.companyName,
      SubcontractorId: subcontractorId,
      Uid: null
    };

    fetchUtils.UpdateSubcontractor(subcontractor).then((res) => {
      this.setState({
        mode: Modes.list
      });
    }).catch( (error) => alert('An error occured while updating'));
  } 

  handleSaveData(data) {
    this.handleShowLoadable(true);
    
    const subcontractor: fetchUtils.ISubcontractor = {
      FirstName: data.firstName,      
      LastName: data.lastName,
      CompanyName: data.companyName,
    };

    fetchUtils.AddSubcontractor(subcontractor).then((res) => {
      this.setState({
        mode: Modes.list
      });
    }).catch( (error) => alert('An Error Occured'));
  }     

  render() {
    const content = this.getContent();

    return (
      <div>
        <Menu attached="top">
          <Menu.Item onClick={() => this.setState({mode: Modes.list})} active={this.state.mode === Modes.list}
            icon="list layout" color="blue"/>

          <Dropdown item={true} icon="wrench" simple={true} >
            <Dropdown.Menu>
              <Dropdown.Header>General</Dropdown.Header>
              <Dropdown.Item onClick={() => this.setState({mode: Modes.new})}>Create New</Dropdown.Item>
              <Dropdown.Divider />
            </Dropdown.Menu>
          </Dropdown>

          {/*<Menu.Menu position="right">
            <div className="ui right aligned category search item">
              <div className="ui transparent icon input">
                <input className="prompt" type="text" placeholder="Search Subcontractors..." />
                <i className="search link icon" />
              </div>
              <div className="results"/>
            </div>
          </Menu.Menu>*/}
        </Menu>

        <Segment attached="bottom">
          {content}
        </Segment>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    handleNavigateTo: (value: pages) => dispatch(navigateTo(value)),
    showLoadable: (isLoading: boolean) => dispatch(showLoadable(isLoading))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Subcontractor) as React.ComponentClass<ISubcontractorProps>;
