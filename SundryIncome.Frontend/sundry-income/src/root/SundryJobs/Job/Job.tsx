import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment } from 'semantic-ui-react';

import { navigateTo, showLoadable } from '../../../actions/app';

import { pages } from '../../../constants/pages';

/** Modes */
import { JobForm } from './components/JobForm';
import { JobListing } from './components/JobListing';
import { JobAssigments } from './components/JobAssigments';
import * as fetchUtils from '../../../utils/fetchUtils';

export interface IJobProps {}

interface IConnectedProps {
  pageCode: pages;
}

interface IConnectedDispatch {
  handleNavigateTo: (value: pages) => void;
  showLoadable: (isLoading: boolean) => void;
}

const enum Modes {
  list,
  new,
  update,
  job_assigments
}

interface IJobState {
  mode: Modes;
  editData: any;
  jobId: string;
}

class Job extends React.Component<IJobProps & IConnectedProps & IConnectedDispatch, IJobState> {
  constructor(props) {
    super(props);

    this.state = {
      mode: Modes.list,
    } as IJobState;

    this.getContent = this.getContent.bind(this);
    this.handleEditData = this.handleEditData.bind(this);
    this.handleUpdateData = this.handleUpdateData.bind(this);
    this.handleSaveData = this.handleSaveData.bind(this);
    this.handleShowLoadable = this.handleShowLoadable.bind(this);
    this.handleEditJobAssigments = this.handleEditJobAssigments.bind(this);
  }

  handleEditData(list) {
  
    this.setState({
      editData: list,
      jobId: list.JobId,
      mode: Modes.update,
    } as IJobState);
  }

  handleEditJobAssigments(list) {
  
    this.setState({
      editData: list,
      jobId: list.JobId,
      mode: Modes.job_assigments,
    } as IJobState);
  }  

  handleShowLoadable(isLoading: boolean) {
    this.props.showLoadable(isLoading);
  } 

  getContent() {
    switch (this.state.mode) {
      default:
      case Modes.list: 
        return (
          <JobListing 
            onEditData={this.handleEditData} 
            showLoading={this.handleShowLoadable}
            onEditJobAssigments={this.handleEditJobAssigments}
          />
        );

      case Modes.new:
        return (
          <JobForm 
            isEdit={false} 
            onCancel={() => this.setState({mode: Modes.list})} 
            onSave={this.handleSaveData} 
            onUpdate={null}/>
        );
      
      case Modes.update:
        return (
          <JobForm 
            data={this.state.editData} 
            isEdit={true} 
            onCancel={() => this.setState({mode: Modes.list})} 
            onSave={null} 
            onUpdate={this.handleUpdateData}
            jobId={this.state.jobId}
          />
        );

      case Modes.job_assigments:
        return (
          <JobAssigments 
            jobId={this.state.jobId} 
            onCancel={() => this.setState({mode: Modes.list})}            
            showLoading={this.handleShowLoadable} 
            jobData={this.state.editData}
          />
        );        
    }
  }

  handleUpdateData(data, JobId) {
    this.handleShowLoadable(true);

    const Job: fetchUtils.IUpdateJob = {  
      CustomerId: data.customerId,
      AreaManagerId: data.areaManagerId,
      OrderNo: data.orderNo,
      Description: data.description,
      RequestedDate: data.requestedDate,
      DateFrom: data.dateFrom,
      DateTo: data.dateTo,      
      AMCAmount: data.aMCAmount,
      JobId: data.jobId,
      Uid: null
    };

    fetchUtils.UpdateCreateJob(Job).then((res) => {
      this.setState({
        mode: Modes.list
      });
    }).catch( (error) => alert('An error occured while updating'));
  } 

  handleSaveData(data) {
    this.handleShowLoadable(true);

    const defaultJobStatusId = '1';
    const defaultStepNo = '1';    
    
    const JobProgressStatus: fetchUtils.IJobProgressStatus = {
      CustomerId: data.customerId,
      AreaManagerId: data.areaManagerId,
      OrderNo: data.orderNo,
      Description: data.description,
      RequestedDate: data.requestedDate,
      DateFrom: data.dateFrom,
      DateTo: data.dateTo,      
      AMCAmount: data.aMCAmount, 
      JobStatusId: data.jobStatusId,
      StepNo: defaultStepNo
    };

    fetchUtils.AddJobProgressStatus(JobProgressStatus).then((res) => {
      this.setState({
        mode: Modes.list
      });
    }).catch( (error) => alert('An Error Occured'));
  }     

  render() {
    const content = this.getContent();

    return (
      <div>
        <Menu attached="top">
          <Menu.Item onClick={() => this.setState({mode: Modes.list})} active={this.state.mode === Modes.list}
            icon="list layout" color="blue"/>

          <Dropdown item={true} icon="wrench" simple={true} >
            <Dropdown.Menu>
              <Dropdown.Header>General</Dropdown.Header>
              <Dropdown.Item onClick={() => this.setState({mode: Modes.new})} >Create New</Dropdown.Item>
              <Dropdown.Divider />
            </Dropdown.Menu>
          </Dropdown>

          {/*<Menu.Menu position="right">
            <div className="ui right aligned category search item">
              <div className="ui transparent icon input">
                <input className="prompt" type="text" placeholder="Search Jobs..." />
                <i className="search link icon" />
              </div>
              <div className="results"/>
            </div>
          </Menu.Menu>*/}
        </Menu>

        <Segment attached="bottom">
          {content}
        </Segment>
      </div>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  return {
    pageCode: state.app.pageCode,
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    handleNavigateTo: (value: pages) => dispatch(navigateTo(value)),
    showLoadable: (isLoading: boolean) => dispatch(showLoadable(isLoading))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(Job) as React.ComponentClass<IJobProps>;
