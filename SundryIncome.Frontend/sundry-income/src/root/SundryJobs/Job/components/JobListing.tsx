import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Icon, Menu, Segment, Select } from 'semantic-ui-react';
import { Listing } from '../../../../components/Generic/Listing';
import * as moment from 'moment';
import * as fetchUtils from '../../../../utils/fetchUtils';

interface IJobListingProps {
  onEditData: (list) => void;
  showLoading: (isLoading: boolean) => void;
  onEditJobAssigments: (list) => void;
}
interface IJobListingState {
  data: any;
  columns: any;
  jobStatuses: any;
}

export class JobListing extends React.Component<IJobListingProps , IJobListingState> {

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      jobStatuses: []
    } as IJobListingState;

    this.handleChangeStatus = this.handleChangeStatus.bind(this);

  }

  componentDidMount() {
    this.refreshList();
  }  

  handleEditData(list) {    
    this.props.onEditData(list);
  }  

  handleDeleteData(list) {

    const { JobId } = list;
    this.props.showLoading(true);
    
    fetchUtils.DeleteCreateJob({
        JobId
    }).then( (res) => {
        alert('Job ' + JobId + ' was deleted');
        this.setState({
            data: []
        });
        this.refreshList();
    }).catch( (error) => {      
        alert('Un Unknow Error Occurred when deleting...');
    });
  } 

  refreshList() {
    this.props.showLoading(true);
    const { GetAllJobProgressStatus, GetAllJobStatuses } = fetchUtils;

    Promise.all([GetAllJobProgressStatus(), GetAllJobStatuses()])
    .then((res) => {
      const jobProgressStatus = res[0];
      const jobStatuses = res[1];    
          
      this.setState({
        data: jobProgressStatus,
        jobStatuses: (jobStatuses as any).map( (x, i) => {
          return {
            key: i,
            text: x.Name,
            value: x.JobStatusId,            
            label: { empty: true, circular: true, style: {background: x.ColorCode} },
            
          };
        })        
      });
      this.props.showLoading(false);
    }).catch((error) => alert('An error occurred when listing Jobs'));
  }

  prepareAccionButton(row) {
    return (        
      <span>
        <a href="#" title="Job Assigments"
         onClick={(e) => {e.preventDefault(); this.props.onEditJobAssigments(row); }}>
          <Icon color="green" name="settings" size="large"  />
        </a>        

        <a href="#" title="Edit" onClick={(e) => {e.preventDefault(); this.handleEditData(row); }} >
          <Icon color="blue" name="edit" size="large"  />
        </a>

        <a href="#" title="Delete" onClick={(e) => {e.preventDefault(); this.handleDeleteData(row); }}>
          <Icon color="red" name="trash outline" size="large"  />
        </a>
      </span>      
    );
  } 
  
  rowOptions() {    
    const optionsArray = {};
    this.state.data.map((row, index) => {
      const color = fetchUtils.hexToRGB(row.ColorCode, '0.2');
      optionsArray[index] = {style: {'background-color': color}};
    });        

    return optionsArray;
  }

  handleChangeStatus(event: any, data: any, row) {    

    const jobProgressId = row.JobProgressId;
    const jobId = row.JobId;
    const jobStatusId = row.JobStatusId;
    const newJobStatusId = data.value;

    if (jobStatusId === newJobStatusId) {
      return false;
    }

    const JobProgressStatus: fetchUtils.IUpdateJobProgressStatus = {
      JobId: jobId,    
      JobProgressId: jobProgressId,
      JobStatusId: newJobStatusId,
      Uid: null     
    };
    
    fetchUtils.UpdateJobProgressStatus(JobProgressStatus).then((res) => {    
      this.refreshList();
    }).catch( (error) => alert('An error occured while updating'));
    
  }

  render() {
    
    const columns = [      
      {dataField: 'CustomerFullName', header: 'Company Name'},
      {dataField: 'AManagerFullName', header: 'Area Manager'},
      {dataField: 'OrderNo', header: 'Order No'},
      {dataField: 'Description', header: 'Description'},
      {dataField: 'RequestedDate', header: 'Requested Date', dataFormat: (value, row) => {        
        return value ? moment(value).format('D MMM Y') : ''; 
      }},
      {dataField: 'DateFrom', header: 'Date From', dataFormat: (value, row) => {        
        return value ? moment(value).format('D MMM Y') : ''; 
      }},
      {dataField: 'DateTo', header: 'DateTo' , dataFormat: (value, row) => {        
        return value ? moment(value).format('D MMM Y') : ''; 
      }},
      {dataField: 'AMCAmount', header: 'AMC Amount', dataFormat: (value, row) => {        
        return parseFloat(value).toFixed(2); 
      }},
      {dataField: 'JobStatusId', header: 'Status', dataFormat: (value, row) => {        
        return (
          <Dropdown 
            defaultValue={row.JobStatusId}
            icon='arrow right' 
            options={this.state.jobStatuses}
            selection={true}
            floating={true} 
            labeled={true} 
            button={true} 
            className='icon'
            onChange={(e,d) => {this.handleChangeStatus(e, d, row)}}
          />         
        );
      }},      

      {dataField: '', header: 'Action', dataFormat: (value, row) => {
        return this.prepareAccionButton(row);
      }, textAlign: 'center'},             
    ];

    return (
      <Listing data={this.state.data} columns={columns} rowOptions={this.rowOptions()} />
    );
  }

}
