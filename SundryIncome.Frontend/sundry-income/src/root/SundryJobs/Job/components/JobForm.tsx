import * as React from 'react';
import { Button, Form, Grid, Input, Label, Select, TextArea, Dropdown } from 'semantic-ui-react';
import * as Datetime from 'react-datetime';
import * as fetchUtils from '../../../../utils/fetchUtils';
import * as moment from 'moment';

export interface IJobFormProps {
  onCancel: () => void;
  onSave: (data) => void;
  onUpdate: (data, jobid) => void;
  data?: any;
  isEdit: boolean;
  jobId?: string;
}

export interface IJobFormState {
  jobId: string;    
  customerId: any;
  areaManagerId: any;
  orderNo: string;
  description: string;
  requestedDate: any;
  dateFrom: any;
  dateTo: any;  
  aMCAmount: string;
  customers: any[];
  users: any[];
  error?: string[];
  jobStatus: any[];
  jobStatusId: any;
}

export class JobForm extends React.Component<IJobFormProps, IJobFormState> {
  constructor(props) {
    super(props);

    this.state = {
      jobId: '',    
      customerId: '',
      areaManagerId: '',
      orderNo: '',
      description: '',
      requestedDate: null,
      dateFrom: null,
      dateTo: null,      
      aMCAmount: '',
      customers: null,
      users: null,
      error: [],
      jobStatus: [], 
      jobStatusId: '1'
    };

    if (this.props.isEdit) {
      const { data } = this.props;

      this.state = {
        jobId: data.JobId,
        customerId: data.CustomerId,
        areaManagerId: data.AreaManagerId,
        orderNo: data.OrderNo,
        description: data.Description,
        requestedDate: moment(data.RequestedDate).format('D MMM Y'),
        dateFrom: moment(data.DateFrom).format('D MMM Y'),
        dateTo: moment(data.DateTo).format('D MMM Y'),
        aMCAmount: parseFloat(data.AMCAmount).toFixed(2),
        customers: null,
        users: null,
        error: [],
        jobStatus: [],
        jobStatusId: data.JobStatusId
      };
    }

    this.handleInputChange = this.handleInputChange.bind(this);
    this.verifydata = this.verifydata.bind(this);
    this.refreshList = this.refreshList.bind(this);
  }

  componentDidMount() {
    this.refreshList();
  }  

  handleInputChange(prop, value) {

    const state = {};
    state[prop] = value;

    this.setState(state as IJobFormState);
  }

  required() {
    const error = [];

    if (!this.state.customerId || this.state.customerId.toString().trim().length === 0) {
      error.push('customerId');
    }

    if (!this.state.areaManagerId || this.state.areaManagerId.toString().trim().length === 0) {
      error.push('areaManagerId');
    }

    if (!this.state.orderNo || this.state.orderNo.toString().trim().length === 0) {
      error.push('orderNo');
    }

    if (!this.state.description || this.state.description.toString().trim().length === 0) {
      error.push('description');
    }

    if (!this.state.requestedDate || this.state.requestedDate.toString().trim().length === 0) {
      error.push('requestedDate');
    }  

    if (!this.state.dateFrom || this.state.dateFrom.toString().trim().length === 0) {
      error.push('dateFrom');
    }  

    if (!this.state.dateTo || this.state.dateTo.toString().trim().length === 0) {
      error.push('dateTo');
    }  

    if (!this.state.aMCAmount || this.state.aMCAmount.toString().trim().length === 0) {
      error.push('aMCAmount');
    }   

    if (!this.state.jobStatusId || this.state.jobStatusId.toString().trim().length === 0) {
      error.push('jobStatusId');
    }                               

    return error;
  }  

  verifydata() {

    const required = this.required.bind(this)();

    if (required.length > 0) {
      this.setState({
        error: required
      });
      return;
    }

    if (this.props.isEdit) {
      this.props.onUpdate(this.state, this.props.jobId);
    } else {
      this.props.onSave(this.state);
    }
  }

  refreshList() {
    const { GetAllUsers, GetAllCustomers, GetAllJobStatuses } = fetchUtils;
    
    Promise.all([GetAllCustomers(), GetAllUsers(), GetAllJobStatuses()])
      .then((res) => {

        const customers = res[0];
        const users = res[1];
        const jobStatus = res[2];

        this.setState({
          customers: (customers as any).map( (x, i) => {
            return {
                key: i,
                text: x.FirstName + ' ' + x.LastName,
                value: x.CustomerId
            };
          }),          
          users: (users as any).map( (x, i) => {
            return {
                key: i,
                text: x.FirstName + ' ' + x.LastName,
                value: x.UserId
            };
          }),                    
          jobStatus: (jobStatus as any).map( (x, i) => {
            return {
              key: i,
              text: x.Name,
              value: x.JobStatusId,            
              label: { empty: true, circular: true, style: {background: x.ColorCode} },
              
            };
          }),          
        });
      }).catch( () => {
        this.setState({
            users: [],
            customers: [],
        });
        alert('An Unknow error ocurred while querying user users or customers');
    });
  }  

  render() {

    if (this.state.customers === null || this.state.users === null) {
      return null;
    }

    return (
      <div>
        <Grid centered={true} verticalAlign="middle">
          <Grid.Column textAlign="left" width={'fifteen'}>
            <Form size="large">
              <Grid columns="equal">
                <Grid.Column>
                  <Form.Field>
                    <label >Customer</label>
                    <Select fluid={true} search={true}
                      options={this.state.customers} onChange={(e, d) => this.setState({customerId: d.value})}
                      defaultValue={this.state.customerId}
                      error={this.state.error.findIndex( (x) => x === 'customerId') > -1}
                    />
                  </Form.Field>

                  <Form.Field>
                    <label >Area Manager</label>
                    <Select fluid={true}  search={true}
                      options={this.state.users} onChange={(e, d) => this.setState({areaManagerId: d.value})}
                      defaultValue={this.state.areaManagerId}
                      error={this.state.error.findIndex( (x) => x === 'areaManagerId') > -1}
                    />
                  </Form.Field>

                  <Form.Field>
                    <label >Order No</label>
                    <Input
                      id="orderNo"
                      placeholder="Order No"
                      label={{ icon: 'asterisk' }}
                      labelPosition="right corner"                  
                      value={this.state.orderNo}
                      onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}   
                      error={this.state.error.findIndex( (x) => x === 'orderNo') > -1}               
                    />
                  </Form.Field>

                  <Form.Field>
                    <label >Description</label>
                    <TextArea 
                      id="description"
                      placeholder="Tell us more" 
                      label={{ icon: 'asterisk' }}
                      labelPosition="right corner"                       
                      value={this.state.description}
                      onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                      error={this.state.error.findIndex( (x) => x === 'description') > -1}
                    />
                  </Form.Field>                

                </Grid.Column>

                <Grid.Column>
                  <Form.Field>
                    <label >Requested Date</label>
                    <Datetime             
                      value={this.state.requestedDate}               
                      dateFormat={'D MMM YY'} 
                      timeFormat={''}  
                      closeOnSelect={true}
                      onChange={(e: any) => { this.handleInputChange('requestedDate', e.format('D MMM Y')); }}
                    />
                  </Form.Field>

                  <Form.Field>
                    <label >Date From</label>
                    <Datetime             
                      value={this.state.dateFrom}               
                      dateFormat={'D MMM YY'} 
                      timeFormat={''}  
                      closeOnSelect={true}                  
                      onChange={(e: any) => { this.handleInputChange('dateFrom', e.format('D MMM Y')); }}
                    />
                  </Form.Field>

                  <Form.Field>
                    <label >Date To</label>
                    <Datetime             
                      value={this.state.dateTo}               
                      dateFormat={'D MMM YY'} 
                      timeFormat={''}  
                      closeOnSelect={true}                  
                      onChange={(e: any) => { this.handleInputChange('dateTo', e.format('D MMM Y')); }}
                    />
                  </Form.Field>

                  <Form.Field>
                    <label >AMC Amount</label>
                    <Input
                      id="aMCAmount"
                      placeholder="AMC Amount"
                      label={{ icon: 'asterisk' }}
                      labelPosition="right corner"                       
                      value={this.state.aMCAmount}
                      onChange={(e: any) => { this.handleInputChange(e.target.id, e.target.value); }}
                      error={this.state.error.findIndex( (x) => x === 'aMCAmount') > -1}
                    /> 
                  </Form.Field>

                  <Form.Field>
                    <label >Job Status</label>
                    <Dropdown 
                      id="jobStatusId"
                      defaultValue={this.state.jobStatusId}
                      icon="arrow right" 
                      options={this.state.jobStatus}
                      selection={true}
                      floating={true} 
                      labeled={true} 
                      button={true} 
                      className="icon"
                      onChange={(e, d) => {this.handleInputChange(e.target.id, d)}}
                      error={this.state.error.findIndex( (x) => x === 'jobStatusId') > -1}
                    />                     
                  </Form.Field>

                </Grid.Column>

              </Grid>
              <Button.Group floated="right">
                <Button onClick={(e) => { this.props.onCancel(); }} >
                  Cancel
                </Button>
                <Button.Or />
                <Button type="submit" onClick={(e) => { e.preventDefault(); this.verifydata(); }} positive={true} >
                  {this.props.isEdit ? 'Update' : 'Save'}
                </Button>
              </Button.Group>          
            </Form>             
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}
