import * as React from 'react';
import { connect } from 'react-redux';
import { Dropdown, Segment, Input, Select, Button, Container, Grid, Icon, Form, Checkbox } from 'semantic-ui-react';
import { Listing } from '../../../../components/Generic/Listing';
import { InputCell } from '../../../../components/Generic/InputCell';
import * as fetchUtils from '../../../../utils/fetchUtils';

interface IJobAssigmentsProps {
  jobId: string;  
  onCancel: () => void;
  showLoading: (isLoading: boolean) => void;
  jobData: any;
}
interface IJobAssigmentsState {
  data: any;
  headerColumns: any;
  franchisees: any[];
  subcontractors: any[];
  selectedFranchisee: any;
  selectedSubcontractor: any;
}

export class JobAssigments extends React.Component<IJobAssigmentsProps , IJobAssigmentsState> {

  constructor(props) {
    super(props);

    this.state = {
      franchisees: [],
      selectedFranchisee: 1,
      subcontractors: [],
      selectedSubcontractor: 1,
      data: null
    } as IJobAssigmentsState;

    this.handleDeleteData = this.handleDeleteData.bind(this);
    this.prepareAccionButton = this.prepareAccionButton.bind(this);
    this.headerColumnArray = this.headerColumnArray.bind(this);
    this.handleAssignmentPrimary = this.handleAssignmentPrimary.bind(this);    
    this.refresh = this.refresh.bind(this);
    this.handleAddFranchisee = this.handleAddFranchisee.bind(this);
    this.handleAddSubcontractor = this.handleAddSubcontractor.bind(this);
    this.handleEditAmount = this.handleEditAmount.bind(this);        
  }

  componentDidMount() {
    this.refresh();
  }

  handleAssignmentPrimary(row, data) {
    this.props.showLoading(true);

    const JobAssignment: fetchUtils.IUpdateJobAssignmentPrimary = {  
      JobAssignId: row.JobAssignId,
      Primary: data.checked, 
      Uid: null
    };

    fetchUtils.UpdateJobAssignmentPrimary(JobAssignment).then((res) => {
      this.props.showLoading(false);
    }).catch( (error) => alert('An error occured while updating'));    
  }  

  handleDeleteData(list) {
    const { JobAssignId } = list;    
    this.props.showLoading(true);
    
    fetchUtils.DeleteJobAssignment({
        JobAssignId
    }).then( (res) => {        
        fetchUtils.showAlert('success', 'Job Assignment was deleted');
        this.refresh();
    }).catch( () => {        
        fetchUtils.showAlert('error', 'Un Unknow Error Occurred when deleting...');
    });    

  }    

  prepareAccionButton(row) {
    return (        
      <span>
        <a href="#" title="Delete" onClick={(e) => {e.preventDefault(); this.handleDeleteData(row); }}>
          <Icon color="red" name="trash outline" size="large"/>          
        </a>
      </span>      
    );
  }
  
  handleEditAmount(amount, jobAssignId) {
    this.props.showLoading(true);
    const JobAssignment: fetchUtils.IUpdateJobAssignmentAmount = {  
      JobAssignId: jobAssignId,
      Amount: amount, 
      Uid: null
    };

    fetchUtils.UpdateJobAssignmentAmount(JobAssignment).then((res) => {
      this.props.showLoading(false);
    }).catch( (error) => alert('An error occured while updating'));      

  }

  headerColumnArray() {
    const column = [
      {dataField: '', header: 'Type', dataFormat: (value, row) => {
        const assignmentType = row.SubcontractorId == null ? 'Franchisee' : 'Subcontractor';      
        return assignmentType;
      }},      
      {dataField: 'Name', header: 'Name', dataFormat: (value, row) => {        
        const name = row.SubcontractorId == null ? row.FranchiseeFullName : row.SubcontractorFullName;      
        return name;
      }},
      {dataField: 'Amount', header: 'Amount', 
        options: {textAlign: 'center', selectable: true}, dataFormat: (value, row) => {         
        return (
          <InputCell onBlur={this.handleEditAmount} rowId={row.JobAssignId} value={value} />          
        );
      }},      
      {dataField: 'Primary', header: 'Primary', options: {textAlign: 'center'}, dataFormat: (value, row) => {        
        return (
          <Checkbox toggle={true} onChange={(e, d) => this.handleAssignmentPrimary(row, d)} 
            defaultChecked={row.Primary === true ? true : false} />
        );
      }},
      {dataField: '', header: 'Action', dataFormat: (value, row) => {
        return this.prepareAccionButton(row);
      }, options: {textAlign: 'center'}},      
    ]; 

    return column;  
  } 

  refresh() {
    const { GetAllFranchisees, GetAllSubcontractors, GetJobAssignmentByJobId } = fetchUtils;
    this.props.showLoading(true);

    Promise.all([GetAllFranchisees(), GetAllSubcontractors(), GetJobAssignmentByJobId(this.props.jobId)])
      .then((res) => {
        const franchisees = res[0];
        const subcontractors = res[1];
        const jobAssignments = res[2];
        this.setState({
          franchisees: (franchisees as any).map( (x, i) => {
            return {
              key: i,
              text: x.FirstName + ' ' + x.LastName,
              value: x.FranchiseeId
            };
          }),
          selectedFranchisee: franchisees[0].FranchiseeId,
          subcontractors: (subcontractors as any).map( (x, i) => {
            return {
              key: i,
              text: x.FirstName + ' ' + x.LastName,
              value: x.SubContractorId
            };
          }),
          selectedSubcontractor: subcontractors[0].SubContractorId,          
          data: jobAssignments
        });

        this.props.showLoading(false);

      }).catch( () => {
        this.setState({
            franchisees: [],
            subcontractors: []
        });
        alert('An Unknow error ocurred while querying user roles');
    });
  }  

  handleAddFranchisee() {
    this.props.showLoading(true);
    
    const JobAssignment: fetchUtils.IJobAssignment = {
      JobId: this.props.jobId,
      FranchiseeId: this.state.selectedFranchisee,
      SubcontractorId: null,
      Amount: '',
      PurchaseInvoiceNo: '',
      Primary: false,
    };

    fetchUtils.AddJobAssignment(JobAssignment).then((res) => {
      this.refresh();
    }).catch( (error) => alert('An Error Occured'));    
  }

  handleAddSubcontractor() {
    this.props.showLoading(true);
    
    const JobAssignment: fetchUtils.IJobAssignment = {
      JobId: this.props.jobId,
      FranchiseeId: this.state.selectedFranchisee,
      SubcontractorId: this.state.selectedSubcontractor,
      Amount: '',
      PurchaseInvoiceNo: '',
      Primary: false,
    };

    fetchUtils.AddJobAssignment(JobAssignment).then((res) => {
      this.refresh();
    }).catch( (error) => alert('An Error Occured'));      
  }  

  render() {
    if (this.state.data === null) {
      return null;
    }

    const data = [
      {JobAssignId: '1', Name: 'Franchisee 1', Amount: '200', Primary: true},
      {JobAssignId: '2', Name: 'Subcontractor 1', Amount: '100', Primary: false},
      {JobAssignId: '3', Name: 'Franchisee 2', Amount: '500', Primary: true},
    ];

    const headerColumns = this.headerColumnArray();

    return (
      <div>
        <Segment padded={true} textAlign="center" >
        <Form>
          <Grid divided="vertically">
            <Grid.Row columns={4}>
              <Grid.Column>&nbsp;</Grid.Column>
              <Grid.Column>
                <Form.Field >
                  <label style={{'text-align' : 'left'}}>Franchisee</label>
                  <Select fluid={true} search={true}
                    options={this.state.franchisees} onChange={(e, d) => this.setState({selectedFranchisee: d.value})}
                    defaultValue={this.state.franchisees[0].value}
                  />  
                  <br />
                  <Button color="teal" onClick={(e) => {e.preventDefault(); this.handleAddFranchisee(); }}>
                    Add Franchisee
                  </Button>
                </Form.Field>
              </Grid.Column>
              <Grid.Column>
                <Form.Field>
                  <label style={{'text-align' : 'left'}}>Subcontractor</label>
                  <Select fluid={true} search={true}
                    options={this.state.subcontractors} 
                    onChange={(e, d) => this.setState({selectedSubcontractor: d.value})}
                    defaultValue={this.state.subcontractors[0].value}
                  /> 
                  <br />
                  <Button color="teal" onClick={(e) => {e.preventDefault(); this.handleAddSubcontractor(); }}>
                    Add Subcontractor
                  </Button>  
                </Form.Field>                            
              </Grid.Column>
              <Grid.Column>&nbsp;</Grid.Column>            
            </Grid.Row>

            <Grid.Row>
              <Grid.Column>
                <Form.Field>
                  <label style={{'text-align' : 'left'}}>AMC Amount:  {this.props.jobData.AMCAmount}</label>
                </Form.Field>
              </Grid.Column>              
            </Grid.Row>

          </Grid>
        </Form>
        </Segment> 
        <Listing data={this.state.data} columns={headerColumns} />
        <Container textAlign="right" fluid={true}>
          <Button color="red" onClick={this.props.onCancel} >Go Back</Button>
        </Container>
      </div>
    );
  }

}
