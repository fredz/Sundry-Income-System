import * as React from 'react';
import { connect } from 'react-redux';
import { switchSubPage } from '../../actions/app';
import { Grid, Segment, Input, Button, Header, Image, Form } from 'semantic-ui-react';

import { Topmenu } from '../../components/Layout/Topmenu';
import { subPages } from '../../constants/pages';

/** SubPages */
import Job from './Job/Job';

export interface ISundryJobsProps {}

interface IConnectedProps {
  subPageCode: subPages;
  message: any;
}

interface IConnectedDispatch {
  handleSwitchSubPage: (value: subPages) => void;
}

class SundryJobs extends React.Component<ISundryJobsProps & IConnectedProps & IConnectedDispatch, {}> {
  topMenuList = [
    {name: 'Job'},
  ];

  constructor(props) {
    super(props);

    this.handleItemClick = this.handleItemClick.bind(this);
    this.getContent = this.getContent.bind(this);
  }

  componentDidMount() {
    
  }

  handleItemClick(name: string) {
    const menu = this.topMenuList.find( (x) => x.name === name);

    let code: subPages;
    switch (name) {
      default:
      case this.topMenuList[0].name: 
        code = subPages.sundryjobs_job;
        break;
      
      case this.topMenuList[1].name:
        code = subPages.sundryjobs_job;
        break;

      case this.topMenuList[2].name:
        code = subPages.sundryjobs_job;
        break;
    }

    this.props.handleSwitchSubPage(code);
  }

  getContent() {
    switch (this.props.subPageCode) {
      default:
      case subPages.sundryjobs_job: 
        return <Job/>;
      
      case subPages.sundryjobs_job:
        return <Job/>;
    }
  }

  render() {
    const content = this.getContent();    

    return (
      <Grid.Column>
        <Topmenu menu={this.topMenuList} onItemClick={this.handleItemClick}/>
        {content}
      </Grid.Column>
    );
  }
}

function mapStateToProps(state: any): IConnectedProps {
  
  return {
    subPageCode: state.app.subPageCode,
    message: state.alert.message
  };
}

function mapDispatchToProps(dispatch): IConnectedDispatch {
  return {
    handleSwitchSubPage: (value: subPages) => dispatch(switchSubPage(value))
  };
}

export default 
  connect(mapStateToProps, mapDispatchToProps)(SundryJobs) as React.ComponentClass<ISundryJobsProps>;
