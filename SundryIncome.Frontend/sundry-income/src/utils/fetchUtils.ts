import Alert from 'react-s-alert';

let url = `${window.location.href.toString()}/api/v1/amc/`;

if (window.location.href.toString().indexOf('localhost') !== -1) {  
  url = 'http://sandbox.serviceaccesspoint.com.au//api/v1/amc/'; 
  //url = 'http://localhost:4369/amc/';
}
const dnnurl = 'http://dnn.serviceaccesspoint.com.au/WebAPI/';

export const fetchJson = (type: string = 'POST', data: FormData = null) => {
  const promise = new Promise<any>((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.open(type, dnnurl, true);
    
    request.onreadystatechange = () => {
      if (request.readyState === 4) {
        try {
          //resolve(JSON.parse(request.responseText)); // TODO Always getting empty
          resolve(request.responseText);
        } catch (e) {
          reject(e);
        }
      }
    };

    request.send(data);
  });

  return promise;
};

const call = (endpoint: string, data: any, type: 'POST' | 'GET' = 'POST', parse: boolean = true) => {
  const promise = new Promise<any>((resolve, reject) => {

    const xhr = new XMLHttpRequest();
    
    xhr.open(type, `${url}${endpoint}`, true);

    xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.onreadystatechange = () => { 
      if (xhr.readyState === 4 && xhr.status === 200) {
        try {
          if (parse) {
            resolve(JSON.parse(xhr.responseText));
          } else {
            resolve(xhr.responseText);
          }
        } catch (e) {
          reject(e);
        }
      } else if (xhr.readyState === 4 && xhr.status !== 200) {
        reject(xhr);
      }
    };

    xhr.send(JSON.stringify(data));
  });

  return promise;
};

function validator() {
  const validator = 'F335C7EE410E437CAE0EE49CC93A84F5';
  return (data) => {
    return Object.assign({}, data, { Validator: validator});
  };
}

/** Roles */
export const GetAllRoles = () => {
  return call('GetAllRoles', null, 'GET');
};

/** Login */
export interface IValidate {
  UserName: string;
  Password: string;
}
export const validate = (user: IValidate) => {
  const data = validator()(user);
  return call('ValidateUser', data);
};

export interface ILogout {
  UserName: string;
  UserId: string;
}
export const logout = (user: ILogout) => {
  const data = validator()(user);

  return call('LogoutUser', data, 'POST', false);
};

/** Users */
export interface IUser {
  FirstName: string;
  LastName: string;
  Email: string;
  MobileNo: string;
  Phone: string;
  Username: string;
  Password: string;
}

export const GetAllUsers = () => {
  return call('GetAllUsers', null, 'GET');
};

export const AddUser = (user: IUser) => {
  const data = validator()(user);
  return call('AddUser', data, 'POST');
};

export interface IUpdateUser {
  UserId: string;
  FirstName: string;
  LastName: string;
  Email: string;
  MobileNo: string;
  Phone: string;
  Username: string;
  Password: string;
  Uid: string;
}
export const UpdateUser = (user: IUpdateUser) => {  
  const data = validator()(user);
  return call('UpdateUser', data, 'POST', false);
};

/** Subcontractors */
export interface ISubcontractor {
  FirstName: string;
  LastName: string;
  CompanyName: string;  
}

export const GetAllSubcontractors = () => {
  return call('GetAllSubContractors', null, 'GET');
};

export const AddSubcontractor = (user: ISubcontractor) => {
  const data = validator()(user);
  return call('AddSubContractor', data, 'POST');
};

export interface IUpdateSubcontractor {
  SubcontractorId: string;    
  FirstName: string;
  LastName: string;
  CompanyName: string;
  Uid: string;
}
export const UpdateSubcontractor = (subcontractor: IUpdateSubcontractor) => {  
  const data = validator()(subcontractor);
  return call('UpdateSubContractor', data, 'POST', false);
};

export interface IVerifyIfUsernameExists {
  UserName: string;
}
export const VerifyIfUsernameExists = (username: IVerifyIfUsernameExists) => {
  const data = validator()(username);
  return call('VerifyIfUsernameExists', data, 'POST', false);
};

/** User Roles */
export function GetUserRolesByUserId(userId) {
  const data = validator()({ UserId: userId });
  return call('GetUserRolesByUserId', data);
}

export function DeleteUserRole(userrole) {
  const data = validator()(userrole);
  return call('DeleteUserRole', data);
}

export function AddUserRole(userrole) {
  const data = validator()(userrole);
  return call('AddUserRole', data);
}

export function UpdateUserRole(userrole) {
  const data = validator()(userrole);
  return call('UpdateUserRole', data);
}

/** Franchisee */
export interface IFranchisee {
  Code: string;
  FirstName: string;
  LastName: string;
  ContactName: string;  
  CompanyName: string;  
}

export const GetAllFranchisees = () => {
  return call('GetAllFranchisees', null, 'GET');
};

export const AddFranchisee = (franchisee: IFranchisee) => {
  const data = validator()(franchisee);
  return call('AddFranchisee', data, 'POST');
};

export interface IUpdateFranchisee {
  FranchiseeId: string;    
  Code: string;
  FirstName: string;
  LastName: string;
  ContactName: string;  
  CompanyName: string; 
  Uid: string;
}

export const UpdateFranchisee = (franchisee: IUpdateFranchisee) => {  
  const data = validator()(franchisee);
  return call('UpdateFranchisee', data, 'POST', false);
};

/** Customers */
export interface ICustomer {  
  FirstName: string;
  LastName: string;
  BillingName: string;
  Email: string;
  Phone: string;
  MobileNo: string;
  PurchaseOrderRequired: string;
  AMCInvoiceOnly: string;
  DefaultInvoiceDescription: string;
}

export const GetAllCustomers = () => {
  return call('GetAllCustomers', null, 'GET');
};

export const AddCustomer = (customer: ICustomer) => {
  const data = validator()(customer);
  return call('AddCustomer', data, 'POST');
};

export interface IUpdateCustomer {
  CustomerId: string;    
  FirstName: string;
  LastName: string;
  BillingName: string;
  Email: string;
  Phone: string;
  MobileNo: string;
  PurchaseOrderRequired: string;
  AMCInvoiceOnly: string;
  DefaultInvoiceDescription: string;
  Uid: string;
}

export const UpdateCustomer = (customer: IUpdateCustomer) => {  
  const data = validator()(customer);
  return call('UpdateCustomer', data, 'POST', false);
};

/** Job */
export interface IJob {  
  CustomerId: string;
  AreaManagerId: string;
  OrderNo: string;
  Description: string;
  RequestedDate: string;
  DateFrom: string;
  DateTo: string;  
  AMCAmount: string;  
}

export const GetAllCreateJobs = () => {
  return call('GetAllCreateJobs', null, 'GET');
};

export const CreateJob = (job: IJob) => {
  const data = validator()(job);
  return call('CreateJob', data, 'POST');
};

export interface IUpdateJob {
  JobId: string;    
  CustomerId: string;
  AreaManagerId: string;
  OrderNo: string;
  Description: string;
  RequestedDate: string;
  DateFrom: string;
  DateTo: string;  
  AMCAmount: string;  
  Uid: string;
}

export const UpdateCreateJob = (job: IUpdateJob) => {  
  const data = validator()(job);
  return call('UpdateCreateJob', data, 'POST', false);
};

export function DeleteCreateJob(job) {
  const data = validator()(job);
  return call('DeleteCreateJob', data);
}

/* JobAssignment */
export function GetJobAssignmentByJobId(jobId) {
  const data = validator()({ JobId: jobId });
  return call('GetJobAssignmentByJobId', data);
}

export interface IJobAssignment {  
  JobId: string;
  FranchiseeId: string;
  SubcontractorId: string;
  Amount: string;
  PurchaseInvoiceNo: string;
  Primary: boolean;
}

export const AddJobAssignment = (jobAssignment: IJobAssignment) => {
  const data = validator()(jobAssignment);
  return call('AddJobAssignment', data, 'POST');
};

export interface IUpdateJobAssignmentPrimary {  
  JobAssignId: string;
  Primary: boolean;
  Uid: string;
}

export const UpdateJobAssignmentPrimary = (jobAssignment: IUpdateJobAssignmentPrimary) => {
  const data = validator()(jobAssignment);
  return call('UpdateJobAssignmentPrimary', data, 'POST');
};

export interface IUpdateJobAssignmentAmount {  
  JobAssignId: string;
  Amount: string;
  Uid: string;
}

export const UpdateJobAssignmentAmount = (jobAssignment: IUpdateJobAssignmentAmount) => {
  const data = validator()(jobAssignment);
  return call('UpdateJobAssignmentAmount', data, 'POST');
};

export function DeleteJobAssignment(jobAssignment) {
  const data = validator()(jobAssignment);
  return call('DeleteJobAssignment', data);
}

/** Address */
export interface IAddress {  
  CustomerId: string;
  Type: string;
  Addresss: string;
  Locality: string;
  State: string;
  PostalCode: string;
}

export const GetAddressByCustomerId = (customerId) => {  
  const data = validator()({ CustomerId: customerId });
  return call('GetAddressByCustomerId', data);  
};

export const AddAddress = (address: IAddress) => {
  const data = validator()(address);
  return call('AddAddress', data, 'POST');
};

export interface IUpdateAddress {
  AddressId: string;    
  CustomerId: string;
  Type: string;
  Addresss: string;
  Locality: string;
  State: string;
  PostalCode: string;
  Uid: string;
}

export const UpdateAddress = (address: IUpdateAddress) => {  
  const data = validator()(address);
  return call('UpdateAddress', data, 'POST', false);
};

export function DeleteAddress(address) {
  const data = validator()(address);
  return call('DeleteAddress', data);
}

export function showAlert(type: string, message: string) {

  const setting = {
          position: 'top',
          effect: 'stackslide',
          beep: false,
          timeout: 15000,
          offset: 0,      
        };

  switch (type) {
    case 'warning':
      Alert.warning(message, setting);
      break;
    case 'error':
      Alert.error(message, setting);
      break;
    case 'info':
      Alert.info(message, setting);
      break;
    case 'success':
      Alert.success(message, setting);
      break;
    default:
      Alert.warning(message, setting);
  }

}

/** JobProcessStatus */
export interface IJobProgressStatus {  
  CustomerId: string;
  AreaManagerId: string;
  OrderNo: string;
  Description: string;
  RequestedDate: string;
  DateFrom: string;
  DateTo: string;  
  AMCAmount: string;
  StepNo: string;
  JobStatusId: string; 
}

export const GetAllJobProgressStatus = () => {
  return call('GetAllJobProgressStatus', null, 'GET');
};

export const AddJobProgressStatus = (jobProgressStatus: IJobProgressStatus) => {
  const data = validator()(jobProgressStatus);
  return call('AddJobProgressStatus', data, 'POST');
};

export interface IUpdateJobProgressStatus {
  JobId: string;    
  JobProgressId: string;
  JobStatusId: string;
  Uid: string;
}

export const UpdateJobProgressStatus = (jobProgressStatus: IUpdateJobProgressStatus) => {  
  const data = validator()(jobProgressStatus);
  return call('UpdateJobProgressStatus', data, 'POST', false);
};

/* JobStatus */
export const GetAllJobStatuses = () => {
  return call('GetAllJobStatuses', null, 'GET');
};

export function hexToRGB(hex, alpha) {
    const r = parseInt(hex.slice(1, 3), 16);
    const g = parseInt(hex.slice(3, 5), 16);
    const b = parseInt(hex.slice(5, 7), 16);

    if (alpha) {
        return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
    } else {
        return 'rgb(' + r + ', ' + g + ', ' + b + ')';
    }
}