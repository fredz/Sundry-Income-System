import * as fetchUtils from './fetchUtils';

export const sendNewPassword = (UserId: string) => {
  const formData = new FormData();
  formData.append('Action', 'Email/');
  formData.append('Path', 'NewPassword/');
  formData.append('UserId', UserId);
  
  return fetchUtils.fetchJson('POST', formData);
};

export const sendNewAccount = (UserId: string) => {
  const formData = new FormData();
  formData.append('Action', 'Email/');
  formData.append('Path', 'NewAccount/');
  formData.append('UserId', UserId);
  
  return fetchUtils.fetchJson('POST', formData);
};