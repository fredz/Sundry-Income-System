import { pages, subPages } from '../constants/pages';
import * as fetchUtils from './fetchUtils';

const userKey = 'user';
const userId = 'userId';
const guiKey = 'gui';

export enum LogResult {
  success,
  failed,
  invalid,
  noAccess,
}
export function logUser(user: string, password: string): Promise<LogResult> {
  const promise = new Promise<LogResult>((resolve, reject) => {
    fetchUtils.validate({
      UserName: user,
      Password: password
    }).then((res) => {
      if (res.UserId === 0) {
        resolve(LogResult.invalid);
      }

      fetchUtils.GetUserRolesByUserId(res.UserId)
      .then( (roles: any[]) => {
        const currentRoles = roles.filter( (x) => x.Disabled === 0);

        if (currentRoles.length <= 0) {
          resolve(LogResult.noAccess);
          return;
        } 

        //TODO SAVE ROLES 
        sessionStorage.setItem(userKey, user);
        sessionStorage.setItem(userId, res.UserId);
        sessionStorage.setItem(guiKey, '123');

        resolve(LogResult.success);
      })
      .catch(() => {
        reject(LogResult.failed);
      });
      
    })
    .catch(() => {
      reject(LogResult.failed);
    });
  });

  return promise;
}

export function setPage(pageCode: pages) {
  sessionStorage.setItem('pageCode', pageCode.toString());
}

export function getPage(): pages | null {
  return parseInt(sessionStorage.getItem('pageCode'));
}

export function setSubPage(subPageCode: subPages) {
  sessionStorage.setItem('subPageCode', subPageCode.toString());
}

export function getSubPage(): subPages | null {
  return parseInt(sessionStorage.getItem('subPageCode'));
}

export function logoutUser(callback: any = null) {
  const user: fetchUtils.ILogout = {
    UserName: sessionStorage.getItem(userKey),
    UserId: sessionStorage.getItem(userId),
  };

  fetchUtils.logout(user)
  .then((res) => {
    
    fetchUtils.showAlert('success', 'Logout Successful');
    if (typeof callback === 'function') {
      callback();
    }  

  }).catch(() => {    
    fetchUtils.showAlert('error', 'User was not completely logout');
  });

  clearSession();
}

export function getUser() {
  return sessionStorage.getItem(userKey);
}

function clearSession() {
  sessionStorage.clear();
}