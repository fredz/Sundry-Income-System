import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import configureStore from './configureStore';
import App from './App/App';

const store = configureStore({});

render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.querySelector('#ReactRoot'),
);
